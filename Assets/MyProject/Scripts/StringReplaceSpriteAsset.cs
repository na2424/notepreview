﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class StringReplaceSpriteAsset
{
	public static string ReplaceUra(string originalText)
	{
		string result = originalText;

		if (originalText.Contains("Ⓤ"))
		{
			result = originalText.Replace("Ⓤ", "<sprite name=\"ura\">");
		}

		if (originalText.Contains("(Ura)"))
		{
			result = originalText.Replace("(Ura)", "<sprite name=\"ura\">");
		}

		return result;
	}

}
