﻿using UnityEngine;

/// <summary>
/// Spriteの参照を持ち、外部に提供するScriptableObject
/// </summary>
[CreateAssetMenu(menuName = "MyScriptable/RefSprite")]
public sealed class RefSpriteScriptableObject : ScriptableObject
{
	[SerializeField] Sprite[] _sprites;

	public Sprite GetSprite(int index)
	{
		return _sprites[index];
	}
}
