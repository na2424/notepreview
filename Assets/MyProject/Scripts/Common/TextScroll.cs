﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Text;

using TMPro;

/// <summary>
/// 文字スクロールを行うクラス.
/// SetText関数で文字列をセットするとスクロールする文字か判別する.
/// </summary>
[RequireComponent(typeof(RectMask2D))]
public sealed class TextScroll : MonoBehaviour
{
	public enum TextState
	{
		StaticText,
		ScrollText,
	}

	[SerializeField] TextState _textState;

	//------------------------------
	// インスペクタまたはResetから設定.
	//------------------------------

	[SerializeField] TextMeshProUGUI _text;
	[SerializeField] RectTransform _textRectTransform;
	[SerializeField] RectTransform _rectTransform;
	[SerializeField] float _speedRatio = 1f;

	//--------------
	// キャッシュ.
	//--------------

	int _textWidth = -1;
	float _time = 0f;
	float _scrollWidth = 0f;
	float _offset = 0f;

	//--------------
	// 定数.
	//--------------

	const int SCROLL_SPACE = 77;
	const float SCROLL_SPEED = 200f;
	const float STOP_TIME = 4.2f;

	//-------------
	// プロパティ.
	//-------------

	TextState textState
	{
		get { return _textState; }
		set
		{
			_textState = value;
			SetTextPosition();
		}
	}

	public bool IsScrollText
	{
		get { return textState == TextState.ScrollText; }
	}

	//--------------
	// メソッド.
	//--------------

	void Reset()
	{
		_text = GetComponentInChildren<TextMeshProUGUI>();
		_textRectTransform = _text.GetComponent<RectTransform>();
		_rectTransform = GetComponent<RectTransform>();
	}

	void Start()
	{
		JudgeScroll();
	}

	private void Update()
	{
		if(!IsScrollText)
		{
			return;
		}
		ScrollTextUpdate();
	}

	/// <summary>
	/// この関数を通して文字をセットするとスクロール文字の時は自動でスクロールするようになる.
	/// </summary>
	public void SetText(string str)
	{
		_text.text = str;
		_textRectTransform.localPosition = Vector3.zero;
		_time = 0f;
		JudgeScroll();
	}

	/// <summary>
	/// スクロールする文字か判定.
	/// </summary>
	void JudgeScroll()
	{
		// テキストの幅を取得.
		_textWidth = Mathf.CeilToInt(_text.preferredWidth);

		// 自身のRectTransformのwidthを取得(マスクの範囲と同じ).
		_scrollWidth = _rectTransform.rect.max.x - _rectTransform.rect.min.x;

		_offset = _scrollWidth / 2;

		if (_textWidth < _scrollWidth)
		{
			textState = TextState.StaticText;
		}
		else
		{
			textState = TextState.ScrollText;
		}
			
	}

	// 文字を初期位置に移動.
	void SetTextPosition()
	{
		_textRectTransform.localPosition = new Vector3(-_offset, _textRectTransform.localPosition.y, 0f);
	}

	// 文字のスクロール.
	void ScrollTextUpdate()
	{
		// 静止位置まで来たらSTOP_TIME[秒]文字を停止.
		if (_time < STOP_TIME && _textRectTransform.localPosition.x <= -_offset)
		{
			_time += Time.deltaTime;
			return;
		}

		// 文字が見えなくなる位置まで左にスクロール.
		if (_textRectTransform.localPosition.x > -_textWidth - _offset)
		{
			float newPositionX = _textRectTransform.localPosition.x - (SCROLL_SPEED * _speedRatio * Time.deltaTime);
			_textRectTransform.localPosition = new Vector3(newPositionX, _textRectTransform.localPosition.y, 0f);
		}
		else
		{
			// 右端に戻す.
			_textRectTransform.localPosition = new Vector3(SCROLL_SPACE + _offset, _textRectTransform.localPosition.y, 0f);
			_time = 0f;
		}
	}
}