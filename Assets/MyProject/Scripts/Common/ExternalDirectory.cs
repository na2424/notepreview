using Cysharp.Text;
using Cysharp.Threading.Tasks;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using UnityEngine;
using UnityEngine.Networking;

public sealed class ExternalDirectory
{
	public static string SongsPath =>
		ZString.Concat(Directory.GetCurrentDirectory(), "/Songs");

	public static string NoteSkinsPath =>
		ZString.Concat(Directory.GetCurrentDirectory(), "/NoteSkins");

	public static string NoteSkinsStreamingAssetsPath =>
		ZString.Concat(Application.streamingAssetsPath, "/NoteSkins");

	public static string SoundEffectsPath =>
		ZString.Concat(Directory.GetCurrentDirectory(), "/SoundEffects");


	public static async UniTask<List<NoteSkinInfo>> NoteSkinsCopyFromStreamingToPersistentAsync(CancellationToken ct)
	{
		NoteSkinsLoader noteSkinsLoader = new NoteSkinsLoader();
		var noteSkinsList = await noteSkinsLoader.ReadAsync(ct);
		return noteSkinsList;
	}

	public static List<TouchSeInfo> LoadTouchSeInfosAsync()
	{
		return new TouchSeLoader().LoadTouchSeInfos();
	}
}
