﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public sealed class AspectRatioRawImage : MonoBehaviour
{
	[SerializeField] RectTransform _rectTransform;
	[SerializeField] RawImage _rawImage = default;

	private void Reset()
	{
		_rectTransform = transform.parent.GetComponent<RectTransform>();
		_rawImage = transform.GetComponent<RawImage>();
	}

	public void SetTexture(Texture tex)
	{
		float baseSizeRatio = _rectTransform.sizeDelta.x / _rectTransform.sizeDelta.y;
		float sizeRatio = (float)tex.width / tex.height;

		if (sizeRatio < baseSizeRatio)
		{
			float width = tex.width * _rectTransform.sizeDelta.y / tex.height;
			_rawImage.rectTransform.sizeDelta = new Vector2(width, _rectTransform.sizeDelta.y);
		}
		else
		{
			float height = tex.height * _rectTransform.sizeDelta.x / tex.width;
			_rawImage.rectTransform.sizeDelta = new Vector2(_rectTransform.sizeDelta.x, height);
		}
		_rawImage.texture = tex;
	}
}
