﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// タップ時にサイズが変わるボタン
/// </summary>
public sealed class SizeChangeButton : Button
{
    //------------------
    // キャッシュ.
    //------------------
    RectTransform _rt;
    Vector3 _size;
    static bool _isEnable = true;

    //------------------
    // 定数.
    //------------------
    const float SIZE_RATIO = 0.96f;

    RectTransform Rt 
    { 
        get 
        { 
            if(_rt != null)
			{
                return _rt;
			}
            else
			{
                _rt = transform.GetChild(0).GetComponent<RectTransform>();
                _size = _rt.localScale;
                return _rt;
            }
        }
    }

    public void SetOneSize()
	{
        Rt.localScale = Vector3.one;
    }

    public static void Enable(bool isEnable)
	{
        _isEnable = isEnable;
	}

    public override void OnPointerEnter(PointerEventData eventData)
    {
        if (currentSelectionState == SelectionState.Disabled)
        {
            return;
        }

        base.OnPointerEnter(eventData);
        Rt.localScale = _size * SIZE_RATIO;
    }

    public override void OnPointerExit(PointerEventData eventData)
    {
        if (currentSelectionState == SelectionState.Disabled)
        {
            return;
        }

        Rt.localScale = _size;
        base.OnPointerExit(eventData);
        
    }

    public override void OnPointerClick(PointerEventData eventData)
    {
        if (!_isEnable || currentSelectionState == SelectionState.Disabled)
        {
            return;
        }

        base.OnPointerClick(eventData);
        Rt.localScale = _size;

    }
}
