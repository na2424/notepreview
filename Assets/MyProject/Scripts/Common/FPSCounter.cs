using Cysharp.Text;
using UnityEngine;
using TMPro;

[Prefab("SingletonPrefabs/FPSCounter", true)]
public sealed class FPSCounter : SingletonMonoBehaviour<FPSCounter>
{
    [SerializeField] TextMeshProUGUI _fpsText = default;
    [SerializeField] float _updateInterval = 0.5f;

    private float _accum;
    private int _frames;
    private float _timeleft;
    private float _fps;

    public void SetActive(bool isActive)
	{
        gameObject.SetActive(isActive);
	}

    private void Update()
    {
        _timeleft -= Time.deltaTime;
        _accum += Time.timeScale / Time.deltaTime;
        _frames++;

        if (0 < _timeleft) return;

        _fps = _accum / _frames;
        _timeleft = _updateInterval;
        _accum = 0;
        _frames = 0;

        _fpsText.SetTextFormat("FPS: {0:F2}", _fps);
    }
}