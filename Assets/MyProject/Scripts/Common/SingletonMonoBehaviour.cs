﻿using Cysharp.Text;
using System;
using UnityEngine;

/// <summary>
/// 常に単一のインスタンスしか存在しないことを保証する
/// 参考文献:http://esprog.hatenablog.com/entry/2016/02/06/153832
/// </summary>
public abstract class SingletonMonoBehaviour<T> : MonoBehaviour where T : MonoBehaviour
{
	private static T _instance;
	private static bool _instantiated;

	//overrideする時は必ず先にbase.Awake()する.
	protected virtual void Awake()
	{
		if (_instance == null)
		{
			_instance = FindObjectOfType(typeof(T)) as T;

			if (_instance == null)
				Debug.Log(typeof(T) + "is noting");

			var type = typeof(T);
			var attribute = Attribute.GetCustomAttribute(type, typeof(PrefabAttribute)) as PrefabAttribute;

			if (attribute == null)
				return;

			if (attribute.Persistent)
				DontDestroyOnLoad(gameObject);

			return;
		}

		if (GetInstanceID() != _instance.GetInstanceID())
			DestroyImmediate(this.gameObject);
	}

	/// <summary>
	/// インスタンスへのアクセスを提供する
	/// </summary>
	public static T Instance
	{
		get
		{
			if (_instantiated)
				return _instance;

			var type = typeof(T);
			var objects = FindObjectsOfType<T>();

			if (objects.Length > 0)
			{
				Instance = objects[0];
				if (objects.Length > 1)
				{
					Debug.LogWarning(ZString.Concat("複数の\"", type, "\"が存在したため、削除されました"));
					for (var i = 1; i < objects.Length; i++)
						DestroyImmediate(objects[i].gameObject);
				}
				_instantiated = true;
				return _instance;
			}
#if UNITY_EDITOR
			Debug.Log(ZString.Concat("ゲーム内に", type, "型インスタンスが存在しないためResources内のPrefabからの作成処理を行います"));
#endif
			var attribute = Attribute.GetCustomAttribute(type, typeof(PrefabAttribute)) as PrefabAttribute;
			if (attribute == null)
			{
				Debug.LogError(ZString.Concat("PrefabAttributeが付加されていない型\"", type, "\"が参照されました"));
			}
			else
			{
				var prefabName = attribute.Path;
				if (String.IsNullOrEmpty(prefabName))
				{
					Debug.LogError(ZString.Concat("PrefabAttributeのnameが空です \"", type, "\""));
					return null;
				}

				//-----ここでPrefabをシーン上に生成-----.
				GameObject go = Instantiate(Resources.Load<GameObject>(prefabName)) as GameObject;
				//--------------------------------------.

				if (go == null)
				{
					Debug.LogError("\"type\"" + "型のPrefab\"" + prefabName + "\"を生成できませんでした");
					return null;
				}
				go.name = prefabName;
				Instance = go.GetComponent<T>();
				if (!_instantiated)
				{
					Debug.LogWarning(ZString.Concat("\"", type, "\"型のComponentが\"", prefabName, "\"に存在しなかったため追加されました"));
					Instance = go.AddComponent<T>();
				}
				if (attribute.Persistent)
					DontDestroyOnLoad(_instance.gameObject);
				return _instance;
			}
			Debug.LogWarning("Prefabからの生成に失敗したため、強制的にオブジェクトを生成します");
			var createObject = new GameObject("SingletonCreateObject", type);
			Instance = createObject.GetComponent<T>();
			return _instance;
		}

		private set
		{
			_instance = value;
			_instantiated = value != null;
		}
	}

	private void OnDestroy()
	{
		_instantiated = false;
	}
}