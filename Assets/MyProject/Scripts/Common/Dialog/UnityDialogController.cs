﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

namespace Dialog
{
	//TODO Android/iOS のネイティブダイアログが実装されたら
	// #if UnityEditor ～ #endif でくくる.

	/// <summary>
	/// UnityEditor 用ダイアログのためのコンポーネント.
	/// </summary>
	public class UnityDialogController : MonoBehaviour, IDialogController
	{
		[Header( "Text" )]
		[SerializeField]
		protected TextMeshProUGUI _titleText = null;
		[SerializeField]
		protected TextMeshProUGUI _messageText = null;
		[SerializeField]
		protected TextMeshProUGUI _decideButtonText = null;
		[SerializeField]
		protected TextMeshProUGUI _cancelButtonText = null;

		[Header( "Buttons" )]
		[SerializeField]
		protected Button _decideButton = null;
		[SerializeField]
		protected Button _cancelButton = null;

		Action _callbackOnAutoClosed = null;

		public static UnityDialogController Create()
		{
			var unityDialogPrefab = Resources.Load<GameObject>("Dialog/Dialog_UnityEditor");
			if( unityDialogPrefab )
			{
				// UnityAPI の関数はメインスレッドからしか呼んではいけないが、
				// DialogManager の Start() でしか呼ばれない
				var obj = Instantiate( unityDialogPrefab );
				if( obj )
					return obj.GetComponent<UnityDialogController>();
			}
			return null;
		}

		void Awake()
		{
			gameObject.SetActive( false );
			transform.parent = DialogManager.Instance.transform;
		}
		public virtual bool Open( DialogParametor data )
		{
			if( !setParam() )
				return false;

			gameObject.SetActive( true );
			return true;

			bool setParam()
			{
				Debug.Assert( data != null );

				if( _titleText )
					_titleText.text = data.Title;
				if( _messageText )
					_messageText.text = data.Message;

				_callbackOnAutoClosed += data.CallbackOnAutoClosed;

				switch( data.Options )
				{
				case DialogParametor.OptionsType.Single:
					registerButtonCallback( _decideButton, _decideButtonText, data.DefaultButton );
					_cancelButton?.gameObject.SetActive( false );
					break;
				case DialogParametor.OptionsType.DefaultCancel:
					registerButtonCallback( _decideButton, _decideButtonText, data.DefaultButton );
					_cancelButton?.gameObject.SetActive( true );
					registerButtonCallback( _cancelButton, _cancelButtonText, data.CancelButton );
					break;
				case DialogParametor.OptionsType.DestructiveCancel:
					registerButtonCallback( _decideButton, _decideButtonText, data.DestructiveButton );
					_cancelButton?.gameObject.SetActive( true );
					registerButtonCallback( _cancelButton, _cancelButtonText, data.CancelButton );
					break;
				default:
					return false;
				}

				return true;
			}
			void registerButtonCallback( Button button, TextMeshProUGUI buttonText, DialogParametor.ActionButton buttonData )
			{
				if( buttonText )
					buttonText.text = buttonData.Label;
				button?.onClick.RemoveAllListeners();
				button?.onClick.AddListener(
					() =>
					{
						buttonData.Callback?.Invoke();
						close();
					}
				);
			}
		}

		public bool Close()
		{
			_callbackOnAutoClosed?.Invoke();

			return close();
		}

		protected virtual bool close()
		{
			gameObject.SetActive( false );
			return true;
		}

		protected virtual void OnDisable()
		{
			_decideButton?.onClick.RemoveAllListeners();
			_decideButton?.onClick.RemoveAllListeners();
			_callbackOnAutoClosed = null;
		}
	}
}
