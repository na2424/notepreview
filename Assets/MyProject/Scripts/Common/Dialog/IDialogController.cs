﻿namespace Dialog
{
	/// <summary>
	/// ダイアログを操作するクラスのインタフェイス.
	/// 各プラットフォームのネイティブダイアログは、このインタフェイスを通して操作する.
	/// </summary>
	interface IDialogController
	{
		bool Open(DialogParametor data);
		bool Close();
	}
}
