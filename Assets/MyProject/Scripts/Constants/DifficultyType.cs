﻿/// <summary>
/// 難易度の種類
/// </summary>
public enum DifficultyType : int
{
	Easy = 0,
	Normal,
	Hard,
	Extra,
	Lunatic,
}
