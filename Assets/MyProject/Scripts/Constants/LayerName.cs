﻿/// <summary>
/// レイヤー名を定数で管理するクラス
/// </summary>
public static class LayerName
{
	public const int Default = 0;
	public const int TransparentFX = 1;
	public const int IgnoreRaycast = 2;
	public const int Water = 4;
	public const int UI = 5;
	public const int ActiveLines = 8;
	public const int FlattenedLines = 9;
	public const int Background = 10;
	public const int Bins = 11;
	public const int FlungTrash = 12;
	public const int WorldBall = 13;
	public const int Checkpoints = 14;
	public const int JudgeArea = 15;
	public const int LaneCover = 16;
	public const int DefaultMask = 1;
	public const int TransparentFXMask = 2;
	public const int IgnoreRaycastMask = 4;
	public const int WaterMask = 16;
	public const int UIMask = 32;
	public const int ActiveLinesMask = 256;
	public const int FlattenedLinesMask = 512;
	public const int BackgroundMask = 1024;
	public const int BinsMask = 2048;
	public const int FlungTrashMask = 4096;
	public const int WorldBallMask = 8192;
	public const int CheckpointsMask = 16384;
	public const int JudgeAreaMask = 32768;
	public const int LaneCoverMask = 65536;
}
