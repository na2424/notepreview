﻿using Cysharp.Threading.Tasks;
using Dialog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using UnityEngine;

public sealed class SongInfoLoader
{
	/// <summary>
	/// 楽曲情報の読み込み
	/// (譜面の中身はここでは読まない)
	/// </summary>
	/// <returns></returns>
	public async UniTask<SongInfo> ReadAsync(CancellationToken ct, string directory)
	{
		string text = await ReadFileText(ct, directory);

		if (string.IsNullOrEmpty(text))
		{
			var builder = new DialogParametor.Builder("譜面がありません", "ダンカグライクの譜面データ(.dl)があるか確認してください。");
			builder.AddDefaultAction("はい", () => { });
			builder.AddCallbackOnAutoClosed(() => { });
			DialogManager.Instance.Open(builder.Build());
			return null;
		}

		if (ct.IsCancellationRequested)
		{
			return null;
		}

		SongInfo songInfo = null;

		try
		{
			await UniTask.SwitchToThreadPool();

			songInfo = LoadSongInfoAsync(text);
			songInfo.DirectoryPath = directory;

			await UniTask.Yield();

			songInfo.JacketTexture = CreateJacketTexture(songInfo.DirectoryPath, songInfo.JacketFileName);
		}
		catch (Exception ex)
		{
			var builder = new DialogParametor.Builder("エラー", ex.Message);
			builder.AddDefaultAction("閉じる", () => { });
			builder.AddCallbackOnAutoClosed(() => { });
			DialogManager.Instance.Open(builder.Build());
		}

		return songInfo;
	}

	/// <summary>
	/// ファイルからテキストを取得.
	/// </summary>
	/// <returns>(楽曲のディレクトリパス, テキスト)</returns>
	async UniTask<string> ReadFileText(CancellationToken ct, string directory)
	{
		string result;

		var extensions = new string[] { "*.dl", "*.txt" };

		string[] dlfiles = null;

		// .dlまたは.txtのファイルを検索
		foreach (string ext in extensions)
		{
			dlfiles = Directory.GetFiles(directory, ext, System.IO.SearchOption.TopDirectoryOnly);

			if (dlfiles.Length > 0)
			{
				break;
			}
		}

		if (dlfiles.Length == 0)
		{
			return null;
		}

		var path = Path.Combine(directory, dlfiles[0]);

		FileInfo file = null;

		// ファイルがある場合
		if (File.Exists(path))
		{
			file = new FileInfo(path);
		}
		else
		{
			Debug.LogError("ファイルがありません:" + path);
		}

		using (StreamReader sr = new StreamReader(file.OpenRead(), Encoding.UTF8))
		{
			try
			{
				result = await sr.ReadToEndAsync()
				.AsUniTask()
				.AttachExternalCancellation(ct);
			}
			catch (OperationCanceledException e)
			{
				Debug.Log("キャンセルされました:" + e.Message);
				throw;
			}

		}

		return result;
	}

	/// <summary>
	/// テキストから楽曲情報を読み取りつつ、SongInfoクラスに情報を入れて返す
	/// </summary>
	/// <param name="text">テキスト</param>
	/// <returns>SonInfo</returns>
	SongInfo LoadSongInfoAsync(string text)
	{
		SongInfo songInfo = new SongInfo();

		StringReader rs = new StringReader(text);
		bool[] flags = new bool[18];

		bool hasBaseBpm = false;

		//ストリームの末端まで繰り返す
		while (rs.Peek() > -1)
		{
			//一行読み込む
			var lineText = rs.ReadLine().Trim();
			int index = -1;

			if (!flags[++index] && lineText.CustomStartsWith("#TITLE:"))
			{
				flags[index] = true;
				songInfo.Title = lineText.GetBetweenChars(':', ';');
				continue;
			}

			if (!flags[++index] && lineText.CustomStartsWith("#SUBTITLE:"))
			{
				flags[index] = true;
				songInfo.SubTitle = lineText.GetBetweenChars(':', ';');
				continue;
			}

			if (!flags[++index] && lineText.CustomStartsWith("#ARTIST:"))
			{
				flags[index] = true;
				songInfo.Artist = lineText.GetBetweenChars(':', ';');
				continue;
			}

			if (!flags[++index] && lineText.CustomStartsWith("#MUSIC:"))
			{
				flags[index] = true;
				songInfo.MusicFileName = lineText.GetBetweenChars(':', ';');
				continue;
			}

			if (!flags[++index] && (lineText.CustomStartsWith("#JACKET:") || lineText.CustomStartsWith("#BANNER:")))
			{
				flags[index] = true;
				songInfo.JacketFileName = lineText.GetBetweenChars(':', ';');
				continue;
			}

			if (!flags[++index] && lineText.CustomStartsWith("#BACKGROUND:"))
			{
				flags[index] = true;
				songInfo.BgFileName = lineText.GetBetweenChars(':', ';');
				continue;
			}

			if (!flags[++index] && lineText.CustomStartsWith("#CREDIT:"))
			{
				flags[index] = true;
				songInfo.SequenceArtist = lineText.GetBetweenChars(':', ';');
				continue;
			}

			if (!flags[++index] && lineText.CustomStartsWith("#ILLUST:"))
			{
				flags[index] = true;
				songInfo.Illust = lineText.GetBetweenChars(':', ';');
				continue;
			}

			if (!flags[++index] && lineText.CustomStartsWith("#DESCRIPTION:"))
			{
				flags[index] = true;

				if (lineText.Contains(";"))
				{
					songInfo.Description.Add(lineText.GetBetweenChars(':', ';'));
				}
				else
				{
					lineText = rs.ReadLine().Trim();

					while (!lineText.Contains(";"))
					{
						songInfo.Description.Add(lineText);
						lineText = rs.ReadLine().Trim();
					}
				}

				continue;
			}

			if (!flags[++index] && lineText.StartsWith("#OFFSET:"))
			{
				flags[index] = true;

				if (float.TryParse(lineText.GetBetweenChars(':', ';'), out var value))
				{
					songInfo.Offset = value;
				}
				else
				{
					songInfo.Offset = 0f;
				}
				continue;
			}

			if (!flags[++index] && lineText.CustomStartsWith("#BPMS:"))
			{
				flags[index] = true;

				while (!lineText.Contains(";"))
				{
					lineText += rs.ReadLine().Trim();
				}

				var bpmData = lineText.GetBetweenChars(':', ';').Split(new char[] { '=', ',' }, StringSplitOptions.RemoveEmptyEntries);

				if (!hasBaseBpm)
				{
					songInfo.BaseBpm = Convert.ToSingle(bpmData[1]);
				}

				for (int i = 0; i < bpmData.Length; i++)
				{
					if (i % 2 == 0)
					{
						if (float.TryParse(bpmData[i], out var bpmPosition))
						{
							songInfo.BpmPositions.Add(bpmPosition);
						}
					}
					else
					{
						if (float.TryParse(bpmData[i], out var bpm))
						{
							songInfo.Bpms.Add(bpm);
						}
					}
				}

				continue;
			}

			if (!flags[++index] && lineText.CustomStartsWith("#BASEBPM:"))
			{
				flags[index] = true;
				if (float.TryParse(lineText.GetBetweenChars(':', ';'), out var value))
				{
					songInfo.BaseBpm = value;
					hasBaseBpm = true;
				}
				continue;
			}

			if (!flags[++index] && lineText.CustomStartsWith("#SPEEDS:"))
			{
				flags[index] = true;

				while (!lineText.Contains(";"))
				{
					lineText += rs.ReadLine().Trim();
				}

				var speedData = lineText.GetBetweenChars(':', ';').Split(new char[] { '=', ',' }, StringSplitOptions.RemoveEmptyEntries);

				for (int i = 0; i < speedData.Length; i++)
				{
					if (i % 3 == 0)
					{
						if (float.TryParse(speedData[i], out var speedPosition))
						{
							songInfo.SpeedPositions.Add(speedPosition);
						}
					}
					else if (i % 3 == 1)
					{
						if (float.TryParse(speedData[i], out var speedStretchRatio))
						{
							songInfo.SpeedStretchRatios.Add(speedStretchRatio);
						}
					}
					else if (i % 3 == 2)
					{
						if (float.TryParse(speedData[i], out var speedDelayBeat))
						{
							songInfo.SpeedDelayBeats.Add(speedDelayBeat);
						}
					}
				}

				continue;
			}

			if (!flags[++index] && lineText.CustomStartsWith("#BGCHANGES:"))
			{
				flags[index] = true;

				while (!lineText.Contains(";"))
				{
					lineText += rs.ReadLine().Trim();
				}

				var bgChangeData = lineText.GetBetweenChars(':', ';').Split(new char[] { '=', ',' }, StringSplitOptions.RemoveEmptyEntries);

				for (int i = 0; i < bgChangeData.Length; i++)
				{
					if (i % 2 == 0)
					{
						if (float.TryParse(bgChangeData[i], out var bgChangePosition))
						{
							songInfo.BgChangePositions.Add(bgChangePosition);
						}
					}
					else if (i % 2 == 1)
					{
						songInfo.BgChangeImageName.Add(bgChangeData[i].Trim());
					}
				}

				continue;
			}

			if (!flags[++index] && lineText.CustomStartsWith("#CMOD:"))
			{
				flags[index] = true;
				if (int.TryParse(lineText.GetBetweenChars(':', ';'), out var value))
				{
					songInfo.IsCmod = (value == 1);
				}
				continue;
			}

			if (!flags[++index] && lineText.CustomStartsWith("#DIFFICULTIES:"))
			{
				flags[index] = true;
				var diff = lineText.GetBetweenChars(':', ';').Split(',');

				int count = 0;
				songInfo.Difficulty = new List<int>(5) { 0, 0, 0, 0, 0 };

				foreach (var d in diff)
				{
					if (d.IsNullOrEmpty() || count >= 5)
					{
						continue;
					}

					songInfo.Difficulty[count] = (d == "X" || d == "x") ? 
						Constant.SpecialNumber.DIFFICULT_X :
						Convert.ToInt32(d);

					count++;
				}

				continue;
			}

			if (!flags[++index] && lineText.CustomStartsWith("#SAMPLESTART"))
			{
				flags[index] = true;

				if (float.TryParse(lineText.GetBetweenChars(':', ';'), out var value))
				{
					songInfo.SampleStart = value;
				}
				else
				{
					songInfo.SampleStart = 0f;
				}
				continue;
			}

			if (!flags[++index] && lineText.CustomStartsWith("#SAMPLELENGTH"))
			{
				flags[index] = true;

				if (float.TryParse(lineText.GetBetweenChars(':', ';'), out var value))
				{
					songInfo.SampleLength = value;
				}
				else
				{
					songInfo.SampleLength = 12f;
				}
				continue;
			}

			if (lineText.CustomStartsWith("#NOTES:"))
			{
				break;
			}

			bool isLoaded = true;
			for (var i = 0; i < flags.Length; i++)
			{
				if (!flags[i])
				{
					isLoaded = false;
					break;
				}
			}

			if (isLoaded)
			{
				rs.Close();
				return songInfo;
			}
		}

		rs.Close();
		return songInfo;
	}

	/// <summary>
	/// Jacket画像のTextureを生成
	/// </summary>
	/// <param name="path">ファイルパス</param>
	/// <returns>Texture</returns>
	Texture CreateJacketTexture(string directoryPath, string jacketFileName)
	{
		Texture2D texture = null;
		if (jacketFileName.HasValue())
		{
			var path = Path.Combine(directoryPath, jacketFileName);
			texture = TextureLoader.Load(path);
		}
		else
		{
			texture = new Texture2D(1, 1, TextureFormat.RGB24, false);
		}

		return texture;
	}
}
