﻿using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// シーン移動時のフェードイン・アウトを担当するクラス
/// </summary>
[Prefab("SingletonPrefabs/FadeManager", true)]
public sealed class FadeManager : SingletonMonoBehaviour<FadeManager>
{
    //--------------------------------------
    // インスペクタまたは外部から設定.
    //--------------------------------------
    [SerializeField] Canvas _fadeCanvas;
    [SerializeField] Image _fadeImage;
    [SerializeField] Texture _maskTexture = null;

    //------------------
    // キャッシュ.
    //------------------
    // フェード用Imageの透明度
    float _alpha = 0.0f;

    // フェードインアウトのフラグ
    bool _isFadeIn = false;
    bool _isFadeOut = false;

    // フェードしたい時間（単位は秒）
    float _fadeTime = 0.2f;

    // 遷移先のシーン番号
    int _nextScene = 1;

    // Shaderプロパティ
    int _maskTexId = -1;
    int _rangeId = -1;

    //フェード用のCanvasとImage生成
    protected override void Awake()
	{
		base.Awake();
        _maskTexId = Constant.ShaderProperty.MaskTexId;
        _rangeId = Constant.ShaderProperty.RangeId;

        _fadeImage.material.SetTexture(_maskTexId, _maskTexture);
    }

    public void SetBlack()
	{
        _fadeImage.color = Color.black;
    }

    /// <summary>
    /// フェードイン開始
    /// </summary>
    /// <returns></returns>
    public async UniTask FadeInAsync()
    {
        //UpdateMaskTexture();
        _fadeImage.color = Color.black;
        _isFadeIn = true;
        await UpdateAsync();
    }

    /// <summary>
    /// フェードアウト開始
    /// </summary>
    /// <param name="n">次のシーンのインデックス</param>
    /// <returns></returns>
    public async UniTask FadeOutAsync(int n)
    {
        //UpdateMaskTexture();
        _nextScene = n;
        _fadeImage.color = Color.clear;
        _fadeCanvas.enabled = true;
        _isFadeOut = true;
        await UpdateAsync();
    }

    void UpdateMaskTexture()
    {
        _fadeImage.material.SetTexture(_maskTexId, _maskTexture);
    }

    void UpdateMaskCutout(float range)
    {
        enabled = true;
        _fadeImage.material.SetFloat(_rangeId, 1 - range);
    }

    async UniTask UpdateAsync()
    {
        while(true)
		{
            //フラグ有効なら毎フレームフェードイン/アウト処理
            if (_isFadeIn)
            {
                //経過時間から透明度計算
                _alpha -= Time.deltaTime / _fadeTime;
                UpdateMaskCutout(_alpha);
                //フェード用Imageの色・透明度設定
                _fadeImage.color = new Color(0.0f, 0.0f, 0.0f, _alpha);

                //フェードイン終了判定
                if (_alpha <= 0.0f)
                {
                    _isFadeIn = false;
                    _alpha = 0.0f;

                    await UniTask.Delay(240);

                    _fadeCanvas.enabled = false;
                    return;
                }
            }
            else if (_isFadeOut)
            {
                //経過時間から透明度計算
                _alpha += Time.deltaTime / _fadeTime;
                UpdateMaskCutout(_alpha);
                //フェード用Imageの色・透明度設定
                _fadeImage.color = new Color(0.0f, 0.0f, 0.0f, _alpha);

                //フェードアウト終了判定
                if (_alpha >= 1.0f)
                {
                    await UniTask.Delay(240);
                    _isFadeOut = false;
                    _alpha = 1.0f;

                    //次のシーンへ遷移
                    SceneManager.LoadScene(_nextScene);
                    return;
                }
            }

            await UniTask.Yield();
        }
        
    }
}