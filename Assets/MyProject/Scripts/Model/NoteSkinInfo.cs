﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NoteSkinInfo
{
	public string skin_name;

	public string notes_tap;
	public string notes_slide;
	public string notes_fuzzy;
	public string relay_slide;
	public string relay_fuzzy;
	public string line_slide;
	public string line_fuzzy;

	public string DirectoryPath;

	public Texture NotesTapTexture;
	public Texture NotesSlideTexture;
	public Texture NotesFuzzyTexture;
	public Texture RelaySlideTexture;
	public Texture RelayFuzzyTexture;

	public Texture LineSlideTexture;
	public Texture LineFuzzyTexture;
}
