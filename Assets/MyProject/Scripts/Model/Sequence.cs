using System.Collections.Generic;

public enum NoteType : int
{
	Undefined = 0,
	Normal = 1,
	LongStart = 2,
	LongRelay = 3,
	LongEnd = 4,
	Fuzzy = 5,
	FuzzyLongStart,
	FuzzyLongRelay,
	FuzzyLongEnd
}

public class Sequence
{
	public List<float> BeatPositions;
	public List<int> Lanes;
	public List<NoteType> NoteTypes;
	public List<bool> LeftRight;

	public List<LongInfo> LongInfo;

	public Sequence()
	{
		BeatPositions = new List<float>();
		Lanes = new List<int>();
		NoteTypes = new List<NoteType>();
		LeftRight = new List<bool>();
		LongInfo = new List<LongInfo>();
	}
}
