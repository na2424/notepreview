﻿using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 楽曲データ
/// (譜面情報を含まない)
/// </summary>
[Serializable]
public class SongInfo
{
	public string Title;
	public string SubTitle;
	public string Artist;
	public string SequenceArtist;
	public string Illust;
	public List<string> Description = new List<string>();
	public List<int> Difficulty = new List<int>();
	public float Offset;
	public float BaseBpm;
	public List<float> BpmPositions = new List<float>();
	public List<float> Bpms = new List<float>();
	public List<float> SpeedPositions = new List<float>();
	public List<float> SpeedStretchRatios = new List<float>();
	public List<float> SpeedDelayBeats = new List<float>();
	public List<float> BgChangePositions = new List<float>();
	public List<string> BgChangeImageName = new List<string>();
	public bool IsCmod;
	public float SampleStart;
	public float SampleLength;
	public string DirectoryPath;
	public string MusicFileName;
	public string JacketFileName;
	public string BgFileName;
	public Texture JacketTexture;
}