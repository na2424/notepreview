﻿using System.Collections.Generic;

public enum LongType : int
{
	Long = 0,
	FuzzyLong = 1
}

public class LongInfo
{
	public int StartNoteIndex;
	public List<int> NoteIndex;
	public List<int> Lanes;
	public List<double> BeatPositions;
	public LongType LongType;
	public double EndTime;
}