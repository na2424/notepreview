﻿using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UBL;
using System.IO;

/// <summary>
/// 背景を設定するクラス
/// </summary>
[Prefab("SingletonPrefabs/BgManager", true)]
public sealed class BgManager : SingletonMonoBehaviour<BgManager>
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[SerializeField] RawImage _bgImage;
	[SerializeField] CanvasScaler _canvasScaler;

	//------------------
	// キャッシュ.
	//------------------
	//Dictionary<string, Texture> _textureCache = new Dictionary<string, Texture>();
	Dictionary<string, Texture> _bgChangeTextureCache = new Dictionary<string, Texture>();
	float _screenRatio = 1f;

	//------------------
	// 定数.
	//------------------
	const int DEFAULT_DIMMER = 33;

	//------------------
	// プロパティ.
	//------------------
	public Texture CurrentBgTexture => _bgImage.texture;

	protected override void Awake()
	{
		base.Awake();

		// Canvasの解像度
		_canvasScaler.referenceResolution = new Vector2(Screen.width, Screen.height);

		// スクリーンのサイズ比
		_screenRatio = (float)Screen.width / (float)Screen.height;
	}

	/// <summary>
	/// 解像度を初期化
	/// </summary>
	public void ResetResolution()
	{
		// Canvasの解像度
		_canvasScaler.referenceResolution = new Vector2(Screen.width, Screen.height);

		// スクリーンのサイズ比
		_screenRatio = (float)Screen.width / (float)Screen.height;

		if (_bgImage.texture != null)
		{
			SetSize((Texture2D)_bgImage.texture);
		}
	}

	/// <summary>
	/// ファイルパスから背景をロードする
	/// </summary>
	/// <param name="path">ファイルパス</param>
	public void LoadFromPath(string path = null)
	{
		if (path == null)
		{
			path = string.Empty;
		}

		Texture2D texture = null;

		if (path.HasValue())
		{
			texture = TextureLoader.Load(path);
		}
		else
		{
			texture = new Texture2D(256, 256, TextureFormat.RGB24, false);
		}

		_bgImage.texture = texture;

		ResetResolution();
	}

	/// <summary>
	/// 画面サイズとテクスチャのサイズを比較して、
	/// 背景の大きさを設定する(外側に合わせる)
	/// </summary>
	/// <param name="texture"></param>
	void SetSize(Texture2D texture)
	{
		float width = texture.width;
		float height = texture.height;
		float ratio = width / height;

		_bgImage.rectTransform.sizeDelta = ratio >= _screenRatio ?
			new Vector2(_bgImage.texture.width * Screen.height / (float)_bgImage.texture.height, Screen.height) :
			new Vector2(Screen.width, _bgImage.texture.height * Screen.width / (float)_bgImage.texture.width);
	}

	/// <summary>
	/// 背景ディマーを設定する.
	/// オプションを使用しない時はデフォルト値が使用される.
	/// </summary>
	/// <param name="shouldUse">オプションの背景ディマーを使用するか</param>
	public void UseDimmer(bool shouldUse)
	{
		Color color;

		if (shouldUse)
		{
			float dark = 1f - GameManager.Instance.DisplayOption.Dimmer / 100f;
			color = new Color(dark, dark, dark, 1f);
		}
		else
		{
			float dark = 1f - DEFAULT_DIMMER / 100f;
			color = new Color(dark, dark, dark, 1f);
		}

		_bgImage.DOColor(color, 1f);
	}


	/// <summary>
	/// ファイルパスからBgChange画像をを事前ロードする
	/// </summary>
	/// <param name="path">ファイルパス</param>
	public void LoadBgChangeImages(string directoryPath, List<string> bgChangeImageName)
	{
		if (bgChangeImageName.IsNullOrEmpty())
		{
			return;
		}

		foreach (var name in bgChangeImageName)
		{
			string path = Path.Combine(directoryPath, name);

			if (_bgChangeTextureCache.ContainsKey(name))
			{
				continue;
			}

			Texture2D texture = path.HasValue() ?
				TextureLoader.Load(path) :
				new Texture2D(256, 256, TextureFormat.RGB24, false);

			_bgChangeTextureCache[name] = texture;
		}
	}

	/// <summary>
	/// 指定のBgChange画像に変える
	/// </summary>
	/// <param name="name"></param>
	public void ChangeBgImage(string name = null)
	{
		if (_bgChangeTextureCache.ContainsKey(name))
		{
			Texture2D texture = (Texture2D)_bgChangeTextureCache[name];
			_bgImage.texture = texture;
			SetSize(texture);
		}
	}

	/// <summary>
	/// BgChange画像のキャッシュを破棄する
	/// </summary>
	public void ClearBgChangeImageCache()
	{
		_bgChangeTextureCache.Clear();
	}


	///// <summary>
	///// キャッシュを破棄する
	///// </summary>
	//public void ClearCache()
	//{
	//	_textureCache.Clear();
	//}
}
