﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// タップのエフェクトを管理するクラス
/// </summary>
public sealed class TapEffect : MonoBehaviour
{
    [SerializeField] TapEffectPool _tapEffectPool;        
    [SerializeField] Camera _camera;

    public void Init()
	{
        _tapEffectPool.Init();
    }

    public void CallUpdate()
    {
        if (Input.GetMouseButtonDown(0))
        {
            // マウスのワールド座標までパーティクルを移動し、パーティクルエフェクトを1つ生成する
            var pos = _camera.ScreenToWorldPoint(Input.mousePosition + _camera.transform.forward * 10);
            _tapEffectPool.Play(pos);
        }
    }
}
