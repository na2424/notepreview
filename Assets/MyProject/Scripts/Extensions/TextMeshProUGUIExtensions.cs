﻿using TMPro;
using UnityEngine;

public static class TextMeshProUGUIExtensions
{
	const char ZERO = '0';

	// ゼロ埋め文字キャッシュ、最大数に注意
	static char[] _characters = new char[10];

	/// <summary>
	/// GCが発生しない0埋め数字テキスト更新.
	/// (エディターではGCが発生する点に注意)
	/// </summary>
	/// <param name="textMeshPro"></param>
	/// <param name="value"></param>
	/// <param name="zeroFillLength"></param>

	public static void NonAllocateSetText(this TextMeshProUGUI textMeshPro, int value, int zeroFillLength)
	{
		var digit = Digit(value);

		if (digit < zeroFillLength)
		{
			digit = zeroFillLength;
		}

		for (int i = digit - 1; i >= 0; i--)
		{
			_characters[i] = (char)((value % 10) + ZERO);
			value /= 10;
		}

		textMeshPro.SetCharArray(_characters, 0, digit);
	}

	/// <summary>
	/// GCが発生しない0埋め数字テキスト更新.
	/// (エディターではGCが発生する点に注意)
	/// </summary>
	/// <param name="textMeshPro"></param>
	/// <param name="value"></param>
	/// <param name="zeroFillLength"></param>
	public static void NonAllocateSetText(this TextMeshProUGUI textMeshPro, uint value, int zeroFillLength)
	{
		NonAllocateSetText(textMeshPro, (int)value, zeroFillLength);
	}

	static int Digit(int num)
	{
		// Mathf.Log10(0)はNegativeInfinityを返すため、別途処理する。
		return (num == 0) ? 1 : ((int)Mathf.Log10(num) + 1);
	}
}
