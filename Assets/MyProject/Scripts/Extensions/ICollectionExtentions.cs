﻿using System.Collections.Generic;

namespace UBL
{
	public static class ICollectionExtentions
	{

		/// <summary>
		/// </summary>
		/// <returns><c>true</c> if is null or empty the specified target; otherwise, <c>false</c>.</returns>
		/// <param name="target">Target.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static bool IsNullOrEmpty<T>(this ICollection<T> target)
		{
			return target == null || target.Count == 0;
		}

		/// <summary>
		/// Is Not Null Of Empty
		/// </summary>
		/// <param name="target"></param>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		public static bool IsNotNullOrEmpty<T>(this ICollection<T> target)
		{
			return !IsNullOrEmpty(target);
		}
	}
}

