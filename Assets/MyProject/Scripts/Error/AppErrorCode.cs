using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AppErrorCode
{
	public static class SystemError
	{
		public const int Unexpected = 100;
		public const int NotAllowIP = 101;
		public const int NotAllowRequestMethod = 200;
		public const int UndefinedEndPoint = 201;
		public const int UndefinedAppName = 202;
		public const int LackAppName = 203;
		public const int LackAppVersion = 204;
		public const int GatewayError = 1000;
		public const int ReverseProxyError = 1100;
	}

	public static class ApiCommon
	{
		public const int NotSetVersion = 205;
		public const int InvalidParam = 500;
		public const int ConnectionFailed = 1101;
		public const int ConnectionTimeout = 1102;
		public const int ConnectionClose = 1103;
	}


}
