using System;

[Serializable]
public class DisplayOption
{
	public float Dimmer = 0.25f;
	public bool BeatBar = true;
	public bool NotesEffect = true;
	public bool VSync = true;
	public bool IsShowFps = false;
	public int TargetFrameRate = 120;
}
