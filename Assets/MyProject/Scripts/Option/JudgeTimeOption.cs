﻿using UnityEngine;

[System.Serializable]
public class JudgeTimeOption
{
	public float BriliantTime = Constant.JudgeTime.BRILLIANT_TIME;
	public float GreatTime = Constant.JudgeTime.GREAT_TIME;
	public float FastTime = Constant.JudgeTime.FAST_TIME;
	public float BadTime = Constant.JudgeTime.BAD_TIME;
	public float LongRevisionTime = Constant.JudgeTime.LONG_REVISION_TIME;
	public float LongRevisionDistance = Constant.JudgeTime.LONG_REVISION_DISTANCE;
	public float FuzzyStartTime = Constant.JudgeTime.FUZZY_START_TIME;
	public bool CanSave = false;

	public bool IsCustom =>
		BriliantTime != Constant.JudgeTime.BRILLIANT_TIME ||
		GreatTime != Constant.JudgeTime.GREAT_TIME ||
		FastTime != Constant.JudgeTime.FAST_TIME ||
		BadTime != Constant.JudgeTime.BAD_TIME ||
		LongRevisionTime != Constant.JudgeTime.LONG_REVISION_TIME ||
		LongRevisionDistance != Constant.JudgeTime.LONG_REVISION_DISTANCE ||
		FuzzyStartTime != Constant.JudgeTime.FUZZY_START_TIME;

	/// <summary>
	/// 判定時間で一番長い時間を取得する
	/// </summary>
	/// <returns>判定が行なわれる時間</returns>
	public float GetHitTime()
	{
		float hitTime = BadTime;

		if (hitTime < FastTime)
		{
			hitTime = FastTime;
		}

		if (hitTime < GreatTime)
		{
			hitTime = GreatTime;
		}

		if (hitTime < BriliantTime)
		{
			hitTime = BriliantTime;
		}

		return hitTime;
	}

	/// <summary>
	/// ロング終端時間補正を加えた判定時間で一番長い時間を取得する
	/// </summary>
	/// <returns>判定が行なわれる時間</returns>
	public float GetLongEndHitTime()
	{
		float hitTime = BadTime;

		if (hitTime < FastTime)
		{
			hitTime = FastTime;
		}

		if (hitTime < GreatTime + LongRevisionTime)
		{
			hitTime = GreatTime + LongRevisionTime;
		}

		if (hitTime < BriliantTime + LongRevisionTime)
		{
			hitTime = BriliantTime + LongRevisionTime;
		}

		return hitTime;
	}
}
