﻿/// <summary>
/// 音量オプション情報
/// </summary>
[System.Serializable]
public class VolumeOption
{
    public bool MusicMute = false;
    public float MusicVolume = 0.8f;
    public bool TouchSEMute = false;
    public float TouchSEVolume = 0.6f;
    public bool SystemSEMute = false;
    public float SystemSEVolume = 0.6f;
}