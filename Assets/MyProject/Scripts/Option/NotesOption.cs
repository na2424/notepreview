using System;

[Serializable]
public class NotesOption
{
	public float HiSpeed = 5.0f;
	public float Size = 1f;
	public float Timing = 0f;
	public bool AutoTiming = false;
	public float FrameRevision = 0f;
	public int PollingRate = 300;
	public JudgeAreaType JudgeAreaType = JudgeAreaType.Screen;
	public NotesScrollType NotesScrollType = NotesScrollType.Decelerate;
	public string NoteSkinsName = Constant.Note.DEFAULT_NOTESKINS_NAME;
	public string TouchSeName = Constant.Note.DEFAULT_NOTESKINS_NAME;
}
