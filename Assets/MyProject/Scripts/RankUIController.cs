using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public sealed class RankUIController : MonoBehaviour
{
	[SerializeField] Image _rankImage = default;
	[SerializeField] RefSpriteScriptableObject _rankSprite;

	public void Init()
	{
		_rankImage.gameObject.SetActive(false);
	}

	public void UpdateUI(bool isShow, int index = 0)
	{
		_rankImage.gameObject.SetActive(isShow);

		if(isShow)
		{
			_rankImage.sprite = _rankSprite.GetSprite(index);
		}
	}

}
