﻿using UnityEngine;
using UnityEngine.Pool;

public enum NotesScrollType
{
	Constant,
	Decelerate
}

/// <summary>
/// Noteの1つあたりの情報を管理するクラス
/// </summary>
public sealed class NoteController : MonoBehaviour
{
	//---------------------------------
	// インスペクタまたは外部から設定.
	//---------------------------------
	[SerializeField] Transform _transform;
	[SerializeField] MeshRenderer _renderer;

	//------------------
	// キャッシュ.
	//------------------
	ObjectPool<NoteController> _pool;
	MaterialPropertyBlock _mpb;

	bool _isReleased;
	bool _isCmod;
	double _beatPosition = -1f;
	double _justTime = -1d;
	double _hitTime = 1d;
	int _lane = -1;
	int _noteIndex = -1;

	NotesScrollType _notesScrollType = NotesScrollType.Constant;

	LongController _longController;

	static float _normalizedSpeed = 1f;

	//------------------
	// 定数.
	//------------------
	static readonly int MAIN_TEX_ID = Constant.ShaderProperty.MainTexId;
	static readonly float BEAT_DISTANCE = Constant.Note.BEAT_DISTANCE;
	static readonly float TIME_DISTANCE = Constant.Note.TIME_DISTANCE;
	static readonly float LANE_DISTANCE = Constant.Note.LANE_DISTANCE;
	static readonly float LANE_LENGTH = Constant.Note.LANE_LENGTH;
	static readonly float OFFSET_X = Constant.Note.OFFSET_X;
	static readonly float OFFSET_Z = Constant.Note.OFFSET_Z;

	//------------------
	// プロパティ.
	//------------------
	public bool IsReleased => _isReleased;
	public int NoteIndex => _noteIndex;
	public bool IsMissed { get; private set; } = false;
	public NoteType NoteType { get; private set; }
	public LongController LongController { get; private set; }
	public float PosX => _transform.position.x;
	public double JustTime => _justTime;
	public int UpTouchId { get; set; } = -1;
	public static float VisibleBeat => (LANE_LENGTH + OFFSET_Z) /
		(BEAT_DISTANCE * (Constant.Note.FIXED_BPM / GameManager.Instance.SelectSongInfo.BaseBpm) * SpeedStretchRatio.CurrentValue * GameManager.Instance.NotesOption.HiSpeed);
	public static double VisibleTime => (LANE_LENGTH + OFFSET_Z) /
		(TIME_DISTANCE * SpeedStretchRatio.CurrentValue * GameManager.Instance.NotesOption.HiSpeed);

	AnimationCurve NoteAnimationCurve => GameManager.Instance.NoteScrollAnimationCurve;
	AnimationCurve InverseNoteAnimationCurve => GameManager.Instance.InverseAnimationCurve;

	float _hiSpeed => GameManager.Instance.NotesOption.HiSpeed;

	public void Init(ObjectPool<NoteController> pool)
	{
		_pool = pool;
		_mpb = new MaterialPropertyBlock();
		_normalizedSpeed = Constant.Note.FIXED_BPM / GameManager.Instance.SelectSongInfo.BaseBpm;
		_hitTime = GameManager.Instance.JudgeTimeOption.GetLongEndHitTime();
		_isCmod = GameManager.Instance.IsCmod;
		_notesScrollType = GameManager.Instance.NotesOption.NotesScrollType;
	}

	public void SetParam(float beatPosition, double justTime, int lane, Texture noteTex, NoteType noteType, int noteIndex)
	{
		_isReleased = false;
		_beatPosition = beatPosition;
		_justTime = justTime;
		_lane = lane;
		_noteIndex = noteIndex;
		// マテリアルのInstanceを避ける.
		_mpb.SetTexture(MAIN_TEX_ID, noteTex);
		_renderer.SetPropertyBlock(_mpb);
		//_renderer.material.SetTexture(_mainTexId, noteTex);
		IsMissed = false;
		NoteType = noteType;
	}

	public void SetLongController(LongController longController)
	{
		if (
			NoteType == NoteType.LongStart ||
			NoteType == NoteType.FuzzyLongStart
		)
		{
			_longController = longController;
		}
	}

	public void SetLongTouchId(int touchId)
	{
		if (_longController != null)
		{
			_longController.SetTouchId(touchId);
		}
	}

	public void OnUpdate(double beat, double musicTime, float speedStretchRatio = 1f)
	{
		if (_isReleased || !gameObject.activeSelf)
		{
			return;
		}

		if (_isCmod)
		{
			UpdateCmodNotePosition(musicTime, speedStretchRatio);
		}
		else
		{
			UpdateNotePosition(beat, speedStretchRatio);
		}

		CheckReleaseNote(musicTime);
	}
	void UpdateCmodNotePosition(double musicTime, float speedStretchRatio)
	{
		double diffTime = _justTime - musicTime;

		// 位置
		float posZ = (float)(diffTime * TIME_DISTANCE * _hiSpeed * speedStretchRatio - (OFFSET_Z));

		if (_notesScrollType == NotesScrollType.Decelerate)
		{
			posZ = NoteAnimationCurve.Evaluate(posZ);
		}

		if (float.IsNaN(posZ))
		{
			posZ = 100f;
		}

		_transform.localPosition = new Vector3(
			_lane * LANE_DISTANCE - OFFSET_X,
			0f,
			posZ
		);

		// 回転
		float rot = InverseNoteAnimationCurve.Evaluate(_transform.localPosition.z) * 0.78f;

		if (float.IsNaN(rot))
		{
			rot = _transform.localPosition.z * 0.78f;
		}

		_transform.localRotation = Quaternion.Euler(
			90f - rot,
			0f,
			0f
		);
	}

	void UpdateNotePosition(double beat, float speedStretchRatio)
	{
		double diffBeat = _beatPosition - beat;

		// 位置
		float posZ = (float)(diffBeat * BEAT_DISTANCE * _normalizedSpeed * _hiSpeed * speedStretchRatio - (OFFSET_Z));

		if (_notesScrollType == NotesScrollType.Decelerate)
		{
			posZ = NoteAnimationCurve.Evaluate(posZ);
		}

		if (float.IsNaN(posZ))
		{
			posZ = 100f;
		}

		_transform.localPosition = new Vector3(
			_lane * LANE_DISTANCE - OFFSET_X,
			0f,
			posZ
		);

		// 回転
		float rot = InverseNoteAnimationCurve.Evaluate(_transform.localPosition.z) * 0.78f;

		if (float.IsNaN(rot))
		{
			rot = _transform.localPosition.z * 0.78f;
		}

		_transform.localRotation = Quaternion.Euler(
			90f - rot,
			0f,
			0f
		);
	}

	void CheckReleaseNote(double musicTime)
	{
		if (_justTime + _hitTime < musicTime)
		{
			Release(true);
		}
	}

	public void Release(bool isMissed = false)
	{
		if (_isReleased || !gameObject.activeSelf)
		{
			return;
		}

		IsMissed = isMissed;
		_isReleased = true;
		_pool.Release(this);
	}

	public void UpdateNoteScrollType()
	{
		_notesScrollType = GameManager.Instance.NotesOption.NotesScrollType;
	}
}
