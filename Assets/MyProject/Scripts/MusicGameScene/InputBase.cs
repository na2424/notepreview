﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// タッチ入力の抽象基底クラス
/// (実際のタッチ入力またはオートに対応)
/// </summary>
public abstract class InputBase : MonoBehaviour
{
	//MEMO: インターフェース化したIDictionaryやSortedDictionaryを
	// foreachで回すとGetEnumerator()でGCAllocateが発生する為、
	// 素のDictionaryにしている
	protected Dictionary<int, TouchData> _touchData = new Dictionary<int, TouchData>();
	public Dictionary<int, TouchData> TouchData => _touchData;
	public abstract void CallUpdate(double musicTime);
	public bool HasTouch => _touchData.Count > 0;
	public void ClearInputData()
	{
		TouchData.Clear();
	}

	public virtual void OnFinalize()
	{
		_touchData.Clear();
		_touchData = null;
	}
}
