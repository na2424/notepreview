﻿using UnityEngine;
using UnityEngine.Pool;

public sealed class BeatBarController : MonoBehaviour
{
	//------------------
	// キャッシュ.
	//------------------
	ObjectPool<BeatBarController> _pool;
	Transform _transform;
	double _beatPosition = -1f;
	bool _isReleased = false;
	double _justTime = -1d;
	float _normalizedSpeed = 0f;
	double _hitTime = 1d;
	bool _isCmod = false;

	NotesScrollType _notesScrollType;
	AnimationCurve _animationCurve;

	//------------------
	// 定数.
	//------------------
	static readonly float BEAT_DISTANCE = Constant.Note.BEAT_DISTANCE;
	static readonly float TIME_DISTANCE = Constant.Note.TIME_DISTANCE;
	static readonly float OFFSET_Z = Constant.Note.OFFSET_Z;

	//------------------
	// プロパティ.
	//------------------
	float _hiSpeed => GameManager.Instance.NotesOption.HiSpeed;

	public void Init(ObjectPool<BeatBarController> pool)
	{
		_transform = transform;
		_pool = pool;
		_normalizedSpeed = Constant.Note.FIXED_BPM / GameManager.Instance.SelectSongInfo.BaseBpm;
		_hitTime = GameManager.Instance.JudgeTimeOption.GetHitTime();
		_isCmod = GameManager.Instance.IsCmod;
		_notesScrollType = GameManager.Instance.NotesOption.NotesScrollType;
		_animationCurve = GameManager.Instance.NoteScrollAnimationCurve;
	}

	public void SetParam(float beatPosition, double justTime)
	{
		_isReleased = false;
		_beatPosition = beatPosition;
		_justTime = justTime;
	}

	public void OnUpdate(double beat, double musicTime, float speedStretchRatio)
	{
		if (_isReleased || !gameObject.activeSelf)
		{
			return;
		}

		if (_isCmod)
		{
			UpdateCmodPosition(musicTime, speedStretchRatio);
		}
		else
		{
			UpdatePosition(beat, speedStretchRatio);
		}

		CheckRelease(musicTime);
	}

	void UpdateCmodPosition(double musicTime, float speedStretchRatio)
	{
		double diffTime = _justTime - musicTime;
		float posZ = (float)(diffTime * TIME_DISTANCE * _hiSpeed * speedStretchRatio - OFFSET_Z);

		if (_notesScrollType == NotesScrollType.Decelerate)
		{
			posZ = _animationCurve.Evaluate(posZ);
		}

		_transform.localPosition = new Vector3(
			0f,
			0f,
			posZ
		);

		_transform.localRotation = Quaternion.Euler(
			90f - (_transform.localPosition.z * 0.75f),
			0f,
			0f
		);
	}

	void UpdatePosition(double beat, float speedStretchRatio)
	{
		double diffPosition = _beatPosition - beat;
		float posZ = (float)(diffPosition * BEAT_DISTANCE * _normalizedSpeed * _hiSpeed * speedStretchRatio - OFFSET_Z);

		if (_notesScrollType == NotesScrollType.Decelerate)
		{
			posZ = _animationCurve.Evaluate(posZ);
		}

		if (float.IsNaN(posZ))
		{
			posZ = 100f;
		}

		_transform.localPosition = new Vector3(
			0f,
			0f,
			posZ
		);

		_transform.localRotation = Quaternion.Euler(
			90f - (_transform.localPosition.z * 0.75f),
			0f,
			0f
		);
	}

	void CheckRelease(double musicTime)
	{
		if (_justTime + _hitTime < musicTime)
		{
			Release();
		}
	}

	public void Release()
	{
		if (_isReleased)
		{
			return;
		}

		_isReleased = true;
		_pool.Release(this);
	}

	public void UpdateNoteScrollType()
	{
		_notesScrollType = GameManager.Instance.NotesOption.NotesScrollType;
	}
}
