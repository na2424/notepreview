using UnityEngine;

public sealed class FilterColorController : MonoBehaviour
{
	[SerializeField] MeshRenderer _meshRenderer;

	public void Init()
	{
		var mat = _meshRenderer.material;
		mat.SetColor(Constant.ShaderProperty.TopColor, new Color(0, 0, 0, 0));
		mat.SetColor(Constant.ShaderProperty.ButtomColor, new Color(0,0,0,0.5f));
	}
}
