﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public sealed class BeatBarObjectPool : MonoBehaviour
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[SerializeField] BeatBarController _beatBarControllerPrefab;

	//------------------
	// キャッシュ.
	//------------------
	ObjectPool<BeatBarController> _pool = null;
	List<BeatBarController> _currentActiveBar = new List<BeatBarController>();
	Transform _transform;

	//------------------
	// 定数.
	//------------------
	// 初期のプールサイズ
	const int DEFAULT_CAPACITY = 16;
	// プールサイズを最大どれだけ大きくするか
	const int MAX_SIZE = 32;

	//------------------
	// プロパティ.
	//------------------
	public List<BeatBarController> CurrentActiveBar => _currentActiveBar;

	public void Init()
	{
		_transform = transform;
	}

	public ObjectPool<BeatBarController> Pool
	{
		get
		{
			if (_pool == null)
			{
				_pool = new ObjectPool<BeatBarController>(OnCreatePoolObject, OnTakeFromPool, OnReturnedToPool, OnDestroyPoolObject, false, DEFAULT_CAPACITY, MAX_SIZE);
			}

			return _pool;
		}
	}

	BeatBarController OnCreatePoolObject()
	{
		var controller = Instantiate(_beatBarControllerPrefab, _transform);
		controller.Init(Pool);
		_currentActiveBar.Add(controller);
		return controller;
	}

	void OnTakeFromPool(BeatBarController controller)
	{
		controller.gameObject.SetActive(true);
		_currentActiveBar.Add(controller);
	}

	void OnReturnedToPool(BeatBarController controller)
	{
		_currentActiveBar.Remove(controller);
		controller.gameObject.SetActive(false);
	}

	void OnDestroyPoolObject(BeatBarController controller)
	{
		controller.gameObject.SetActive(false);
		Destroy(controller.gameObject);
	}
}
