﻿using System.Collections.Generic;

/// <summary>
/// BPMが絡む計算を担当するクラス
/// </summary>
public sealed class BpmHelper
{
	float[] _bpmPositions;
	float[] _bpms;

	double _globalOffset;
	double _offset;

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name="songInfo">SongInfo</param>
	public void Init(SongInfo songInfo)
	{
		// 元のSongInfoを変更しないようにListを複製する.
		List<float> tempBpmPositions = new List<float>(songInfo.BpmPositions);
		tempBpmPositions.Add(float.MaxValue);

		_bpmPositions = tempBpmPositions.ToArray();
		_bpms = songInfo.Bpms.ToArray();
		_offset = songInfo.Offset;
		_globalOffset = GameManager.Instance.NotesOption.Timing;
	}

	/// <summary>
	/// 曲の再生時間から拍数を求める
	/// (BPM変化対応)
	/// </summary>
	/// <param name="musicTime">再生時間</param>
	/// <returns>拍数</returns>
	public double TimeToBeat(double musicTime)
	{
		double beat = 0d;
		double totalBpmChangeTime = 0d;

		for (int i = 0; i < _bpmPositions.Length - 1; i++)
		{
			double diff = (-_globalOffset + _offset + musicTime - totalBpmChangeTime) * _bpms[i] / 60f;

			if ((beat + diff) > _bpmPositions[i + 1])
			{
				beat += _bpmPositions[i + 1] - beat;
				totalBpmChangeTime += (_bpmPositions[i + 1] - _bpmPositions[i]) * 60d / _bpms[i];
			}
			else
			{
				beat += diff;
				break;
			}
		}

		return beat;
	}

	/// <summary>
	/// 拍数から時間を求める
	/// (BPM変化対応)
	/// </summary>
	/// <param name="beat">拍数</param>
	/// <returns>時間</returns>
	public double BeatToTime(double beat)
	{
		double time = 0d;

		for (int i = 0; i < _bpmPositions.Length - 1; i++)
		{
			if (beat > _bpmPositions[i + 1])
			{
				time += (_bpmPositions[i + 1] - _bpmPositions[i]) * 60d / _bpms[i];
			}
			else
			{
				time += ((beat - _bpmPositions[i]) * 60d / _bpms[i]) - _offset + _globalOffset;
				break;
			}
		}

		return time;
	}

}
