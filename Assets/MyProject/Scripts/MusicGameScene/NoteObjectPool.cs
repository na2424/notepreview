﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

/// <summary>
/// ノートのオブジェクトを使い回せるようにするクラス.
/// TODO:実質的な判定処理はここに入っているが分離する予定
/// </summary>
public sealed class NoteObjectPool : MonoBehaviour
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[SerializeField] NoteController _noteControllerPrefab;

	public event Action<JudgeType> OnMissedNote = null;

	//------------------
	// キャッシュ.
	//------------------
	ObjectPool<NoteController> _pool = null;
	List<NoteController> _currentActiveNote = new List<NoteController>();
	Transform _transform;

	//------------------
	// 定数.
	//------------------
	// 初期のプールサイズ
	const int DEFAULT_CAPACITY = 16;
	// プールサイズを最大どれだけ大きくするか
	const int MAX_SIZE = 100;

	//------------------
	// プロパティ.
	//------------------
	public List<NoteController> CurrentActiveNote => _currentActiveNote;

	public ObjectPool<NoteController> Pool
	{
		get
		{
			if (_pool == null)
			{
				_pool = new ObjectPool<NoteController>(OnCreatePoolObject, OnTakeFromPool, OnReturnedToPool, OnDestroyPoolObject, false, DEFAULT_CAPACITY, MAX_SIZE);
			}

			return _pool;
		}
	}


	public void Init()
	{
		_transform = transform;
	}

	NoteController OnCreatePoolObject()
	{
		var noteController = Instantiate(_noteControllerPrefab, _transform);
		noteController.Init(Pool);
		noteController.transform.localScale = _noteControllerPrefab.transform.localScale * GameManager.Instance.NotesOption.Size;
		_currentActiveNote.Add(noteController);
		return noteController;
	}

	void OnTakeFromPool(NoteController noteController)
	{
		_currentActiveNote.Add(noteController);
		noteController.gameObject.SetActive(true);
	}

	void OnReturnedToPool(NoteController noteController)
	{
		if (noteController.IsMissed)
		{
			OnMissedNote?.Invoke(JudgeType.Missed);
		}
		_currentActiveNote.Remove(noteController);
		noteController.gameObject.SetActive(false);
	}

	void OnDestroyPoolObject(NoteController noteController)
	{
		//Debug.Log("Note Destroy");
		_currentActiveNote.Remove(noteController);
		Destroy(noteController.gameObject);
	}

	public void ChangeNoteSize()
	{
		foreach(var note in _currentActiveNote)
		{
			note.transform.localScale = _noteControllerPrefab.transform.localScale * GameManager.Instance.NotesOption.Size;
		}
	}

	public void AddMissed()
	{
		OnMissedNote?.Invoke(JudgeType.Missed);
	}

}
