﻿using Cysharp.Text;
using Cysharp.Threading.Tasks;
using Dialog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using UnityEngine;

/// <summary>
/// 楽曲の再生・終了を管理するクラス
/// </summary>
public sealed class MusicManager : MonoBehaviour
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[SerializeField] AudioSource _audioSource;
	public event Action OnFinishMusic = null;

	//------------------
	// キャッシュ.
	//------------------
	AudioClip _audioClip;
	double _beforeTime = 0d;
	double _cacheTime = 0d;
	double _lastNoteTime = 0d;
	double _minusPlayTime = -1d;
	float _maxVolume = 0.5f;
	bool _isPlay = false;
	bool _isMinusPlayTime = false;
	double _frequency = 44100d;

	float _smoothedTimeSamples = 0f;
	float _prevFrameSamples = 0f;
	int _counter = 0;

	//------------------
	// 定数.
	//------------------
	// 曲が終了または最後のノートから何秒待機して終了するかの時間
	const float DELAY_ENDTIME = 2f;
	// 開始してから曲を流し始めるまでの空白時間
	const double START_MARGIN = 1.1d;
	// 途中再生の時に開始してから指定した時間になるまでの猶予時間
	const double MID_START_MARGIN = 2.2d;
	// ノーツの動きを滑らかにする為にDeltaTime加算で動かしているが、
	// 少しずつ曲の再生時間とずれていくので指定フレーム毎に再生時間と同期を取る。そのフレーム値の定数。
	const int COUNTER_RESET_NUM = 180;

	//------------------
	// プロパティ.
	//------------------
	public bool IsPlay => _isPlay;
	public double MusicEndTime => _audioClip.length < _lastNoteTime ? _audioClip.length : _lastNoteTime;

	/// <summary>
	/// 非同期の初期化処理
	/// </summary>
	/// <param name="ct">キャンセルトークン</param>
	/// <param name="directoryPath">フォルダパス</param>
	/// <param name="fileName">ファイル名</param>
	/// <param name="lastNoteTime">最後のノーツの時間</param>
	/// <param name="startTime">開始時間</param>
	/// <returns></returns>
	public async UniTask InitAsync(CancellationToken ct, string directoryPath, string fileName, double lastNoteTime, float startTime = 0f)
	{
		try
		{
			var path = ZString.Concat("file://", Path.Combine(directoryPath, fileName));
			_audioClip = await AudioClipLoader.LoadAsync(ct, path, false);
		}
		catch (Exception ex)
		{
			var builder = new DialogParametor.Builder("楽曲読み込みエラー", ex.Message);
			builder.AddDefaultAction("戻る", () => GameManager.Instance.ChangeScene(SceneName.SelectMusic));
			builder.AddCallbackOnAutoClosed(() => GameManager.Instance.ChangeScene(SceneName.SelectMusic));
			DialogManager.Instance.Open(builder.Build());
			return;
		}

		if(_audioClip == null)
		{
			var builder = new DialogParametor.Builder("楽曲読み込みエラー", "楽曲ファイルが見つかりませんでした");
			builder.AddDefaultAction("戻る", () => GameManager.Instance.ChangeScene(SceneName.SelectMusic));
			builder.AddCallbackOnAutoClosed(() => GameManager.Instance.ChangeScene(SceneName.SelectMusic));
			DialogManager.Instance.Open(builder.Build());
			return;
		}

		_lastNoteTime = lastNoteTime;
		_frequency = _audioClip.frequency;

		SetParam(_audioClip, startTime);
	}

	/// <summary>
	/// 各パラメータを設定
	/// </summary>
	/// <param name="audioClip">AudioClip</param>
	/// <param name="startTime">開始時間</param>
	void SetParam(AudioClip audioClip, float startTime)
	{
		bool isMidStart = startTime > 0f;

		_minusPlayTime = isMidStart ? startTime - MID_START_MARGIN : -START_MARGIN;
		_maxVolume = GameManager.Instance.VolumeOption.MusicVolume;

		_audioSource.mute = GameManager.Instance.VolumeOption.MusicMute;
		_audioSource.volume = isMidStart ? 0f : _maxVolume;
		_audioSource.clip = audioClip;
		_audioSource.loop = false;

		if (isMidStart)
		{
			_counter = COUNTER_RESET_NUM - 5;
			_audioSource.time = (float)_minusPlayTime;
			StartCoroutine(VolumeChangeRoutine(startTime));
		}

		_isPlay = true;
		_isMinusPlayTime = true;
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
	}

	/// <summary>
	/// 途中再生の場合に呼ばれる.
	/// 少しずつ音量を上げることにより、フェードインさせる
	/// </summary>
	/// <param name="time"></param>
	/// <returns></returns>
	IEnumerator VolumeChangeRoutine(float time)
	{
		var length = (float)MID_START_MARGIN;

		while (_audioSource.time < time)
		{
			float t = Mathf.Clamp01((_audioSource.time - time + length) / length);
			_audioSource.volume = Mathf.Lerp(0f, _maxVolume, t);
			yield return null;
		}

		_audioSource.volume = _maxVolume;
	}

	/// <summary>
	/// 毎フレーム呼ばれる更新処理
	/// </summary>
	public void CallUpdate()
	{
		if (_audioSource.clip == null)
		{
			return;
		}

		if (_isMinusPlayTime && _isPlay)
		{
			_minusPlayTime += Time.deltaTime;

			if (_minusPlayTime >= 0)
			{
				_audioSource.Play();
				_cacheTime = Time.realtimeSinceStartupAsDouble;
				_isMinusPlayTime = false;
				//Debug.Log(_audioSource.timeSamples);
			}

			return;
		}

		if (_isPlay)
		{
			var deltaSamples = _counter == 0
						? (_audioSource.timeSamples - _prevFrameSamples)
						: _audioClip.frequency/* * _audioSource.pitch*/ * Time.deltaTime;

			_smoothedTimeSamples += deltaSamples;
			_prevFrameSamples = _smoothedTimeSamples;

			_counter = ++_counter % COUNTER_RESET_NUM;
		}

		// 曲が終了または最後のノートから２秒後に譜面を終了させる
		if ((_audioSource.time >= _audioClip.length - Time.deltaTime) || (_audioSource.time >= _lastNoteTime + DELAY_ENDTIME))
		{
			_isPlay = false;
			OnFinishMusic.Invoke();
			_audioSource.Stop();

			Screen.sleepTimeout = SleepTimeout.SystemSetting;
		}
	}

	/// <summary>
	/// 現在の再生時間を取得
	/// </summary>
	/// <returns>再生時間</returns>
	public double GetMusicTime()
	{
		if (_isMinusPlayTime)
		{
			return _minusPlayTime;
		}

		double time = _smoothedTimeSamples / _frequency;

		if (!Calculate.FastApproximately(time, _beforeTime, 0.002d))
		{
			_beforeTime = time;
			_cacheTime = Time.realtimeSinceStartupAsDouble;
			//Debug.Log("Calculate.FastApproximately In");
		}
		else
		{
			//_audioSource.time(timeSamples)が正確な値ではなく、飛び飛びの値を返すので補正を加える.
			time += Time.realtimeSinceStartupAsDouble - _cacheTime;
		}

		return time;
	}

	/// <summary>
	/// 一時停止
	/// </summary>
	public void Pause()
	{
		_isPlay = false;

		if (!_isMinusPlayTime)
		{
			_audioSource.Pause();
		}

		Screen.sleepTimeout = SleepTimeout.SystemSetting;
	}

	/// <summary>
	/// 再生を再開
	/// </summary>
	public void Play()
	{
		_isPlay = true;

		if (!_isMinusPlayTime)
		{
			_audioSource.Play();
			_cacheTime = Time.realtimeSinceStartupAsDouble;
		}

		Screen.sleepTimeout = SleepTimeout.NeverSleep;
	}

	void OnDestroy()
	{
		OnFinishMusic = null;
	}

}
