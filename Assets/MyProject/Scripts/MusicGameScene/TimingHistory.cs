using System.Collections.Generic;

public sealed class TimingHistory
{
	List<float> _musicTime = new List<float>();
	List<float> _timing = new List<float>();
	List<JudgeType> _judgeTypes = new List<JudgeType>();

	public IReadOnlyList<float> MusicTime => _musicTime;
	public IReadOnlyList<float> Timing => _timing;
	public IReadOnlyList<JudgeType> JudgeTypes => _judgeTypes;

	public void Add(double musicTime, double timing, JudgeType judgeType)
	{
		_musicTime.Add((float)musicTime);
		_timing.Add((float)timing);
		_judgeTypes.Add(judgeType);
	}
}

public class NoFuzzyTimingHistory
{
	List<float> _timing = new List<float>();
	public IReadOnlyList<float> Timing => _timing;
	public void Add(double timing)
	{
		_timing.Add((float)timing);
	}
}