﻿using Cysharp.Threading.Tasks;
using Dialog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using UnityEngine;
using UnityEngine.Pool;

public sealed class SequenceReader
{
	const int MAX_LANE = 7;

	/// <summary>
	/// 譜面の非同期読み込み
	/// </summary>
	/// <param name="ct">キャンセルトークン</param>
	/// <param name="directoryPath">ディレクトリパス</param>
	/// <returns>譜面情報のUniTask</returns>
	public async UniTask<Sequence> ReadAsync(CancellationToken ct, SongInfo songInfo)
	{
		try
		{
			return LoadSequence(await ReadFileText(ct, songInfo.DirectoryPath), songInfo);
		}
		catch (Exception ex)
		{
			var builder = new DialogParametor.Builder("エラー", ex.Message);
			builder.AddDefaultAction("譜面選択に戻る", () => GameManager.Instance.ChangeScene(SceneName.SelectMusic));
			builder.AddCallbackOnAutoClosed(() => GameManager.Instance.ChangeScene(SceneName.SelectMusic));
			DialogManager.Instance.Open(builder.Build());
		}

		return null;
	}

	/// <summary>
	/// ディレクトリ内の.dlファイルからテキストを取得.
	/// テスト用に.txtも読み込めるようにしています
	/// </summary>
	/// <param name="ct">キャンセルトークン</param>
	/// <param name="directoryPath">ディレクトリパス</param>
	/// <returns>テキストのUniTask</returns>
	async UniTask<string> ReadFileText(CancellationToken ct, string directoryPath)
	{
		string[] dlfiles = null;
		var extensions = new string[] { "*.dl", "*.txt" };

		// .dlまたは.txtのファイルを検索
		foreach (string ext in extensions)
		{
			dlfiles = Directory.GetFiles(directoryPath, ext, System.IO.SearchOption.TopDirectoryOnly);

			if (dlfiles.Length > 0)
			{
				break;
			}
		}

		var path = Path.Combine(directoryPath, dlfiles[0]);
		string result = string.Empty;

		FileInfo file = null;

		// ファイルがある場合
		if (File.Exists(path))
		{
			file = new FileInfo(path);
		}
		else
		{
			Debug.LogError("ファイルがありません:" + path);
			return string.Empty;
		}

		using (StreamReader sr = new StreamReader(file.OpenRead(), Encoding.UTF8))
		{
			try
			{
				result = await sr.ReadToEndAsync()
					.AsUniTask()
					.AttachExternalCancellation(ct);
			}
			catch (OperationCanceledException e)
			{
				Debug.LogWarning("キャンセルされました: " + e.Message);
				return string.Empty;
			}
		}

		return result;
	}

	string DifficultyToString(DifficultyType type) => type switch
	{
		DifficultyType.Easy => "Easy:",
		DifficultyType.Normal => "Normal:",
		DifficultyType.Hard => "Hard:",
		DifficultyType.Extra => "Extra:",
		DifficultyType.Lunatic => "Lunatic:",
		_ => "Lunatic:"
	};

	/// <summary>
	/// テキストからシーケンス(譜面)データを読み込む.
	/// </summary>
	/// <param name="text">テキスト</param>
	/// <returns>譜面情報</returns>
	Sequence LoadSequence(string text, SongInfo songInfo)
	{
		Sequence sequence = new Sequence();
		StringReader rs = new StringReader(text);
		int noteReadState = 0;
		int bar = 0;
		float timeSignature = 4f;

		string difficulty = DifficultyToString(GameManager.Instance.SelectDifficulty);

		using (CollectionPool<List<string>, string>.Get(out var barText))
		{
			// ストリームの末端まで繰り返す
			while (rs.Peek() > -1)
			{
				// 一行読み込む
				var lineText = rs.ReadLine().Trim();

				if (noteReadState == 0 && lineText.CustomStartsWith("#NOTES:"))
				{
					noteReadState = 1;
					continue;
				}

				if (noteReadState == 1 && (lineText.CustomStartsWith("dance-double:") || lineText.CustomStartsWith("dankaglike:")))
				{
					noteReadState = 2;
					continue;
				}

				if (noteReadState == 2 && lineText.CustomStartsWith(difficulty))
				{
					noteReadState = 3;

					// SplitBPMS
					lineText = rs.ReadLine().Trim();

					if (lineText.CustomStartsWith("SplitBPMS"))
					{
						var bpmData = lineText.GetBetweenChars('[', ']').Split(new char[] { '=', ',' }, StringSplitOptions.RemoveEmptyEntries);

						if (bpmData.Length >= 2)
						{
							songInfo.BpmPositions.Clear();
							songInfo.Bpms.Clear();
						}

						for (int i = 0; i < bpmData.Length; i++)
						{
							if (i % 2 == 0)
							{
								if (float.TryParse(bpmData[i], out var bpmPosition))
								{
									songInfo.BpmPositions.Add(bpmPosition);
								}
							}
							else
							{
								if (float.TryParse(bpmData[i], out var bpm))
								{
									songInfo.Bpms.Add(bpm);
								}
							}
						}
					}

					// SplitSPEEDS
					lineText = rs.ReadLine().Trim();

					if (lineText.CustomStartsWith("SplitSPEEDS"))
					{
						songInfo.SpeedPositions.Clear();
						songInfo.SpeedStretchRatios.Clear();
						songInfo.SpeedDelayBeats.Clear();

						var speedData = lineText.GetBetweenChars('[', ']').Split(new char[] { '=', ',' }, StringSplitOptions.RemoveEmptyEntries);

						for (int i = 0; i < speedData.Length; i++)
						{
							if (i % 3 == 0)
							{
								if (float.TryParse(speedData[i], out var speedPosition))
								{
									songInfo.SpeedPositions.Add(speedPosition);
								}
							}
							else if (i % 3 == 1)
							{
								if (float.TryParse(speedData[i], out var speedStretchRatio))
								{
									songInfo.SpeedStretchRatios.Add(speedStretchRatio);
								}
							}
							else if (i % 3 == 2)
							{
								if (float.TryParse(speedData[i], out var speedDelayBeat))
								{
									songInfo.SpeedDelayBeats.Add(speedDelayBeat);
								}
							}
						}
					}
					continue;
				}

				if (noteReadState == 3)
				{
					if (lineText.CustomStartsWith(",") || lineText.CustomStartsWith(";"))
					{
						var count = barText.Count;
						for (var i = 0; i < count; i++)
						{
							for (int j = 0; j < barText[i].Length; j++)
							{
								if (j >= MAX_LANE)
								{
									break;
								}

								char c = barText[i][j];

								// タップノート
								if (c == '1')
								{
									sequence.BeatPositions.Add((bar * timeSignature) + (i * timeSignature / count));
									sequence.Lanes.Add(j);
									sequence.NoteTypes.Add(NoteType.Normal);
									sequence.LeftRight.Add(false);
									continue;
								}

								// 単ファジーノート
								if (c == 'F' || c == 'M')
								{
									sequence.BeatPositions.Add((bar * timeSignature) + (i * timeSignature / count));
									sequence.Lanes.Add(j);
									sequence.NoteTypes.Add(NoteType.Fuzzy);
									sequence.LeftRight.Add(false);
									continue;
								}

								//--------------------------------------------
								// ロング[青]
								//
								// 大文字と小文字で別々の繋ぎとして認識する
								// 
								// (A|a) → 開始 
								// (B|b) → 中継
								// (C|c) → 終端
								//--------------------------------------------
								if (c == 'A' || c == 'a')
								{
									sequence.BeatPositions.Add((bar * timeSignature) + (i * timeSignature / count));
									sequence.Lanes.Add(j);
									sequence.NoteTypes.Add(NoteType.LongStart);
									sequence.LeftRight.Add(c == 'a');
									continue;
								}

								if (c == 'B' || c == 'b')
								{
									sequence.BeatPositions.Add((bar * timeSignature) + (i * timeSignature / count));
									sequence.Lanes.Add(j);
									sequence.NoteTypes.Add(NoteType.LongRelay);
									sequence.LeftRight.Add(c == 'b');
									continue;
								}

								if (c == 'C' || c == 'c')
								{
									sequence.BeatPositions.Add((bar * timeSignature) + (i * timeSignature / count));
									sequence.Lanes.Add(j);
									sequence.NoteTypes.Add(NoteType.LongEnd);
									sequence.LeftRight.Add(c == 'c');
									continue;
								}

								//--------------------------------------------
								// ファジーロング[緑]
								//
								// 大文字と小文字で別々の繋ぎとして認識する
								// 
								// (X|x) → 開始 
								// (Y|y) → 中継
								// (Z|z) → 終端
								//--------------------------------------------
								if (c == 'X' || c == 'x')
								{
									sequence.BeatPositions.Add((bar * timeSignature) + (i * timeSignature / count));
									sequence.Lanes.Add(j);
									sequence.NoteTypes.Add(NoteType.FuzzyLongStart);
									sequence.LeftRight.Add(c == 'x');
									continue;
								}

								if (c == 'Y' || c == 'y')
								{
									sequence.BeatPositions.Add((bar * timeSignature) + (i * timeSignature / count));
									sequence.Lanes.Add(j);
									sequence.NoteTypes.Add(NoteType.FuzzyLongRelay);
									sequence.LeftRight.Add(c == 'y');
									continue;
								}

								if (c == 'Z' || c == 'z')
								{
									sequence.BeatPositions.Add((bar * timeSignature) + (i * timeSignature / count));
									sequence.Lanes.Add(j);
									sequence.NoteTypes.Add(NoteType.FuzzyLongEnd);
									sequence.LeftRight.Add(c == 'z');
									continue;
								}
							}
						}

						barText.Clear();
						bar++;
					}
					else
					{
						barText.Add(lineText);
					}

					if (lineText.CustomStartsWith(";"))
					{
						break;
					}
				}
			}
		}

		rs.Close();

		sequence.LongInfo = CreateLongInfos(sequence, songInfo);

		return sequence;
	}

	/// <summary>
	/// LongInfoのリストを生成
	/// </summary>
	/// <param name="sequence"></param>
	/// <param name="songInfo"></param>
	/// <returns></returns>
	List<LongInfo> CreateLongInfos(Sequence sequence, SongInfo songInfo)
	{
		var bpmHelper = new BpmHelper();
		bpmHelper.Init(songInfo);

		var longInfos = new List<LongInfo>();

		for (int index = 0; index < sequence.BeatPositions.Count; index++)
		{
			if ((sequence.NoteTypes[index] != NoteType.LongStart &&
				sequence.NoteTypes[index] != NoteType.FuzzyLongStart) ||
				index >= sequence.BeatPositions.Count)
			{
				continue;
			}

			LongInfo longInfo = new LongInfo()
			{
				NoteIndex = new List<int>(),
				BeatPositions = new List<double>(),
				Lanes = new List<int>(),
				LongType = sequence.NoteTypes[index] == NoteType.LongStart ? LongType.Long : LongType.FuzzyLong
			};

			// ロングの開始ノートを取得
			longInfo.BeatPositions.Add(sequence.BeatPositions[index]);
			longInfo.Lanes.Add(sequence.Lanes[index]);
			longInfo.StartNoteIndex = index;

			// ロングの中継と終了のノートを取得
			for (int i = index + 1; i < sequence.BeatPositions.Count; i++)
			{
				LongType longType = LongType.Long;

				if (sequence.NoteTypes[i] == NoteType.LongEnd ||
					sequence.NoteTypes[i] == NoteType.LongRelay)
				{
					longType = LongType.Long;
				}

				if (sequence.NoteTypes[i] == NoteType.FuzzyLongEnd ||
					sequence.NoteTypes[i] == NoteType.FuzzyLongRelay)
				{
					longType = LongType.FuzzyLong;
				}

				if (
					// ABCとXYZ、ABCとabc２つの場合で別々の接続として認識する
					(longType != longInfo.LongType || sequence.LeftRight[i] != sequence.LeftRight[index]) ||
					sequence.BeatPositions[i] == sequence.BeatPositions[index])
				{
					continue;
				}

				if (sequence.NoteTypes[i] == NoteType.LongEnd || sequence.NoteTypes[i] == NoteType.LongRelay ||
					sequence.NoteTypes[i] == NoteType.FuzzyLongEnd || sequence.NoteTypes[i] == NoteType.FuzzyLongRelay)
				{
					longInfo.NoteIndex.Add(i);
					longInfo.BeatPositions.Add(sequence.BeatPositions[i]);
					longInfo.Lanes.Add(sequence.Lanes[i]);
				}

				if (sequence.NoteTypes[i] == NoteType.LongEnd || sequence.NoteTypes[i] == NoteType.FuzzyLongEnd)
				{
					longInfo.EndTime = bpmHelper.BeatToTime(sequence.BeatPositions[i]);
					break;
				}
			}

			longInfos.Add(longInfo);
		}

		return longInfos;
	}
}
