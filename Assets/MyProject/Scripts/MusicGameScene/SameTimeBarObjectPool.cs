﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public sealed class SameTimeBarObjectPool : MonoBehaviour
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[SerializeField] SameTimeBarController _beatBarControllerPrefab;

	//------------------
	// キャッシュ.
	//------------------
	ObjectPool<SameTimeBarController> _pool = null;
	List<SameTimeBarController> _currentActiveSameTimeBar = new List<SameTimeBarController>();
	Transform _transform;

	//------------------
	// 定数.
	//------------------
	// 初期のプールサイズ
	const int DEFAULT_CAPACITY = 16;
	// プールサイズを最大どれだけ大きくするか
	const int MAX_SIZE = 32;

	//------------------
	// プロパティ.
	//------------------
	public List<SameTimeBarController> CurrentActiveSameTimeBar => _currentActiveSameTimeBar;

	public void Init()
	{
		_transform = transform;
	}

	public ObjectPool<SameTimeBarController> Pool
	{
		get
		{
			if (_pool == null)
			{
				_pool = new ObjectPool<SameTimeBarController>(OnCreatePoolObject, OnTakeFromPool, OnReturnedToPool, OnDestroyPoolObject, false, DEFAULT_CAPACITY, MAX_SIZE);
			}

			return _pool;
		}
	}

	SameTimeBarController OnCreatePoolObject()
	{
		var controller = Instantiate(_beatBarControllerPrefab, _transform);
		controller.Init(Pool);
		_currentActiveSameTimeBar.Add(controller);
		return controller;
	}

	void OnTakeFromPool(SameTimeBarController controller)
	{
		controller.gameObject.SetActive(true);
		_currentActiveSameTimeBar.Add(controller);
	}

	void OnReturnedToPool(SameTimeBarController controller)
	{
		_currentActiveSameTimeBar.Remove(controller);
		controller.gameObject.SetActive(false);
	}

	void OnDestroyPoolObject(SameTimeBarController controller)
	{
		controller.gameObject.SetActive(false);
		Destroy(controller.gameObject);
	}
}
