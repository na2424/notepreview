﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public sealed class JudgeEffectController : MonoBehaviour
{
	[SerializeField] ParticleSystem[] _ps;

	public ObjectPool<JudgeEffectController> Pool;

	bool _isPlay = false;

	public void Play()
	{
		_isPlay = true;

		foreach(var p in _ps)
		{
			p.Play();
		}
	}

	private void Update()
	{
		if(_isPlay && !_ps[0].isPlaying)
		{
			OnFinish();
		}
	}

	void OnFinish()
	{
		_isPlay = false;
		Pool.Release(this);
	}
}
