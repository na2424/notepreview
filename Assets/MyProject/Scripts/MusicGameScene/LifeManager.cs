﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

/// <summary>
/// ライフゲージの値と描画を管理するクラス
/// </summary>
public sealed class LifeManager : MonoBehaviour
{
	[SerializeField] Slider _lifeSlider = default;
	[SerializeField] Image _sliderImage = default;
	[SerializeField] Image _sliderBgImage = default;
	[SerializeField] Image _lifeBgImage = default;

	public Action OnDead = null;

	float _life = 250f;
	float _maxLifeValue = 500f;
	bool _isDead = false;
	bool _isDanger = false;
	int _second = 0;
	List<float> _secondLifeCache = new List<float>();
	LifeHistory _lifeHistory = new LifeHistory();

	//TODO:外部ファイルにする
	readonly float PerfectLife = 3f;
	readonly float GrateLife = 2f;
	readonly float GoodLife = 1f;
	readonly float BadLife = -10f;
	readonly float MissLife = -30f;

	const float DANGER_LIFE = 90f;

	public bool IsDead => _isDead;
	public LifeHistory LifeHistory => _lifeHistory;

	float JudgeTypeToLifeValue(JudgeType type) => type switch
	{
		JudgeType.Brilliant => PerfectLife,
		JudgeType.Great => GrateLife,
		JudgeType.Fast => GoodLife,
		JudgeType.Bad => BadLife,
		JudgeType.Missed => MissLife,
		_ => 0f
	};

	public void Init()
	{
		_lifeSlider.maxValue = _maxLifeValue;
		_life = _maxLifeValue / 2f;
		_lifeSlider.value = _life;
	}

	public void UpdateLife(JudgeType type)
	{
		if (_isDead)
		{
			return;
		}

		_life += JudgeTypeToLifeValue(type);

		if (_life > _maxLifeValue)
		{
			_life = _maxLifeValue;
		}
		if (_life < 0f)
		{
			_isDead = true;
			_life = 0f;
			OnDead?.Invoke();
		}

		if (!_isDanger && _life <= DANGER_LIFE)
		{
			_isDanger = true;
			_lifeBgImage.DOColor(Color.red, 0.78f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo);
			_sliderImage.DOColor(Color.red, 0.78f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo);
			_sliderBgImage.DOColor(Color.red, 0.78f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo);
		}
		if (_isDanger && _life > DANGER_LIFE)
		{
			_isDanger = false;
			_lifeBgImage.DOKill();
			_sliderImage.DOKill();
			_sliderBgImage.DOKill();
			_lifeBgImage.color = Color.white;
			_sliderImage.color = Color.white;
			_sliderBgImage.color = Color.white;
		}

		_lifeSlider.value = _life;
	}

	public void UpdateRecord(double musicTime)
	{
		if (musicTime < 0f)
		{
			return;
		}

		if (_second < musicTime)
		{
			if (_secondLifeCache.Count > 0)
			{
				float secondTotal = 0f;
				foreach (var life in _secondLifeCache)
				{
					secondTotal += life;
				}

				_lifeHistory.Add(_second, secondTotal / _secondLifeCache.Count);
				_secondLifeCache.Clear();
			}

			_second++;
		}
		else
		{
			_secondLifeCache.Add(_life);
		}

	}

	void OnDestroy()
	{
		_lifeBgImage.DOKill();
		_sliderImage.DOKill();
		_sliderBgImage.DOKill();
		_secondLifeCache.Clear();
		_secondLifeCache = null;
		OnDead = null;
	}
}
