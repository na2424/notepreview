﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

/// <summary>
/// 同時押しラインの情報を管理するクラス
/// </summary>
public sealed class SameTimeBarController : MonoBehaviour
{
	[SerializeField] MeshRenderer _meshRenderer;
	[SerializeField] MeshFilter _meshFilter;

	//------------------
	// キャッシュ.
	//------------------
	ObjectPool<SameTimeBarController> _pool;
	Transform _transform;
	Mesh _mesh;
	Vector3[] _vertices = new Vector3[4];
	double _beatPosition = -1f;
	bool _isReleased = false;
	double _justTime = -1d;
	int _leftNoteIndex = -1;
	int _rightNoteIndex = -1;
	float _normalizedSpeed = 0f;
	bool _isCmod;
	AnimationCurve _animationCurve;
	NotesScrollType _notesScrollType;

	//------------------
	// 定数.
	//------------------
	static readonly float BEAT_DISTANCE = Constant.Note.BEAT_DISTANCE;
	static readonly float TIME_DISTANCE = Constant.Note.TIME_DISTANCE;
	static readonly float LANE_DISTANCE = Constant.Note.LANE_DISTANCE;
	static readonly float OFFSET_Z = Constant.Note.OFFSET_Z;
	static readonly float WIDTH = 0.041f;

	//------------------
	// プロパティ.
	//------------------
	float _hiSpeed => GameManager.Instance.NotesOption.HiSpeed;
	public int LeftNoteIndex => _leftNoteIndex;
	public int RightNoteIndex => _rightNoteIndex;

	public void Init(ObjectPool<SameTimeBarController> pool)
	{
		_isCmod = GameManager.Instance.IsCmod;
		_notesScrollType = GameManager.Instance.NotesOption.NotesScrollType;
		_animationCurve = GameManager.Instance.NoteScrollAnimationCurve;

		_transform = transform;
		_pool = pool;
		_normalizedSpeed = Constant.Note.FIXED_BPM / GameManager.Instance.SelectSongInfo.BaseBpm;

		_vertices = new Vector3[]
		{
			new Vector3(-LANE_DISTANCE * 3f, 0f, -WIDTH),
			new Vector3(-LANE_DISTANCE * 3f, 0, WIDTH),
			new Vector3( LANE_DISTANCE * 3f, 0, WIDTH),
			new Vector3( LANE_DISTANCE * 3f, 0, -WIDTH)
		};
		int[] triangles = { 0, 1, 2, 0, 2, 3 };

		_mesh = new Mesh();
		_mesh.vertices = _vertices;
		_mesh.triangles = triangles;

		_meshFilter.mesh = _mesh;
	}

	public void SetParam(double beatPosition, double justTime, int laneLeft, int laneRight, int leftNoteIndex, int rightNoteIndex)
	{
		_isReleased = false;
		_beatPosition = beatPosition;
		_justTime = justTime;

		_vertices[0] = new Vector3(LANE_DISTANCE * (laneLeft - 3f), 0f, -WIDTH);
		_vertices[1] = new Vector3(LANE_DISTANCE * (laneLeft - 3f), 0f, WIDTH);
		_vertices[2] = new Vector3(LANE_DISTANCE * (laneRight - 3f), 0f, WIDTH);
		_vertices[3] = new Vector3(LANE_DISTANCE * (laneRight - 3f), 0f, -WIDTH);
		
		_mesh.SetVertices(_vertices);
		_meshFilter.mesh = _mesh;

		_leftNoteIndex = leftNoteIndex;
		_rightNoteIndex = rightNoteIndex;
	}

	public void OnUpdate(double beat, double musicTime, float speedStretchRatio)
	{
		if (_isReleased || !gameObject.activeSelf)
		{
			return;
		}

		if (_isCmod)
		{
			UpdateCmodPosition(musicTime, speedStretchRatio);
		}
		else
		{
			UpdatePosition(beat, speedStretchRatio);
		}

		CheckRelease(musicTime);
	}

	void UpdateCmodPosition(double musicTime, float speedStretchRatio)
	{
		double diffTime = _justTime - musicTime;
		float posZ = (float)(diffTime * TIME_DISTANCE * _hiSpeed * speedStretchRatio - OFFSET_Z);

		if (_notesScrollType == NotesScrollType.Decelerate)
		{
			posZ = _animationCurve.Evaluate(posZ);
		}

		if (float.IsNaN(posZ))
		{
			posZ = 100f;
		}

		_transform.localPosition = new Vector3(
			0f,
			0f,
			posZ
		);
	}

	void UpdatePosition(double beat, float speedStretchRatio)
	{
		float posZ = (float)((_beatPosition - beat) * BEAT_DISTANCE * _normalizedSpeed * _hiSpeed * speedStretchRatio - OFFSET_Z);

		if (_notesScrollType == NotesScrollType.Decelerate)
		{
			posZ = _animationCurve.Evaluate(posZ);
		}

		if (float.IsNaN(posZ))
		{
			posZ = 100f;
		}

		_transform.localPosition = new Vector3(
			0f,
			0f,
			posZ
		);
	}

	void CheckRelease(double musicTime)
	{
		if (_justTime < musicTime)
		{
			Release();
		}
	}

	public void Release()
	{
		if (_isReleased)
		{
			return;
		}

		_isReleased = true;
		_pool.Release(this);
	}
}
