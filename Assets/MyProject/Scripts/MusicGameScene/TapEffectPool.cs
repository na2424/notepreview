﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public sealed class TapEffectPool : MonoBehaviour
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[SerializeField] JudgeEffectController _tapEffectControllerPrefab;

	//------------------
	// キャッシュ.
	//------------------
	ObjectPool<JudgeEffectController> _pool = null;
	Transform _transform;

	//------------------
	// 定数.
	//------------------
	// 初期のプールサイズ
	const int DEFAULT_CAPACITY = 8;
	// プールサイズを最大どれだけ大きくするか
	const int MAX_SIZE = 16;

	public void Init()
	{
		_transform = transform;
	}

	public ObjectPool<JudgeEffectController> Pool
	{
		get
		{
			if (_pool == null)
			{
				_pool = new ObjectPool<JudgeEffectController>(OnCreatePoolObject, OnTakeFromPool, OnReturnedToPool, OnDestroyPoolObject, false, DEFAULT_CAPACITY, MAX_SIZE);
			}

			return _pool;
		}
	}

	public void Play(Vector3 pos)
	{
		var controller = Pool.Get();
		controller.transform.localPosition = pos;
		controller.Play();
	}

	JudgeEffectController OnCreatePoolObject()
	{
		var controller = Instantiate(_tapEffectControllerPrefab, _transform);
		controller.Pool = Pool;
		return controller;
	}

	void OnTakeFromPool(JudgeEffectController controller)
	{
		controller.gameObject.SetActive(true);
		controller.Play();
	}

	void OnReturnedToPool(JudgeEffectController controller)
	{
		controller.gameObject.SetActive(false);
	}

	void OnDestroyPoolObject(JudgeEffectController controller)
	{
		Destroy(controller.gameObject);
	}
}
