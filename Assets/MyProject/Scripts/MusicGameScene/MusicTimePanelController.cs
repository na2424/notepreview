using UnityEngine;
using UnityEngine.UI;

public sealed class MusicTimePanelController : MonoBehaviour
{
	[SerializeField] Slider _musicTimeSlider;

	double _musicEndTime = double.MaxValue;

	public void Init(double musicEndTime)
	{
		_musicEndTime = musicEndTime;
	}

	public void CallUpdate(double musicTime)
	{
		_musicTimeSlider.value = Mathf.Clamp01(1f - (float)(musicTime / _musicEndTime));
	}
}
