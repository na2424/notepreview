﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public sealed class LongObjectPool : MonoBehaviour
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[SerializeField] LongController _longControllerPrefab;
	
	//------------------
	// キャッシュ.
	//------------------
	ObjectPool<LongController> _pool = null;
	List<LongController> _currentActiveLong = new List<LongController>();
	Transform _transform;
	BpmHelper _bpmHelper;

	//------------------
	// 定数.
	//------------------
	// 初期のプールサイズ
	const int DEFAULT_CAPACITY = 16;
	// プールサイズを最大どれだけ大きくするか
	const int MAX_SIZE = 24;

	//------------------
	// プロパティ.
	//------------------
	public List<LongController> CurrentActiveLong => _currentActiveLong;

	public void Init(BpmHelper bpmHelper)
	{
		_transform = transform;
		_bpmHelper = bpmHelper;
	}

	public ObjectPool<LongController> Pool
	{
		get
		{
			if (_pool == null)
			{
				_pool = new ObjectPool<LongController>(OnCreatePoolObject, OnTakeFromPool, OnReturnedToPool, OnDestroyPoolObject, false, DEFAULT_CAPACITY, MAX_SIZE);
			}

			return _pool;
		}
	}

	LongController OnCreatePoolObject()
	{
		var controller = Instantiate(_longControllerPrefab, _transform);
		controller.Init(Pool, _bpmHelper);

		float size = GameManager.Instance.NotesOption.Size;

		if(size < 1f)
		{
			controller.SetWidthScale(GameManager.Instance.NotesOption.Size);
		}
		
		_currentActiveLong.Add(controller);
		
		return controller;
	}

	void OnTakeFromPool(LongController controller)
	{
		_currentActiveLong.Add(controller);
		controller.gameObject.SetActive(true);
	}

	void OnReturnedToPool(LongController controller)
	{
		_currentActiveLong.Remove(controller);
		controller.gameObject.SetActive(false);
	}

	void OnDestroyPoolObject(LongController controller)
	{
		//Debug.Log("Long Destroy");
		_currentActiveLong.Remove(controller);
		Destroy(controller.gameObject);
	}

}
