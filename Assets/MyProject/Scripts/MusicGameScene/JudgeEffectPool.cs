﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public sealed class JudgeEffectPool : MonoBehaviour
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[SerializeField] JudgeEffectController _judgeEffectControllerPrefab;

	//------------------
	// キャッシュ.
	//------------------
	ObjectPool<JudgeEffectController> _pool = null;
	Transform _transform;

	//------------------
	// 定数.
	//------------------
	// 初期のプールサイズ
	const int DEFAULT_CAPACITY = 32;
	// プールサイズを最大どれだけ大きくするか
	const int MAX_SIZE = 64;

	public void Init()
	{
		_transform = transform;
		// 初回のカクツキを軽減する為
		Play(100f);
	}

	public ObjectPool<JudgeEffectController> Pool
	{
		get
		{
			if (_pool == null)
			{
				_pool = new ObjectPool<JudgeEffectController>(OnCreatePoolObject, OnTakeFromPool, OnReturnedToPool, OnDestroyPoolObject, false, DEFAULT_CAPACITY, MAX_SIZE);
			}

			return _pool;
		}
	}

	public void Play(float posX)
	{
		var controller = Pool.Get();
		controller.transform.localPosition = new Vector3(posX, 0f, 0f);
		controller.Play();
	}

	JudgeEffectController OnCreatePoolObject()
	{
		var controller = Instantiate(_judgeEffectControllerPrefab, _transform);
		controller.Pool = Pool;
		return controller;
	}

	void OnTakeFromPool(JudgeEffectController controller)
	{
		controller.gameObject.SetActive(true);
		controller.Play();
	}

	void OnReturnedToPool(JudgeEffectController controller)
	{
		controller.gameObject.SetActive(false);
	}

	void OnDestroyPoolObject(JudgeEffectController controller)
	{
		Destroy(controller.gameObject);
	}
}
