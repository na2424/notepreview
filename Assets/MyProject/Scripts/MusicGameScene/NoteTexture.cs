﻿using UnityEngine;

[CreateAssetMenu(menuName = "MyScriptable/NoteTexture")]
public sealed class NoteTexture : ScriptableObject
{
	[SerializeField] Texture _normalNoteTex;
	[SerializeField] Texture _longNoteTex;
	[SerializeField] Texture _longRelayTex;
	[SerializeField] Texture _fuzzyNoteTex;
	[SerializeField] Texture _fuzzyRelayTex;

	/// <summary>
	/// NoteTypeから指定のTextureを返す.
	/// </summary>
	/// <param name="type">NoteType</param>
	/// <returns>Texture</returns>
	public Texture NoteTypeToTexture(NoteType type) => type switch
	{
		NoteType.Normal => _normalNoteTex,
		NoteType.LongStart => _longNoteTex,
		NoteType.LongRelay => _longRelayTex,
		NoteType.LongEnd => _longNoteTex,
		NoteType.Fuzzy => _fuzzyNoteTex,
		NoteType.FuzzyLongStart => _fuzzyNoteTex,
		NoteType.FuzzyLongRelay => _fuzzyRelayTex,
		NoteType.FuzzyLongEnd => _fuzzyNoteTex,
		_ => _normalNoteTex
	};

	public void SetTextureFromNoteSkinInfo(NoteSkinInfo info)
	{
		_normalNoteTex = info.NotesTapTexture;
		_longNoteTex = info.NotesSlideTexture;
		_longRelayTex = info.RelaySlideTexture;
		_fuzzyNoteTex = info.NotesFuzzyTexture;
		_fuzzyRelayTex = info.RelayFuzzyTexture;
	}

}