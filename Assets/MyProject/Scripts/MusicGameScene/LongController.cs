﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Pool;

/// <summary>
/// ロングのライン情報管理するクラス
/// </summary>
public sealed class LongController : MonoBehaviour
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[SerializeField] MeshRenderer _meshRenderer;
	[SerializeField] MeshFilter _meshFilter;
	[SerializeField, Range(0, 1f)] float _sizeX;

	//------------------
	// キャッシュ.
	//------------------
	ObjectPool<LongController> _pool;
	Transform _transform;
	LongInfo _longInfo;
	Action<bool, int, int, NoteType> _onRelease = null;
	MaterialPropertyBlock _mpb;
	BpmHelper _bpmHelper;
	AnimationCurve _animationCurve;
	NotesScrollType _notesScrollType;

	bool _isReleased = false;
	int _touchId = -1;
	bool _currentUseStencil = false;
	float _hiSpeed = 1.0f;
	float _normalizedSpeed = 0f;
	double _longEndHitTime = 1d;
	bool _isCmod;

	// メッシュに関わる変数
	Mesh _mesh;
	List<Vector2> _normalizePositions;
	Vector2[] _cachePositions;

	Vector3[] _vertices;
	int[] _triangles;
	Vector2[] _uvs;
	float _beforeSpeedStretchRatio = 1.0f;
	float _cacheSizeX;
	float _widthScale = 1f;
	int _currentIndex = 0;

	//------------------
	// 定数.
	//------------------
	static readonly int STENCIL_ID = Constant.ShaderProperty.StencilId;
	static readonly int MAIN_TEX_ID = Constant.ShaderProperty.MainTexId;

	static readonly float BEAT_DISTANCE = Constant.Note.BEAT_DISTANCE;
	static readonly float TIME_DISTANCE = Constant.Note.TIME_DISTANCE;
	static readonly float LANE_DISTANCE = Constant.Note.LANE_DISTANCE;
	static readonly float OFFSET_X = Constant.Note.OFFSET_X;
	static readonly float OFFSET_Z = Constant.Note.OFFSET_Z;

	static readonly float STENCIL_ON = 2f;
	static readonly float STENCIL_OFF = 1f;

	//------------------
	// プロパティ.
	//------------------

	public bool IsReleased => _isReleased;
	public float CurrentPosX { get; private set; }
	public double StartBeatPosition { get; private set; }
	public double EndBeatPosition { get; private set; }

	//途中で離した時の入力の開始拍変更に対応
	public double InputStartBeatPosition { get; private set; }
	public double StartTime { get; private set; }
	public double EndTime => _longInfo.EndTime;
	public int TouchId => _touchId;
	public LongInfo LongInfo => _longInfo;

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name="pool"></param>
	/// <param name="bpmHelper"></param>
	public void Init(ObjectPool<LongController> pool, BpmHelper bpmHelper)
	{
		_transform = transform;
		_pool = pool;
		_hiSpeed = GameManager.Instance.NotesOption.HiSpeed;
		_mpb = new MaterialPropertyBlock();
		_normalizedSpeed = Constant.Note.FIXED_BPM / GameManager.Instance.SelectSongInfo.BaseBpm;
		_longEndHitTime = GameManager.Instance.JudgeTimeOption.GetLongEndHitTime();
		_mesh = new Mesh();
		_isCmod = GameManager.Instance.IsCmod;
		_notesScrollType = GameManager.Instance.NotesOption.NotesScrollType;
		_animationCurve = GameManager.Instance.NoteScrollAnimationCurve;
		_bpmHelper = bpmHelper;
	}

	/// <summary>
	/// ラインの生成(Poolの使いまわし)時に呼ばれる.自身の設定を行なう
	/// </summary>
	/// <param name="longInfo"></param>
	/// <param name="onRelease"></param>
	/// <param name="longTex"></param>
	public void SetParam(LongInfo longInfo, Action<bool, int, int, NoteType> onRelease, Texture longTex)
	{
		UseStencil(false);
		_isReleased = false;
		_touchId = -1;
		_longInfo = longInfo;
		_onRelease = onRelease;

		_currentIndex = 0;

		// マテリアルのInstanceを避ける.
		_mpb.SetTexture(MAIN_TEX_ID, longTex);
		_meshRenderer.SetPropertyBlock(_mpb);

		StartBeatPosition = longInfo.BeatPositions.First();
		EndBeatPosition = longInfo.BeatPositions.Last();
		InputStartBeatPosition = StartBeatPosition;

		StartTime = _bpmHelper.BeatToTime(StartBeatPosition);

		_cacheSizeX = _sizeX * _widthScale;

		SetMesh();
	}

	/// <summary>
	/// メッシュを生成してMeshFilterに設定する
	/// </summary>
	void SetMesh()
	{
		_normalizePositions = null;
		_normalizePositions = new List<Vector2>();

		var beatPositions = _longInfo.BeatPositions;
		var lanes = _longInfo.Lanes;
		int count = beatPositions.Count;

		_cachePositions = new Vector2[count];

		for (int i = 0; i < count; i++)
		{
			if (_isCmod)
			{
				double diffTime = _bpmHelper.BeatToTime(beatPositions[i]) - StartTime;

				_normalizePositions.Add(new Vector2(
					lanes[i] * LANE_DISTANCE - OFFSET_X,
					(float)(diffTime * TIME_DISTANCE * _hiSpeed /*- OFFSET_Z*/)
				));
			}
			else
			{
				_normalizePositions.Add(new Vector2(
					lanes[i] * LANE_DISTANCE - OFFSET_X,
					(float)((beatPositions[i] - StartBeatPosition) * BEAT_DISTANCE * _normalizedSpeed * _hiSpeed /*- OFFSET_Z*/)
				));
			}
		}

		CreateMesh(_normalizePositions);

		_meshFilter.mesh = _mesh;
	}

	/// <summary>
	/// メッシュ生成
	/// (MEMO: メッシュを生成した後に同一フレームでApplySpeedStretchRatioを呼ばないとメッシュが正しい位置にならないので一瞬ちらついたように見える。
	/// その為、ApplySpeedStretchRatioを呼ぶのを忘れないようにすること。)
	/// </summary>
	/// <param name="positions"></param>
	void CreateMesh(List<Vector2> positions)
	{
		_mesh.Clear();

		_vertices = new Vector3[2 * positions.Count];
		_triangles = new int[(positions.Count - 1) * 2 * 3];
		_uvs = new Vector2[_vertices.Length];

		float halfSizeX = _cacheSizeX * 0.5f;

		// vertices
		int vi = 0;
		for (int i = 0; i < _normalizePositions.Count; i++)
		{
			for (int x = 0; x < 2; x++)
			{
				_vertices[vi++] = new Vector3(positions[i].x + (_cacheSizeX * x - halfSizeX), positions[i].y, 0);
			}
		}

		// triangles
		int ti = 0;
		for (int y = 0; y < _normalizePositions.Count - 1; y++)
		{
			_triangles[ti] = y * 2;
			_triangles[ti + 1] = _triangles[ti + 5] = (y + 1) * 2;
			_triangles[ti + 2] = _triangles[ti + 4] = 1 + y * 2;
			_triangles[ti + 3] = 1 + (y + 1) * 2;
			ti += 6;
		}

		// uvs
		int ui = 0;
		for (int i = 0; i < _normalizePositions.Count; i++)
		{
			for (int x = 0; x < 2; x++)
			{
				_uvs[ui++] = new Vector2((float)i / _normalizePositions.Count(), x);
			}
		}

		_mesh.SetVertices(_vertices);
		_mesh.SetTriangles(_triangles, 0);
		_mesh.SetUVs(0, _uvs);
	}

	/// <summary>
	/// ロングを掴んでいる指(タッチID)を保持する
	/// </summary>
	/// <param name="touchId"></param>
	public void SetTouchId(int touchId)
	{
		_touchId = touchId;
	}

	/// <summary>
	/// ステンシルマスクを使うか設定
	/// </summary>
	/// <param name="use"></param>
	public void UseStencil(bool use)
	{
		if (use != _currentUseStencil)
		{
			_currentUseStencil = use;
			_meshRenderer.material.SetFloat(STENCIL_ID, use ? STENCIL_ON : STENCIL_OFF);
		}
	}

	/// <summary>
	/// ラインの太さを設定する
	/// </summary>
	/// <param name="scale">倍率</param>
	public void SetWidthScale(float scale)
	{
		_widthScale = scale;
	}

	public void OnUpdate(double beat, double musicTime, float speedStretchRatio)
	{
		if (_isReleased || !gameObject.activeSelf || _longInfo == null)
		{
			return;
		}

		//CheckMiss(musicTime);

		UpdatePosition(beat, musicTime, speedStretchRatio);
		UpdateMash(beat);
		ApplySpeedStretchRatio(speedStretchRatio);

		if (_currentIndex < _longInfo.BeatPositions.Count - 1 &&
			(_longInfo.BeatPositions[_currentIndex] < beat && beat < _longInfo.BeatPositions[_currentIndex + 1]))
		{
			float halfSizeX = _cacheSizeX * 0.5f;

			// 手前のメッシュの頂点は判定ラインに沿わせる
			for (int vi = 0; vi < 2; vi++)
			{
				_vertices[vi] = new Vector3(
					// X
					(_currentUseStencil ? CurrentPosX : _cachePositions[0].x) + (_cacheSizeX * vi - halfSizeX),
					// Y
					_currentUseStencil ?
						-_transform.position.z :
						_notesScrollType == NotesScrollType.Constant ?
							_cachePositions[0].y :
							_animationCurve.Evaluate(_cachePositions[0].y + _transform.localPosition.z) - _transform.localPosition.z,
					// Z
					0);
			}

			if (_currentUseStencil)
			{
				_mesh.SetVertices(_vertices);
			}
		}

		CheckReleaseNote(musicTime);

	}

	void UpdatePosition(double beat, double musicTime, float speedStretchRatio)
	{
		CurrentPosX = -100f;

		// レーンを跨ぐロングノートで現在のX座標を求める
		for (int i = 0; i < _longInfo.BeatPositions.Count - 1; i++)
		{
			if (_longInfo.BeatPositions[i] < beat && beat < _longInfo.BeatPositions[i + 1])
			{
				CurrentPosX = Mathf.Lerp(
					_longInfo.Lanes[i] * LANE_DISTANCE - OFFSET_X,
					_longInfo.Lanes[i + 1] * LANE_DISTANCE - OFFSET_X,
					(float)((beat - _longInfo.BeatPositions[i]) / (_longInfo.BeatPositions[i + 1] - _longInfo.BeatPositions[i]))
				);
				break;
			}
		}

		if (_isCmod)
		{
			_transform.localPosition = new Vector3(
				0f,
				0f,
				(float)((StartTime - musicTime) * TIME_DISTANCE * _hiSpeed * speedStretchRatio - (OFFSET_Z))
			);
		}
		else
		{
			_transform.localPosition = new Vector3(
				0f,
				0f,
				(float)((StartBeatPosition - beat) * BEAT_DISTANCE * _normalizedSpeed * _hiSpeed * speedStretchRatio - (OFFSET_Z))
			);
		}

	}

	void UpdateMash(double beat)
	{
		bool hasUpdateMesh = false;

		// 中継を超えた時に中継前のメッシュの頂点を削除
		if (_currentIndex < _longInfo.BeatPositions.Count - 1 &&
			_longInfo.BeatPositions[_currentIndex + 1] < beat &&
			InputStartBeatPosition <= _longInfo.BeatPositions[_currentIndex + 1])
		{
			//Debug.Log("_currentIndex:" + _currentIndex);
			_normalizePositions.RemoveAt(0);
			_cachePositions = new Vector2[_normalizePositions.Count];

			hasUpdateMesh = true;
			_currentIndex++;
		}

		if (hasUpdateMesh)
		{
			if (_normalizePositions.Count > 0)
			{
				CreateMesh(_normalizePositions);
			}
		}
	}

	/// <summary>
	/// スピードの加速減速をラインのメッシュに対応させる
	/// (ノーツスクロールが減速モードの時は常にメッシュを動かす)
	/// </summary>
	/// <param name="speedStretchRatio"></param>
	void ApplySpeedStretchRatio(float speedStretchRatio)
	{
		if (_notesScrollType == NotesScrollType.Constant && Calculate.FastApproximately(speedStretchRatio, _beforeSpeedStretchRatio, 0.00001f))
		{
			return;
		}

		for (int i = 0; i < _cachePositions.Length; i++)
		{
			_cachePositions[i] = new Vector2(
				_normalizePositions[i].x,
				(float)(_normalizePositions[i].y * speedStretchRatio/* - OFFSET_Z*/));
		}

		float halfSizeX = _cacheSizeX * 0.5f;

		int vi = 0;
		for (int i = 0; i < _cachePositions.Length; i++)
		{
			float posY = _notesScrollType == NotesScrollType.Constant ?
					_cachePositions[i].y :
					_animationCurve.Evaluate(_cachePositions[i].y + _transform.localPosition.z) - _transform.localPosition.z;

			for (int x = 0; x < 2; x++)
			{
				_vertices[vi++] = new Vector3(_cachePositions[i].x + (_cacheSizeX * x - halfSizeX), posY, 0);
			}
		}

		_mesh.SetVertices(_vertices);
		_mesh.RecalculateBounds();

		_beforeSpeedStretchRatio = speedStretchRatio;
	}

	void CheckReleaseNote(double musicTime)
	{
		if (_longInfo.EndTime + _longEndHitTime < musicTime)
		{
			Release();
		}
	}

	public void Release()
	{
		if (_isReleased || !gameObject.activeSelf)
		{
			return;
		}

		_onRelease?.Invoke(false, _touchId, LongInfo.NoteIndex[0], NoteType.Undefined);
		_isReleased = true;
		_pool.Release(this);
	}

	/// <summary>
	/// 途中で離したときにラインの一部のポジションを取り除いたメッシュを再生成する。
	/// InputStartBeatPositionにロングの再スタート拍数を設定する
	/// </summary>
	/// <param name="index"></param>
	public void RemovePosition(int index)
	{
		var range = 1 + index - (_longInfo.BeatPositions.Count - _normalizePositions.Count);
		_normalizePositions.RemoveRange(0, range);
		_cachePositions = new Vector2[_normalizePositions.Count];

		CreateMesh(_normalizePositions);
		ApplySpeedStretchRatio(_beforeSpeedStretchRatio);

		InputStartBeatPosition = _longInfo.BeatPositions[index + 1];

		_currentIndex += range;
	}

	void OnDestroy()
	{
		if (_mesh != null)
		{
			Destroy(_mesh);
			_mesh = null;
		}

		_onRelease = null;
	}
}
