using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "MyScriptable/CameraSettings")]
public sealed class CameraSettings : ScriptableObject
{
	[SerializeField] Vector3 _position;
	[SerializeField] Vector3 _rotation;
	[SerializeField] float _fieldOfView;

	public Vector3 Position => _position;
	public Quaternion Rotation => Quaternion.Euler(_rotation);
	public float FieldOfView => _fieldOfView;
}
