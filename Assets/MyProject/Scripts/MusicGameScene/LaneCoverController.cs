﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem.EnhancedTouch;
using Touch = UnityEngine.InputSystem.EnhancedTouch.Touch;

/// <summary>
/// レーンカバーを管理するクラス.
/// 
/// (OnDisableでTouchAreaクラスのEnhancedTouchSupportの
/// オンオフ切り替えのタイミングに干渉してエラーを引き起こす為、
/// TouchAreaクラスにこのLaneCoverControllerを持たせて明示的に呼び出すことで回避している)
/// </summary>
[Serializable]
public sealed class LaneCoverController
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[SerializeField] GameObject _laneObject;
	[SerializeField] Transform _laneTransform;
	[SerializeField] EventTrigger _laneCoverEventTrigger;
	[SerializeField] GameObject _laneCoverButtonObj;

	//------------------
	// キャッシュ.
	//------------------
	Camera _mainCamera;
	AnimationCurve _animationCurve;
	AnimationCurve _inverseCurve;
	float _position = 0f;
	bool _isEnable = false;
	bool _canMove = false;

	//------------------
	// 定数.
	//------------------
	readonly int _mask = 1 << LayerName.LaneCover;
	const float LANE_LENGTH = 30f;

	/// <summary>
	/// OnDisable時にTouchAreaから呼ばれる
	/// </summary>
	public void OnDisableObject()
	{
		if (!_isEnable)
		{
			return;
		}

		Touch.onFingerMove -= OnFingerMove;
	}

	/// <summary>
	/// 初期化処理
	/// </summary>
	public void Init()
	{
		_isEnable = false;

		_laneCoverButtonObj.SetActive(_isEnable);
		_laneObject.SetActive(_isEnable);

		if (!_isEnable)
		{
			return;
		}

		_animationCurve = GameManager.Instance.NoteScrollAnimationCurve;
		_inverseCurve = CreateInverseCurve(_animationCurve);

		if(!EnhancedTouchSupport.enabled)
        {
			EnhancedTouchSupport.Enable();
			TouchSimulation.Enable();
		}

		Touch.onFingerMove += OnFingerMove;

		_mainCamera = Camera.main;

		//_position = _animationCurve.Evaluate(GameManager.Instance.DisplayOption.LaneCoverPosition / 100f * 30f);
		_laneTransform.localPosition = new Vector3(0f, 0.001f, LANE_LENGTH - (_position - _laneTransform.localScale.x / 2f));

		EventTrigger.Entry entry1 = new EventTrigger.Entry();
		entry1.eventID = EventTriggerType.PointerExit;
		entry1.callback.AddListener((x) => OnPointerExit());
		_laneCoverEventTrigger.triggers.Add(entry1);

		EventTrigger.Entry entry2 = new EventTrigger.Entry();
		entry2.eventID = EventTriggerType.PointerEnter;
		entry2.callback.AddListener((x) => OnPointerEnter());
		_laneCoverEventTrigger.triggers.Add(entry2);
	}

	/// <summary>
	/// レーン上で指を動かしたときに呼ばれる.
	/// マルチタップ対応
	/// </summary>
	/// <param name="finger"></param>
	void OnFingerMove(Finger finger)
	{
		if (!_isEnable || !_canMove)
		{
			return;
		}

		Vector3 screenPoint = finger.currentTouch.screenPosition;
		Ray ray = _mainCamera.ScreenPointToRay(screenPoint);

		if (Physics.Raycast(ray, out var hit, 50f, _mask))
		{
			_laneTransform.localPosition = new Vector3(0f, 0.001f, hit.point.z + _laneTransform.localScale.x / 2f);
			_position = LANE_LENGTH - hit.point.z;
		}
	}

	/// <summary>
	/// タッチポイントがパネルから出た時
	/// </summary>
	void OnPointerExit()
	{
		_canMove = false;
	}

	/// <summary>
	/// タッチポイントがパネルに入った時
	/// </summary>
	void OnPointerEnter()
	{
		_canMove = true;
	}

	/// <summary>
	/// AnimationCurveの逆変換AnimationCurveを生成する
	/// </summary>
	/// <param name="curve">AnimationCurve</param>
	/// <returns>逆変換AnimationCurve</returns>
	AnimationCurve CreateInverseCurve(AnimationCurve curve)
	{
		var resultCurve = new AnimationCurve();

		var keys = curve.keys;
		int keyCount = keys.Length;
		var postWrapmode = curve.postWrapMode;
		resultCurve.postWrapMode = curve.preWrapMode;
		resultCurve.preWrapMode = postWrapmode;

		for (int i = 0; i < keyCount; i++)
		{
			Keyframe K = keys[i];
			var temp = K.time;
			K.time = K.value;
			K.value = temp;
			K.inTangent = 1f / K.inTangent;
			K.outTangent = 1f / K.outTangent;
			keys[i] = K;
		}
		resultCurve.keys = keys;

		return resultCurve;
	}
}
