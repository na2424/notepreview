﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.EnhancedTouch;
using Touch = UnityEngine.InputSystem.EnhancedTouch.Touch;

public enum ScreenTouchPhase : int
{
	None = 0,
	Tap,
	Hold,
	Up
}

public enum JudgeAreaType
{
	Screen,
	World
}

public struct TouchData
{
	public ScreenTouchPhase Phase;
	public float PosX;
	public double TouchTime;
	public int AutoIndex;

	public TouchData(ScreenTouchPhase phase, float posX, double touchTime, int autoIndex = 0)
	{
		Phase = phase;
		PosX = posX;
		TouchTime = touchTime;
		AutoIndex = autoIndex;
	}
}

/// <summary>
/// 入力のタッチの場所等を管理するクラス
/// </summary>
public sealed class TouchArea : InputBase
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[SerializeField] GameObject[] _areaObjects;

	//------------------
	// キャッシュ.
	//------------------
	JudgeAreaType _judgeAreaType;
	MusicManager _musicManager;
	Camera _mainCamera = null;
	Dictionary<int, int> _touchArea = new Dictionary<int, int>();
	Dictionary<int, float> _holdTouchIdScreenPoint = new Dictionary<int, float>();

	bool _isInit = false;

	float _frameRevision;
	float _myScreenPosY;
	Transform[] _areaObjectTransforms;

	//------------------
	// 定数.
	//------------------
	readonly int _mask = 1 << LayerName.JudgeArea;

	//------------------
	// プロパティ.
	//------------------
	public bool Enable { get; set; }
	public bool IsScreenJudgeArea => _judgeAreaType == JudgeAreaType.Screen;

	void OnDisable()
	{
		if (!_isInit)
		{
			DisableTouch();
			return;
		}

		Touch.onFingerDown -= OnFingerDown;
		Touch.onFingerMove -= OnFingerMove;
		Touch.onFingerUp -= OnFingerUp;

		DisableTouch();
	}

	/// <summary>
	/// タッチを有効にする
	/// </summary>
	void EnableTouch()
	{
		if (!EnhancedTouchSupport.enabled)
		{
			EnhancedTouchSupport.Enable();
			TouchSimulation.Enable();
		}
	}

	/// <summary>
	/// タッチを無効にする
	/// </summary>
	void DisableTouch()
    {
		if (EnhancedTouchSupport.enabled)
		{
			TouchSimulation.Disable();
			EnhancedTouchSupport.Disable();
		}
	}

	/// <summary>
	/// 初期化処理
	/// </summary>
	/// <param name="isEnable">有効か</param>
	/// <param name="musicManager">MusicManager</param>
	public void Init(bool isEnable, MusicManager musicManager)
	{
		InitAreaObjects();

		if (!isEnable)
		{
			return;
		}

		_frameRevision = GameManager.Instance.NotesOption.FrameRevision;
		_judgeAreaType = GameManager.Instance.NotesOption.JudgeAreaType;
		
		_musicManager = musicManager;
		_mainCamera = Camera.main;
		_myScreenPosY = _mainCamera.WorldToScreenPoint(transform.position).y;

		// EnhancedTouchの有効化
		EnableTouch();

		InputSystem.pollingFrequency = GameManager.Instance.NotesOption.PollingRate;

		Touch.onFingerDown += OnFingerDown;
		Touch.onFingerMove += OnFingerMove;
		Touch.onFingerUp += OnFingerUp;

		_isInit = true;
	}

	/// <summary>
	/// エリアオブジェクトの初期化
	/// </summary>
	void InitAreaObjects()
	{
		foreach (var area in _areaObjects)
		{
			area.SetActive(false);
		}

		_areaObjectTransforms = new Transform[_areaObjects.Length];

		for (int i = 0; i < _areaObjectTransforms.Length; i++)
		{
			_areaObjectTransforms[i] = _areaObjects[i].transform;
		}
	}

	/// <summary>
	/// 指が画面に触れた時に呼ばれる.
	/// </summary>
	/// <param name="finger"></param>
	private void OnFingerDown(Finger finger)
	{
		if (!Enable)
		{
			return;
		}

		// タッチ設定の判定エリアタイプがスクリーンの場合
		if (IsScreenJudgeArea && finger.currentTouch.screenPosition.y / Screen.height > 0.5f)
		{
			return;
		}

		Vector3 screenPoint = IsScreenJudgeArea ?
			new Vector3(finger.currentTouch.screenPosition.x, _myScreenPosY) :
			finger.currentTouch.screenPosition;

		Ray ray = _mainCamera.ScreenPointToRay(screenPoint);

		if (Physics.Raycast(ray, out var hit, 30f, _mask))
		{
			int touchId = finger.currentTouch.touchId;

			if (_holdTouchIdScreenPoint.ContainsKey(touchId))
			{
				Debug.Log("onDown touchId alredy:" + touchId);
			}

			// MEMO:フレーム誤差補正について
			//
			// 判定する時の「ジャストからのズレ」をこのようにしたいので、
			// ジャストからのズレ =  [タッチした時の曲の再生位置] - ジャストとなる曲の再生位置
			//
			// フレーム誤差補正を次のように対応しています.
			// [タッチした時の曲の再生位置] = 現在の曲の再生位置 - (現在のゲーム内時間 - タッチした時のゲーム内時間) * フレーム誤差補正(0～1)
			var diffTime = Time.realtimeSinceStartupAsDouble - finger.currentTouch.time;
			_touchData[touchId] = new TouchData(ScreenTouchPhase.Tap, hit.point.x, _musicManager.GetMusicTime() - (diffTime * _frameRevision));
			_touchArea[touchId] = -1;

			OnHit(hit.point.x, touchId);
			_holdTouchIdScreenPoint.Add(touchId, hit.point.x);
		}
	}

	/// <summary>
	///	画面に触れた指が動いたときに呼ばれる.
	///	(静止していると呼ばれない).
	/// </summary>
	/// <param name="finger"></param>
	private void OnFingerMove(Finger finger)
	{
		if (!Enable)
		{
			return;
		}

		// タッチ設定の判定エリアタイプがスクリーンの場合
		if (IsScreenJudgeArea && finger.currentTouch.screenPosition.y / Screen.height > 0.5f)
		{
			return;
		}

		Vector3 screenPoint = IsScreenJudgeArea ?
			new Vector3(finger.currentTouch.screenPosition.x, _myScreenPosY) :
			finger.currentTouch.screenPosition;

		Ray ray = _mainCamera.ScreenPointToRay(screenPoint);

		if (Physics.Raycast(ray, out var hit, 30f, _mask))
		{
			int touchId = finger.currentTouch.touchId;
			
			if (_holdTouchIdScreenPoint.ContainsKey(touchId))
			{
				if (_touchData.ContainsKey(touchId))
				{
					// onFingerDownと同一フレームで呼ばれるとtouchIdが同じ場合_touchData[touchId]に上書きされるため対策している.
					return;
				}
				else
				{
					_touchData[touchId] = new TouchData(ScreenTouchPhase.Hold, hit.point.x, _musicManager.GetMusicTime());
					_holdTouchIdScreenPoint[touchId] = hit.point.x;
				}
			}
			else
			{
				// 横から流れるようにタップするとonFingerDownが呼ばれないことがあるため対策している.
				var diffTime = Time.realtimeSinceStartupAsDouble - finger.currentTouch.time;
				_touchData[touchId] = new TouchData(ScreenTouchPhase.Tap, hit.point.x, _musicManager.GetMusicTime() - (diffTime * _frameRevision));
				_holdTouchIdScreenPoint.Add(finger.currentTouch.touchId, hit.point.x);
			}

			if (!_touchArea.ContainsKey(touchId))
			{
				_touchArea[touchId] = -1;
			}

			OnHit(hit.point.x, touchId);
		}

	}

	/// <summary>
	/// 指が画面から離れた時に呼ばれる.
	/// </summary>
	/// <param name="finger"></param>
	private void OnFingerUp(Finger finger)
	{
		if (!Enable)
		{
			return;
		}

		// MEMO: 入れない方が正しい挙動になる
		//if (IsScreenJudgeArea && finger.currentTouch.screenPosition.y / Screen.height > 0.5f)
		//{
		//	return;
		//}

		//Debug.Log("time:" + finger.currentTouch.time + "  starttime:" + finger.currentTouch.startTime + " realtimeSinceStartup:" + Time.realtimeSinceStartup);

		var touchId = finger.currentTouch.touchId;

		float pointX = float.MaxValue;

		if (_holdTouchIdScreenPoint.ContainsKey(touchId))
		{
			pointX = _holdTouchIdScreenPoint[touchId];
			_holdTouchIdScreenPoint.Remove(touchId);
		}
		else
		{
			Vector3 screenPoint = IsScreenJudgeArea ?
			new Vector3(finger.currentTouch.screenPosition.x, _myScreenPosY) :
			finger.currentTouch.screenPosition;

			Ray ray = _mainCamera.ScreenPointToRay(screenPoint);

			if (Physics.Raycast(ray, out var hit, 30f, _mask))
			{
				pointX = hit.point.x;
			}
		}

		var diffTime = Time.realtimeSinceStartupAsDouble - finger.currentTouch.time;
		_touchData[touchId] = new TouchData(ScreenTouchPhase.Up, pointX, _musicManager.GetMusicTime() - (diffTime * _frameRevision));

		if (_touchArea.ContainsKey(touchId) && _touchArea[touchId] != -1)
		{
			_areaObjects[_touchArea[touchId]].SetActive(false);
		}

		_touchArea[touchId] = -1;

	}

	

	public override void CallUpdate(double musicTime)
	{
		// InputSystemに静止中(Stationary)のイベントがない対策.
		if (_holdTouchIdScreenPoint.Count > 0)
		{
			foreach (var touch in _holdTouchIdScreenPoint)
			{
				if (!_touchData.ContainsKey(touch.Key))
				{
					_touchData[touch.Key] = new TouchData(ScreenTouchPhase.Hold, touch.Value, musicTime);
				}
			}
		}
	}

	void OnHit(float xPos, int touchId)
	{
		float minDistance = float.MaxValue;
		int nearArea = 0;

		for (int i = 0; i < _areaObjectTransforms.Length; i++)
		{
			var areaXPos = _areaObjectTransforms[i].localPosition.x;
			var distance = Mathf.Abs(xPos - areaXPos);

			if (distance < minDistance)
			{
				minDistance = distance;
				nearArea = i;
			}
		}

		if (_touchArea[touchId] != -1 && _touchArea[touchId] != nearArea)
		{
			_areaObjects[_touchArea[touchId]].SetActive(false);
		}

		_touchArea[touchId] = nearArea;

		_areaObjects[_touchArea[touchId]].SetActive(true);
	}

	public override void OnFinalize()
	{
		_touchArea.Clear();
		_holdTouchIdScreenPoint.Clear();
		_touchArea = null;
		_holdTouchIdScreenPoint = null;

		base.OnFinalize();
	}
}
