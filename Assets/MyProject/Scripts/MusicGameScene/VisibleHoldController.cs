﻿using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ホールド中の判定ライン上のホールド位置を表示するクラス
/// </summary>
public sealed class VisibleHoldController : MonoBehaviour
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[SerializeField] Transform[] _visibleHoldTransforms;
	[SerializeField] NoteTexture _noteTexture;

	//------------------
	// キャッシュ.
	//------------------
	int _index = 0;
	GameObject[] _visibleHoldObjects;
	Material[] _materials;
	Dictionary<long, int> _hashToIndex = new Dictionary<long, int>(8);
	HashSet<int> _activeIndex = new HashSet<int>();
	Vector3 _xOne = Vector3.right;

	readonly static int _mainTexId = Constant.ShaderProperty.MainTexId;

	/// <summary>
	/// 初期化処理
	/// </summary>
	public void Init()
	{
		_visibleHoldObjects = new GameObject[_visibleHoldTransforms.Length];

		float size = GameManager.Instance.NotesOption.Size;
		_materials = new Material[_visibleHoldTransforms.Length];

		for (int i = 0; i < _visibleHoldTransforms.Length; i++)
		{
			_visibleHoldObjects[i] = _visibleHoldTransforms[i].gameObject;
			_visibleHoldTransforms[i].localScale *= size;
			_materials[i] = _visibleHoldTransforms[i].GetComponent<MeshRenderer>().material;
		}
	}

	/// <summary>
	/// 表示/非表示切り替え
	/// </summary>
	/// <param name="isActive">アクティブか</param>
	/// <param name="touchId">タッチID</param>
	/// <param name="noteType">ノートの種類</param>
	/// <param name="noteTypeToTexture">テクスチャ取得のためのコールバック</param>
	public void SetActive(bool isActive, int touchId, int startNoteIndex, NoteType noteType = NoteType.Undefined)
	{
		long hash = 0;
		hash ^= touchId + 0x9e3779b9 + (hash << 6) + (hash >> 2);
		hash ^= startNoteIndex + 0x9e3779b9 + (hash << 6) + (hash >> 2);

		if (isActive)
		{
			NextIndex();

			// まだロング稼働中のIndexだった場合は次のIndexへスキップする
			if (_activeIndex.Contains(_index))
			{
				NextIndex();
			}

			_hashToIndex.Add(hash, _index);

			int index = _hashToIndex[hash];

			_materials[index].SetTexture(_mainTexId, _noteTexture.NoteTypeToTexture(noteType));
			_visibleHoldObjects[index].SetActive(true);
			_activeIndex.Add(index);
		}
		else
		{
			if (_hashToIndex.ContainsKey(hash))
			{
				int index = _hashToIndex[hash];
				_visibleHoldObjects[index].SetActive(false);
				_hashToIndex.Remove(hash);
				_activeIndex.Remove(index);
			}
		}
	}

	/// <summary>
	/// ホールド位置の更新
	/// </summary>
	/// <param name="posX">X座標</param>
	/// <param name="touchId">タッチID</param>
	public void SetPosition(float posX, int touchId, int startNoteIndex)
	{
		long hash = 0;
		hash ^= touchId + 0x9e3779b9 + (hash << 6) + (hash >> 2);
		hash ^= startNoteIndex + 0x9e3779b9 + (hash << 6) + (hash >> 2);

		if (_hashToIndex.ContainsKey(hash))
		{
			_visibleHoldTransforms[_hashToIndex[hash]].localPosition = _xOne * posX;
		}
	}

	/// <summary>
	/// 次のIndex番号に更新
	/// </summary>
	void NextIndex()
	{
		_index++;
		if (_index >= _visibleHoldTransforms.Length)
		{
			_index = 0;
		}
	}

	void OnDestroy()
	{
		if (_materials == null)
		{
			return;
		}

		for (int i = 0; i < _materials.Length; i++)
		{
			Destroy(_materials[i]);
			_materials[i] = null;
		}

		_materials = null;

		_hashToIndex.Clear();
		_hashToIndex = null;

		_activeIndex.Clear();
		_activeIndex = null;
	}
}
