﻿
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// オートプレイ時のタッチ入力を生成するクラス
/// </summary>
public sealed class AutoPlay : InputBase
{
	//------------------
	// キャッシュ.
	//------------------
	Sequence _sequence;
	BpmHelper _bpmHelper = new BpmHelper();
	int _noteIndex = 0;
	Dictionary<int, int> _holdNoteIndexDict = new Dictionary<int, int>();

	//------------------
	// 定数.
	//------------------
	static readonly float LANE_DISTANCE = Constant.Note.LANE_DISTANCE;
	static readonly float OFFSET_X = Constant.Note.OFFSET_X;

	// 1フレーム当たりにAutoPlayがロングを叩ける回数
	const int FINGER_LONG_PER_FRAME = 5;

	public void Init(Sequence sequence, SongInfo songInfo)
	{
		_sequence = sequence;
		_bpmHelper.Init(songInfo);
	}

	public void SetMidPlay(double musicTime)
	{
		var beat = _bpmHelper.TimeToBeat(musicTime);

		for (int i = 0; i < _sequence.BeatPositions.Count; i++)
		{
			if (beat <= _sequence.BeatPositions[i])
			{
				_noteIndex = i;
				break;
			}
		}
	}

	public override void CallUpdate(double musicTime)
	{
		_touchData.Clear();
		_holdNoteIndexDict.Clear();

		double beat = _bpmHelper.TimeToBeat(musicTime);
		int invalidLongIndex = -1;

		while (true)
		{
			if (_noteIndex < _sequence.BeatPositions.Count && _sequence.BeatPositions[_noteIndex] < beat)
			{
				var posX = _sequence.Lanes[_noteIndex] * LANE_DISTANCE - OFFSET_X;
				var noteType = _sequence.NoteTypes[_noteIndex];

				var phase = GetPhase(noteType);

				int touchNoteIndex = _noteIndex;

				if (phase == ScreenTouchPhase.Hold)
				{
					if (_holdNoteIndexDict.TryGetValue(_noteIndex, out var value))
					{
						touchNoteIndex = value;
					}
				}

				if (!_touchData.ContainsKey(_noteIndex))
				{
					_touchData.Add(_noteIndex, new TouchData(phase, posX, _bpmHelper.BeatToTime(_sequence.BeatPositions[_noteIndex]), touchNoteIndex));
				}

				_noteIndex++;
			}
			else
			{
				break;
			}
		}


		for (int fingerIndex = 0; fingerIndex < FINGER_LONG_PER_FRAME; fingerIndex++)
		{
			LongInfo longInfo = null;

			// ホールド中にすべきlongInfoを選ぶ
			for (int i = invalidLongIndex + 1; i < _sequence.LongInfo.Count; i++)
			{
				if (i == invalidLongIndex)
				{
					continue;
				}

				var last = _sequence.LongInfo[i].BeatPositions.Count - 1;

				if (_sequence.LongInfo[i].BeatPositions[0] < beat && beat < _sequence.LongInfo[i].BeatPositions[last])
				{
					invalidLongIndex = i;
					longInfo = _sequence.LongInfo[i];
					break;
				}
			}

			if (longInfo != null)
			{
				// レーンを跨ぐロングノートで現在のX座標を求めて_touchDataに入れる
				for (int i = 0; i < longInfo.BeatPositions.Count - 1; i++)
				{
					if (longInfo.BeatPositions[i] < beat && beat < longInfo.BeatPositions[i + 1])
					{
						float longPosX = Mathf.Lerp(
							longInfo.Lanes[i] * LANE_DISTANCE - OFFSET_X,
							longInfo.Lanes[i + 1] * LANE_DISTANCE - OFFSET_X,
							(float)((beat - longInfo.BeatPositions[i]) / (longInfo.BeatPositions[i + 1] - longInfo.BeatPositions[i]))
						);

						if (!_touchData.ContainsKey(longInfo.NoteIndex[0]))
						{
							_touchData.Add(longInfo.NoteIndex[0], new TouchData(ScreenTouchPhase.Hold, longPosX, _bpmHelper.BeatToTime(longInfo.BeatPositions[i]), longInfo.NoteIndex[0]));

							if (0 < i - 1)
							{
								_holdNoteIndexDict.Add(longInfo.NoteIndex[i - 1], longInfo.NoteIndex[0]);
							}
						}
						break;
					}
				}
			}
		}
		
	}

	ScreenTouchPhase GetPhase(NoteType noteType) => noteType switch
	{
		NoteType.Normal => ScreenTouchPhase.Tap,
		NoteType.LongStart => ScreenTouchPhase.Tap,
		NoteType.LongRelay => ScreenTouchPhase.Hold,
		NoteType.LongEnd => ScreenTouchPhase.Up,
		NoteType.Fuzzy => ScreenTouchPhase.Up,
		NoteType.FuzzyLongStart => ScreenTouchPhase.Tap,
		NoteType.FuzzyLongRelay => ScreenTouchPhase.Hold,
		NoteType.FuzzyLongEnd => ScreenTouchPhase.Hold,
		_ => ScreenTouchPhase.Tap
	};

	public override void OnFinalize()
	{
		_sequence = null;
		_holdNoteIndexDict.Clear();
		_holdNoteIndexDict = null;

		base.OnFinalize();
	}
}
