﻿using Cysharp.Threading.Tasks;
using Dialog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;

/// <summary>
/// ゲームのメインロジックを担当するクラス
/// </summary>
[Serializable]
public sealed class MusicGame
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[SerializeField] NoteObjectPool _noteObjectPool;
	[SerializeField] LongObjectPool _longObjectPool;
	[SerializeField] JudgeEffectPool _judgeEffectPool;
	[SerializeField] BeatBarObjectPool _beatBarObjectPool;
	[SerializeField] SameTimeBar _sameTimeBar;

	[SerializeField] VisibleHoldController _visibleHoldController;
	[SerializeField] NoteTexture _noteTexture;
	[SerializeField] LongTexture _longTexture;

	public event Action<JudgeType> OnMissedNote
	{
		add { _noteObjectPool.OnMissedNote += value; }
		remove { _noteObjectPool.OnMissedNote -= value; }
	}

	//------------------
	// キャッシュ.
	//------------------
	SequenceReader sequenceReader = new SequenceReader();
	Sequence _sequence;
	JudgeManager _judgeManager;
	TimingHistory _timingHistory = new TimingHistory();
	NoFuzzyTimingHistory _noFuzzyTimingHistory = new NoFuzzyTimingHistory();
	BpmHelper _bpmHelper = new BpmHelper();
	SpeedStretchRatio _speedStretchRatio = new SpeedStretchRatio();
	Action<bool, int, int, NoteType> _onReleaseLong;
	int _bar = 0;
	int _noteIndex = 0;
	int _longIndex = 0;
	bool _enableBeatBar = true;
	bool _enableJudgeEffect = true;
	bool _isCmod = false;
	bool _isAuto = false;

	InputBase _input;
	Dictionary<int, int> _upNoteTouchIdDict = new Dictionary<int, int>(8);
	HashSet<int> _skipNoteIndex = new HashSet<int>();
	HashSet<int> _hitTouchId = new HashSet<int>();
	HashSet<int> _releasedLongEndNoteIndex = new HashSet<int>();
	HashSet<int> _currentholdTouchId = new HashSet<int>();
	Queue<int> _checkLongNoteIndexQueue = new Queue<int>();

	float _longDistanceRevision = 0f;
	List<int> _sortedKeyList = new List<int>();

	List<float> _bgChangePositions;
	List<string> _bgChangeImageName;
	int _bgChangeIndex = 0;

	// 判定の横幅
	float _judgeDistance = Constant.JudgeTime.JUDGE_DISTANCE;

	double _beforeTouchSeJustTime = -1f;
	double _beforeTouchSeFuzzyJustTime = -1f;

	//------------------
	// 定数.
	//------------------
	const float FIND_NOTE_MARGIN_TIME = 0.2f;
	const int CREATABLE_NOTES_PER_FRAME = 16;
	const int CREATABLE_BARS_PER_FRAME = 16;

	//------------------
	// プロパティ.
	//------------------
	// 総ノーツ数
	public int TotalNote => _sequence.BeatPositions.Count;
	// 譜面データ
	public Sequence Sequence => _sequence;
	// 判定履歴
	public TimingHistory TimingHistrory => _timingHistory;
	public NoFuzzyTimingHistory NoFuzzyTimingHistory => _noFuzzyTimingHistory;
	// 開始の拍
	float StartBeatBar { get; set; }
	// 最後のノーツのある時間
	public double LastNoteTime { get; private set; }

	public async UniTask<bool> InitAsync(CancellationToken ct, SongInfo songInfo, InputBase input, JudgeManager judgeManager)
	{
		_beatBarObjectPool.Init();
		_noteObjectPool.Init();
		_longObjectPool.Init(_bpmHelper);

		_judgeEffectPool.Init();
		_visibleHoldController.Init();

		_enableBeatBar = GameManager.Instance.DisplayOption.BeatBar;
		_enableJudgeEffect = GameManager.Instance.DisplayOption.NotesEffect;
		_isAuto = true;
		_isCmod = GameManager.Instance.IsCmod;
		_bgChangePositions = songInfo.BgChangePositions;
		_bgChangeImageName = songInfo.BgChangeImageName;

		_judgeDistance = _isAuto ? 0.02f : Constant.JudgeTime.JUDGE_DISTANCE;

		_input = input;
		_judgeManager = judgeManager;
		_onReleaseLong = _visibleHoldController.SetActive;

		_longDistanceRevision = GameManager.Instance.JudgeTimeOption.LongRevisionDistance;

		_sequence = await sequenceReader.ReadAsync(ct, songInfo);

		_bpmHelper.Init(songInfo);
		_speedStretchRatio.Init(songInfo);

		_sameTimeBar.Init(_bpmHelper, _sequence);

		if (
			_sequence == null ||
			_sequence.BeatPositions == null ||
			_sequence.BeatPositions.Count == 0
		)
		{
			var builder = new DialogParametor.Builder("エラー", "ノートが1つも読まれていません。\n譜面ファイルの仕様を確認してください。");
			builder.AddDefaultAction("選曲画面に戻る", () => GameManager.Instance.ChangeScene(SceneName.SelectMusic));
			builder.AddCallbackOnAutoClosed(() => GameManager.Instance.ChangeScene(SceneName.SelectMusic));
			DialogManager.Instance.Open(builder.Build());
			return false;
		}

		CalculateStartBeatBar();
		CalculateLastNoteTime();

		_bar = (int)Mathf.Min(StartBeatBar, (int)NoteController.VisibleBeat + 1);

		return true;
	}

	void CalculateStartBeatBar()
	{
		StartBeatBar = (int)_sequence.BeatPositions.First();
	}

	void CalculateLastNoteTime()
	{
		var lastNoteBeatPosition = _sequence.BeatPositions.Last();
		LastNoteTime = _bpmHelper.BeatToTime(lastNoteBeatPosition);
	}

	public void SetMidPlay(double musicTime)
	{
		var beat = _bpmHelper.TimeToBeat(musicTime);

		_bar = (int)beat;

		for (int i = 0; i < _sequence.BeatPositions.Count; i++)
		{
			if (beat <= _sequence.BeatPositions[i])
			{
				_noteIndex = i;
				break;
			}
		}

		for (int i = 0; i < _sequence.LongInfo.Count; i++)
		{
			if (beat <= _sequence.LongInfo[i].BeatPositions[0])
			{
				_longIndex = i;
				break;
			}
		}

		for (int i = 0; i < _bgChangePositions.Count; i++)
		{
			if (beat <= _bgChangePositions[i])
			{
				_bgChangeIndex = i;
				break;
			}
		}
	}

	public void AddMissedTimingHistory(double musicTime)
	{
		_timingHistory.Add(musicTime, 1000d, JudgeType.Missed);
	}

	public void CallUpdate(double musicTime)
	{
		double beat = _bpmHelper.TimeToBeat(musicTime);

		// TouchIdの順番に並び替えたListを作る
		// _sortedKeyListに結果が入る
		SortTouchIdList();

		//-----------------------------------------
		// 判定最優先
		//-----------------------------------------
		// Note判定処理
		foreach (int key in _sortedKeyList)
		{
			var value = _input.TouchData[key];

			// タップ判定
			JudgeTap(value, key);

			// 離し判定
			JudgeUp(value, key);

			// ホールド(押しっぱなし)判定
			JudgeHold(value, key);
		}

		//----------------------------------------
		// ノート処理
		//----------------------------------------

		// 加速・減速ギミックによるスピード倍率を更新
		_speedStretchRatio.CallUpdate(beat);

		// Note生成
		for (int i = 0; i < CREATABLE_NOTES_PER_FRAME; i++)
		{
			if (!NeedsCreateNote(beat, musicTime))
			{
				break;
			}

			if (_skipNoteIndex.Contains(_noteIndex))
			{
				_skipNoteIndex.Remove(_noteIndex);
			}
			else
			{
				CreateNoteFromObjectPool();
			}

			_checkLongNoteIndexQueue.Enqueue(_noteIndex);
			_noteIndex++;
		}

		// Note位置更新
		for (int i = 0; i < _noteObjectPool.CurrentActiveNote.Count; i++)
		{
			_noteObjectPool.CurrentActiveNote[i].OnUpdate(beat, musicTime, SpeedStretchRatio.CurrentValue);
		}

		//----------------------------------------
		// ロング処理
		//----------------------------------------

		// Long生成
		for (int i = 0; i < _checkLongNoteIndexQueue.Count; i++)
		{
			int longNoteIndex = _checkLongNoteIndexQueue.Dequeue();

			if (!NeedsCreateLong(longNoteIndex))
			{
				continue;
			}

			CreateLongFromObjectPool();

			_longIndex++;
		}

		// Long位置更新
		for (int i = 0; i < _longObjectPool.CurrentActiveLong.Count; i++)
		{
			_longObjectPool.CurrentActiveLong[i].OnUpdate(beat, musicTime, SpeedStretchRatio.CurrentValue);
		}

		// Long判定処理
		// (MEMO: Long位置更新の後でないと1フレーム分、ロングの手前表示がずれるので注意)
		for (int i = 0; i < _sortedKeyList.Count; i++)
		{
			var value = _input.TouchData[_sortedKeyList[i]];

			int touchId = _sortedKeyList[i];

			if (_isAuto)
			{
				touchId = value.AutoIndex;
			}

			JudgeLong(beat, touchId, value);
		}

		//---------------------------------------
		// 同時押しライン
		//---------------------------------------

		_sameTimeBar.CallUpdate(beat, musicTime, SpeedStretchRatio.CurrentValue);

		//----------------------------------------
		// 拍線処理
		//----------------------------------------

		// 拍線生成
		for (int i = 0; i < CREATABLE_BARS_PER_FRAME; i++)
		{
			if (!NeedsCreateBeatBar(beat, musicTime))
			{
				break;
			}

			if (_enableBeatBar)
			{
				CreateBeatBarFromObjectPool();
			}
			_bar++;
		}

		// 拍線位置更新
		for (int i = 0; i < _beatBarObjectPool.CurrentActiveBar.Count; i++)
		{
			_beatBarObjectPool.CurrentActiveBar[i].OnUpdate(beat, musicTime, SpeedStretchRatio.CurrentValue);
		}

		//----------------------------------------
		// BgChange処理
		//----------------------------------------
		CheckBgChange(beat);
	}

	/// <summary>
	/// タッチIDの順に並び変えたリストを作る
	/// </summary>
	void SortTouchIdList()
	{
		_sortedKeyList.Clear();

		foreach (var touch in _input.TouchData)
		{
			_sortedKeyList.Add(touch.Key);
		}

		// MEMO: GC対策
		// 比較関数が外部環境を参照しない場合、比較関数は内部でキャッシュされて使い回されます
		_sortedKeyList.Sort((a, b) => a - b);
	}

	/// <summary>
	/// 拍線生成する必要があるか
	/// </summary>
	/// <param name="beat">Beat位置</param>
	bool NeedsCreateBeatBar(double beat, double musicTime)
	{
		return
			_noteIndex < _sequence.BeatPositions.Count &&
			(_isCmod ?
			(musicTime > _bpmHelper.BeatToTime(_bar) - NoteController.VisibleTime) :
			(beat > _bar - NoteController.VisibleBeat));
	}

	/// <summary>
	/// オブジェクトプールから拍線生成
	/// </summary>
	void CreateBeatBarFromObjectPool()
	{
		var controller = _beatBarObjectPool.Pool.Get();
		double justTime = _bpmHelper.BeatToTime(_bar);

		controller.SetParam(
			beatPosition: _bar,
			justTime: justTime
		);
	}

	/// <summary>
	/// Note生成する必要があるか
	/// </summary>
	/// <param name="beat">Beat位置</param>
	bool NeedsCreateNote(double beat, double musicTime)
	{
		bool isFixedVisibleTime = false;
		double visibleTime = NoteController.VisibleTime;

		if (visibleTime < 0.2f)
		{
			isFixedVisibleTime = true;
			visibleTime = 0.78f;
		}

		return
			_noteIndex < _sequence.BeatPositions.Count &&
			((_isCmod || isFixedVisibleTime) ?
			(musicTime > _bpmHelper.BeatToTime(_sequence.BeatPositions[_noteIndex]) - visibleTime) :
			(beat > _sequence.BeatPositions[_noteIndex] - NoteController.VisibleBeat));
	}

	/// <summary>
	/// オブジェクトプールからNote生成
	/// </summary>
	void CreateNoteFromObjectPool()
	{
		var noteController = _noteObjectPool.Pool.Get();
		double justTime = _bpmHelper.BeatToTime(_sequence.BeatPositions[_noteIndex]);

		noteController.SetParam(
			beatPosition: _sequence.BeatPositions[_noteIndex],
			justTime: justTime,
			lane: _sequence.Lanes[_noteIndex],
			noteTex: _noteTexture.NoteTypeToTexture(_sequence.NoteTypes[_noteIndex]),
			noteType: _sequence.NoteTypes[_noteIndex],
			noteIndex: _noteIndex
		);

		// 離し判定を有効にするTouchIdを設定
		if (_sequence.NoteTypes[_noteIndex] == NoteType.LongEnd || _sequence.NoteTypes[_noteIndex] == NoteType.FuzzyLongEnd)
		{
			if (_upNoteTouchIdDict.TryGetValue(_noteIndex, out var upTouchId))
			{
				noteController.UpTouchId = upTouchId;
				_upNoteTouchIdDict.Remove(_noteIndex);
			}
		}

		// 同時押しラインの生成判定に使用する
		_sameTimeBar.OnCreateNote(_noteIndex);
	}

	/// <summary>
	/// Long生成する必要があるか
	/// </summary>
	/// <param name="beat">Beat位置</param>
	bool NeedsCreateLong(int noteIndex)
	{
		return
			(_sequence.NoteTypes[noteIndex] == NoteType.LongStart ||
			 _sequence.NoteTypes[noteIndex] == NoteType.FuzzyLongStart) &&
			noteIndex < _sequence.BeatPositions.Count;
	}

	/// <summary>
	/// オブジェクトプールからLong生成
	/// </summary>
	void CreateLongFromObjectPool()
	{
		var longController = _longObjectPool.Pool.Get();

		LongInfo longInfo = _sequence.LongInfo[_longIndex];

		longController.SetParam(longInfo, _onReleaseLong, _longTexture.LongTypeToTexture(longInfo.LongType));

		foreach (var note in _noteObjectPool.CurrentActiveNote)
		{
			if (note.LongController == null && note.NoteIndex == longInfo.StartNoteIndex)
			{
				//Debug.Log(note.NoteIndex);
				note.SetLongController(longController);
				break;
			}
		}

	}

	/// <summary>
	/// タップ判定
	/// </summary>
	/// <param name="musicTime">再生時間</param>
	/// <param name="touch">タッチ情報</param>
	void JudgeTap(TouchData touch, int touchId)
	{
		if (touch.Phase != ScreenTouchPhase.Tap)
		{
			return;
		}

		// 各ノートとの時間差を測り、最も近い時間を求める
		double minTime = FindMinTime(touch.TouchTime, touch.PosX, ScreenTouchPhase.Tap, NoteType.Normal, NoteType.LongStart, NoteType.FuzzyLongStart, NoteType.Fuzzy);

		// 最も近い距離時間の中でX座標の位置が近いノートを選ぶ
		NoteController nearNote = FindNearNote(touch.TouchTime, minTime, touch.PosX, NoteType.Normal, NoteType.LongStart, NoteType.FuzzyLongStart, NoteType.Fuzzy);

		// ファジーをタップ判定しない
		if (
			nearNote != null &&
			((nearNote.NoteType == NoteType.FuzzyLongStart && !_isAuto) ||
			nearNote.NoteType == NoteType.Fuzzy))
		{
			return;
		}

		// 判定する対象があれば判定処理
		if (nearNote != null)
		{
			JudgeTiming(nearNote, touch.TouchTime, ScreenTouchPhase.Tap, touchId);
		}
		else
		{
			if (minTime > 0.26f)
			{
				TouchSEManager.Instance.Play(TouchSEType.NullTap);
			}
		}
	}

	/// <summary>
	/// 離し判定
	/// </summary>
	/// <param name="musicTime">再生時間</param>
	/// <param name="touch">タッチ情報</param>
	void JudgeUp(TouchData touch, int touchId)
	{
		if (touch.Phase != ScreenTouchPhase.Up)
		{
			return;
		}

		// 各ノートとの時間差を測り、最も近い時間を求める
		double minTime = FindMinTime(touch.TouchTime, touch.PosX, ScreenTouchPhase.Up, NoteType.LongEnd, NoteType.FuzzyLongEnd, NoteType.Fuzzy, NoteType.Undefined, NoteType.Undefined, touchId);

		// 最も近い距離時間の中でX座標の位置が近いノートを選ぶ
		NoteController nearNote = FindNearNote(touch.TouchTime, minTime, touch.PosX, NoteType.LongEnd, NoteType.FuzzyLongEnd, NoteType.Fuzzy);

		// 判定する対象があれば判定処理
		if (nearNote != null)
		{
			JudgeTiming(nearNote, touch.TouchTime, ScreenTouchPhase.Up, touchId);
		}

		if (_hitTouchId.Contains(touchId) && !_isAuto)
		{
			_hitTouchId.Remove(touchId);
		}
	}

	/// <summary>
	/// 押しっぱなし判定
	/// </summary>
	/// <param name="musicTime">再生時間</param>
	/// <param name="touch">タッチ情報</param>
	void JudgeHold(TouchData touch, int touchId)
	{
		if (touch.Phase != ScreenTouchPhase.Hold)
		{
			return;
		}

		// 各ノートとの時間差を測り、最も近い時間を求める
		double minTime = FindMinTime(touch.TouchTime, touch.PosX, ScreenTouchPhase.Hold, NoteType.Fuzzy, NoteType.LongRelay, NoteType.FuzzyLongStart, NoteType.FuzzyLongRelay, NoteType.FuzzyLongEnd);

		// 最も近い距離時間の中でX座標の位置が近いノートを選ぶ
		NoteController nearNote = FindNearNote(touch.TouchTime, minTime, touch.PosX, NoteType.Fuzzy, NoteType.LongRelay, NoteType.FuzzyLongStart, NoteType.FuzzyLongRelay, NoteType.FuzzyLongEnd);

		// 判定する対象があれば判定処理
		if (nearNote != null)
		{
			JudgeTiming(nearNote, touch.TouchTime, ScreenTouchPhase.Hold, touchId);
		}
	}

	/// <summary>
	/// ロングのホールドの開始判定時に呼ばれる
	/// </summary>
	/// <param name="touchId"></param>
	/// <param name="holdLong"></param>
	void OnJudgeStartHold(int touchId, LongController holdLong)
	{
		// ホールド中のタッチIDを保持
		SafeAddCurrentHoldTouchId(touchId);

		// LongのUpを有効にする
		int lastIndex = holdLong.LongInfo.NoteIndex.Count - 1;
		int upNoteIndex = holdLong.LongInfo.NoteIndex[lastIndex];

		// 一度、表示外ノートに設定する用のDictにTouchIdを格納
		_upNoteTouchIdDict[upNoteIndex] = touchId;

		// 現在表示されているノートはDictから取り除きつつ、upNoteIndexのノートにTouchIdを設定
		foreach (var note in _noteObjectPool.CurrentActiveNote)
		{
			if (note.NoteIndex == upNoteIndex)
			{
				_upNoteTouchIdDict.Remove(upNoteIndex);
				note.UpTouchId = touchId;
				break;
			}
		}

		holdLong.SetTouchId(touchId);
		_visibleHoldController.SetActive(true, holdLong.TouchId, holdLong.LongInfo.NoteIndex[0], (holdLong.LongInfo.LongType == LongType.Long) ? NoteType.LongStart : NoteType.FuzzyLongStart);
		_visibleHoldController.SetPosition(holdLong.CurrentPosX, holdLong.TouchId, holdLong.LongInfo.NoteIndex[0]);
	}

	/// <summary>
	/// ロング(ライン)判定
	/// (TODO:処理が多いので整理したい)
	/// ・ロングのホールド時に判定ライン下のステンシルマスク(holdLong.UseStencil)を有効にする.
	/// ・ロングのホールド時にホールドオブジェクト(_holdActiveController)を表示する.
	/// ・ロングのホールド時の指の位置は開始、中継、終了の位置が合っていれば何処でも良く、指さえ離さなければロングは表示されたままにする.
	/// ・ロングを途中で離したときにロングを消す処理 (緑ロングと青ロングで処理が異なる).
	/// ・ロングを途中で離したときに後続のノートを消す処理と判定.
	/// ・TouchIdが未設定のロングの場合、一番近いロングを選ぶ処理.
	/// ・TouchIdが設定済みのロングの場合、近さよりも値が一致するロングを選ぶ処理.
	/// </summary>
	/// <param name="beat">Beat位置</param>
	/// <param name="touchId">タッチID</param>
	/// <param name="touch">TouchData</param>
	void JudgeLong(double beat, int touchId, TouchData touch)
	{
		double musicTime = touch.TouchTime;
		LongController holdLong = null;
		LongController releaseLong = null;

		float minDistance = float.MaxValue;

		for (int i = 0; i < _longObjectPool.CurrentActiveLong.Count; i++)
		{
			LongController @long = _longObjectPool.CurrentActiveLong[i];

			if (@long.InputStartBeatPosition <= beat && beat <= @long.EndBeatPosition)
			{
				if (touch.Phase == ScreenTouchPhase.Hold)
				{
					if (@long.TouchId == -1)
					{
						if (!_currentholdTouchId.Contains(touchId))
						{
							// タッチIDが未設定のラインは近いものを選ぶ
							var distance = Mathf.Abs(touch.PosX - @long.CurrentPosX);

							// MEMO: ラインの横幅は補正を行なわない
							if (distance < _judgeDistance)
							{
								if (distance < minDistance)
								{
									minDistance = distance;
									holdLong = @long;
									//Debug.Log("distance : " +distance + " long noteIndex:" + @long.LongInfo.NoteIndex[0]);
								}
							}
						}
					}
					else if (@long.TouchId == touchId)
					{
						holdLong = @long;
						break;
					}
				}
				else if (touch.Phase == ScreenTouchPhase.Up)
				{
					if (@long.TouchId == touchId)
					{
						releaseLong = @long;
						break;
					}
					//else
					//{
					//	Debug.Log("Up TouchId None (@long.TouchId , touchId): " + @long.TouchId + " , " + touchId);// 動作確認中...
					//}
				}
			}

			if (@long.EndBeatPosition < beat)
			{
				_visibleHoldController.SetActive(false, @long.TouchId, @long.LongInfo.NoteIndex[0]);

				// ホールド中のタッチIDを取り除く
				SafeRemoveCurrentHoldTouchId(@long.TouchId);
			}
		}

		bool hasHold = false;

		if (holdLong != null)
		{
			// ホールド判定
			if (touch.Phase == ScreenTouchPhase.Hold)
			{
				if (holdLong.TouchId == -1)
				{
					OnJudgeStartHold(touchId, holdLong);
					hasHold = true;
				}
				else if (holdLong.TouchId == touchId)
				{
					_visibleHoldController.SetPosition(holdLong.CurrentPosX, holdLong.TouchId, holdLong.LongInfo.NoteIndex[0]);
					hasHold = true;
				}
			}

			holdLong.UseStencil(hasHold);
		}

		if (releaseLong != null)
		{
			// ホールド中のタッチIDを取り除く
			SafeRemoveCurrentHoldTouchId(touchId);

			// ファジーロングを途中で離したときの処理
			// ・中継間のラインだけ消す(ノートは残す)
			if (releaseLong.LongInfo.LongType == LongType.FuzzyLong)
			{
				int count = releaseLong.LongInfo.BeatPositions.Count;
				for (int i = 0; i < count - 1; i++)
				{
					if (releaseLong.LongInfo.BeatPositions[i] <= beat && beat < releaseLong.LongInfo.BeatPositions[i + 1])
					{
						releaseLong.UseStencil(false);

						if (i == releaseLong.LongInfo.BeatPositions.Count - 2)
						{
							releaseLong.Release();
						}
						else
						{
							releaseLong.RemovePosition(i);
							releaseLong.SetTouchId(-1);
							_visibleHoldController.SetActive(false, touchId, releaseLong.LongInfo.NoteIndex[0]);
						}

						break;
					}
				}
			}

			// 青ロングを離した時に後続のノートをReleaseする
			// ファジーロングは間のラインを消すだけだが、
			// ロングは３通りある
			// ・最後のポジション間のライン → 最後のラインと終端ノートを消す(Missにする)
			// ・最後から1つ手前のポジション間のライン → 最後までのライン2つと最後の中継と終端ノートを消す(※2つMissになる)
			// ・その他のポジション間のライン → 2つ先までのラインを消し、ライン間にある中継ノートを消す(Missにする)
			if (releaseLong.LongInfo.LongType == LongType.Long)
			{
				int count = releaseLong.LongInfo.BeatPositions.Count;
				for (int i = 0; i < count - 1; i++)
				{
					if (releaseLong.LongInfo.BeatPositions[i] <= beat && beat < releaseLong.LongInfo.BeatPositions[i + 1])
					{
						releaseLong.UseStencil(false);

						// 最後
						if (i == releaseLong.LongInfo.BeatPositions.Count - 2)
						{
							var absDiffTime = Math.Abs(musicTime - releaseLong.EndTime);
							int last = releaseLong.LongInfo.NoteIndex.Count - 1;
							var lastNoteIndex = releaseLong.LongInfo.NoteIndex[last];
							var note = FindNoteFromNoteIndex(lastNoteIndex);

							if (note != null)
							{
								note.Release(absDiffTime >= _judgeManager.LongHitTime);
							}
							else if (!_releasedLongEndNoteIndex.Contains(lastNoteIndex))
							{
								// まだ生成されていない先のノートを生成しないようにする
								_skipNoteIndex.Add(lastNoteIndex);
								_noteObjectPool.AddMissed();
							}
							else
							{
								_releasedLongEndNoteIndex.Remove(lastNoteIndex);
							}

							releaseLong.Release();

						}
						else
						{
							NoteController note = FindNoteFromNoteIndex(releaseLong.LongInfo.NoteIndex[i]);

							if (note != null)
							{
								note.Release(true);
							}
							else
							{
								// まだ生成されていない先のノートを生成しないようにする
								_skipNoteIndex.Add(releaseLong.LongInfo.NoteIndex[i]);
								_noteObjectPool.AddMissed();
							}

							if (i + 1 == releaseLong.LongInfo.NoteIndex.Count - 1)
							{
								// 1つ手前
								var lastNoteIndex = releaseLong.LongInfo.NoteIndex.Last();
								note = FindNoteFromNoteIndex(lastNoteIndex);

								if (note != null)
								{
									note.Release(true);
								}
								else if (!_releasedLongEndNoteIndex.Contains(lastNoteIndex))
								{
									_skipNoteIndex.Add(lastNoteIndex);
									_noteObjectPool.AddMissed();
								}
								else
								{
									_releasedLongEndNoteIndex.Remove(lastNoteIndex);
								}

								releaseLong.Release();
							}
							else
							{
								// その他
								releaseLong.RemovePosition(i + 1);
								releaseLong.SetTouchId(-1);
								_visibleHoldController.SetActive(false, touchId, releaseLong.LongInfo.NoteIndex[0]);
							}
						}

						break;
					}
				}

				// ロングに含まれる同時押しラインをReleaseする
				_sameTimeBar.OnReleseLong(releaseLong);
			}

		}
	}

	/// <summary>
	/// FindのGC削減用
	/// </summary>
	/// <param name="noteIndex"></param>
	/// <returns></returns>
	NoteController FindNoteFromNoteIndex(int noteIndex)
	{
		NoteController note = null;
		int noteCount = _noteObjectPool.CurrentActiveNote.Count;

		for (int i = 0; i < noteCount; i++)
		{
			var noteController = _noteObjectPool.CurrentActiveNote[i];
			if (noteController.NoteIndex == noteIndex)
			{
				note = noteController;
				break;
			}
		}

		return note;
	}

	/// <summary>
	/// 有効なNoteか
	/// (NoteTypeも比較できる)
	/// </summary>
	bool IsValidNote(NoteController note, NoteType noteType, NoteType noteType2, NoteType noteType3, NoteType noteType4, NoteType noteType5)
	{
		return
			!note.IsReleased &&
			note.gameObject.activeSelf &&
			(note.NoteType == noteType || note.NoteType == noteType2 || note.NoteType == noteType3 || note.NoteType == noteType4 || note.NoteType == noteType5);
	}

	/// <summary>
	/// 各ノートとの時間差を測り、最も近い時間を求める
	/// 判定の範囲外の時はdouble.MaxValueを返す
	/// </summary>
	double FindMinTime(double touchTime, float posX, ScreenTouchPhase touchPhase, NoteType noteType, NoteType noteType2 = NoteType.Undefined, NoteType noteType3 = NoteType.Undefined, NoteType noteType4 = NoteType.Undefined, NoteType noteType5 = NoteType.Undefined, int touchId = -1)
	{
		double minTime = double.MaxValue;
		foreach (NoteController note in _noteObjectPool.CurrentActiveNote)
		{
			// 明らかに判定時間外のノーツは以降の処理を行なわない
			if (note.JustTime - touchTime > _judgeManager.LongHitTime + FIND_NOTE_MARGIN_TIME)
			{
				continue;
			}

			// 無効なノーツは以降の処理を行なわない
			if (!IsValidNote(note, noteType, noteType2, noteType3, noteType4, noteType5))
			{
				continue;
			}

			// アップ判定に対する条件その1
			// ・指の状態がアップの時
			// ・ノーツの種類が青ロングの終端
			// ・ロングを掴んている指(TouchID)でなければ判定しない
			if (touchPhase == ScreenTouchPhase.Up &&
				note.NoteType == NoteType.LongEnd &&
				note.UpTouchId != touchId &&
				!_isAuto)
			{
				continue;
			}

			// アップ判定に対する条件その2
			// ・指の状態がアップの時
			// ・ノーツの種類がファジーまたはファジーロングの終端
			// ・判定済みの指の時は判定しない
			if (touchPhase == ScreenTouchPhase.Up &&
				(note.NoteType == NoteType.Fuzzy || (note.NoteType == NoteType.FuzzyLongEnd && note.UpTouchId != touchId)) &&
				_hitTouchId.Contains(touchId) &&
				!_isAuto)
			{
				continue;
			}

			// ホールド判定に対する条件
			// ・指の状態がホールドの時
			// ・ファジー(ホールド)判定開始時間前の時は判定しない
			if (touchPhase == ScreenTouchPhase.Hold && ((touchTime - note.JustTime) < _judgeManager.HoldStart))
			{
				continue;
			}

			// 横幅補正する必要があれば補正値を加える
			bool isRevision = (note.NoteType != NoteType.Fuzzy) && (touchPhase == ScreenTouchPhase.Hold || touchPhase == ScreenTouchPhase.Up);
			float revisionValue = isRevision ? _longDistanceRevision : 0f;

			// 判定の横距離
			var distance = Mathf.Abs(note.PosX - posX);

			// 判定の横距離外の時は判定しない
			if (distance > (_judgeDistance + revisionValue))
			{
				continue;
			}

			// ジャストからのズレ時間
			var diffTime = Math.Abs(touchTime - note.JustTime);

			// 判定エリア外の時は判定しない
			if (!_judgeManager.IsHitArea(distance, diffTime, isRevision))
			{
				continue;
			}

			// より近い時間であればminTimeに格納
			if (diffTime <= _judgeManager.HitTime && diffTime <= (minTime + double.Epsilon))
			{
				minTime = diffTime;
			}
		}

		return minTime;
	}

	/// <summary>
	/// 最も近い距離時間の中でX座標の位置が近いノートを選ぶ
	/// 判定の範囲外の時はnullを返す
	/// </summary>
	NoteController FindNearNote(double touchTime, double minTime, float posX, NoteType noteType, NoteType noteType2 = NoteType.Undefined, NoteType noteType3 = NoteType.Undefined, NoteType noteType4 = NoteType.Undefined, NoteType noteType5 = NoteType.Undefined)
	{
		// 最も近い時間が取れてない時
		if (minTime >= double.MaxValue - double.Epsilon)
		{
			return null;
		}

		float minDistance = float.MaxValue;
		NoteController nearNote = null;

		foreach (NoteController note in _noteObjectPool.CurrentActiveNote)
		{
			if (note.JustTime - touchTime > _judgeManager.LongHitTime + FIND_NOTE_MARGIN_TIME)
			{
				continue;
			}

			if (!IsValidNote(note, noteType, noteType2, noteType3, noteType4, noteType5))
			{
				continue;
			}

			var diffTime = Math.Abs(touchTime - note.JustTime);
			if (diffTime > minTime + double.Epsilon)
			{
				continue;
			}

			var distance = Mathf.Abs(note.PosX - posX);
			if (distance <= minDistance + float.Epsilon)
			{
				minDistance = distance;
				nearNote = note;
			}
		}

		return nearNote;
	}

	/// <summary>
	/// ノートの判定処理
	/// </summary>
	/// <param name="note"></param>
	/// <param name="touchTime"></param>
	/// <param name="touchPhase"></param>
	/// <param name="touchId"></param>
	void JudgeTiming(NoteController note, double touchTime, ScreenTouchPhase touchPhase, int touchId)
	{
		double diffTime = touchTime - note.JustTime;

		if (touchPhase != ScreenTouchPhase.Hold)
		{
			_noFuzzyTimingHistory.Add(diffTime);
		}

		var judgeType = _judgeManager.JudgeTiming(diffTime, touchPhase);

		_timingHistory.Add(touchTime, diffTime, judgeType);

		if (judgeType != JudgeType.None)
		{
			// ファジーのアップ判定を無効にするTouchIdを設定
			if ((touchPhase == ScreenTouchPhase.Tap || touchPhase == ScreenTouchPhase.Hold) &&
				!_hitTouchId.Contains(touchId) && !_isAuto)
			{
				_hitTouchId.Add(touchId);
			}

			if (note.NoteType == NoteType.LongEnd)
			{
				_releasedLongEndNoteIndex.Add(note.NoteIndex);
			}

			note.Release();
		}

		if (judgeType == JudgeType.Brilliant || judgeType == JudgeType.Great)
		{
			if (note.NoteType == NoteType.LongStart || note.NoteType == NoteType.FuzzyLongStart)
			{
				if (note.LongController != null && note.LongController.TouchId == -1)
				{
					OnJudgeStartHold(touchId, note.LongController);
				}
			}
		}

		if (judgeType == JudgeType.Brilliant)
		{
			if (_enableJudgeEffect)
			{
				_judgeEffectPool.Play(note.PosX);
			}
		}

		// 同時押しラインに含まれるノートの場合は同時押しラインをReleaseする
		_sameTimeBar.OnJudgeTiming(note);

		if (judgeType != JudgeType.None && judgeType != JudgeType.Missed)
		{
			PlayHitSound(judgeType, note.NoteType, note.JustTime);
		}
	}

	/// <summary>
	/// ロングのホールド中のタッチIDを保持
	/// </summary>
	/// <param name="touchId"></param>
	void SafeAddCurrentHoldTouchId(int touchId)
	{
		if (!_currentholdTouchId.Contains(touchId))
		{
			_currentholdTouchId.Add(touchId);
		}
	}

	/// <summary>
	/// ロングのホールド中のタッチIDを除く
	/// </summary>
	/// <param name="touchId"></param>
	void SafeRemoveCurrentHoldTouchId(int touchId)
	{
		if (_currentholdTouchId.Contains(touchId))
		{
			_currentholdTouchId.Remove(touchId);
		}
	}

	/// <summary>
	/// ヒット音を鳴らす
	/// </summary>
	/// <param name="type">判定</param>
	void PlayHitSound(JudgeType type, NoteType noteType, double justTime)
	{
		if (type == JudgeType.Brilliant)
		{
			switch (noteType)
			{
				case NoteType.Fuzzy:
				case NoteType.FuzzyLongStart:
				case NoteType.FuzzyLongRelay:
				case NoteType.FuzzyLongEnd:
					if (!Calculate.FastApproximately(justTime, _beforeTouchSeFuzzyJustTime, 0.000001d))
					{
						TouchSEManager.Instance.Play(TouchSEType.FuzzyBrilliantHit);
						_beforeTouchSeFuzzyJustTime = justTime;
					}
					break;

				default:

					if (!Calculate.FastApproximately(justTime, _beforeTouchSeJustTime, 0.000001d))
					{
						TouchSEManager.Instance.Play(TouchSEType.BrillianttHit);
						_beforeTouchSeJustTime = justTime;
					}
					break;
			}
		}
		else
		{
			TouchSEManager.Instance.Play(TouchSEType.NearHit);
		}
	}

	/// <summary>
	/// BgChangeで背景を切り替えるか確認
	/// </summary>
	/// <param name="beat"></param>
	void CheckBgChange(double beat)
	{
		if (_bgChangeIndex < _bgChangePositions.Count && _bgChangePositions[_bgChangeIndex] <= beat)
		{
			BgManager.Instance.ChangeBgImage(_bgChangeImageName[_bgChangeIndex]);

			_bgChangeIndex++;
		}
	}
}
