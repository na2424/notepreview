﻿using System.Collections.Generic;
using UnityEngine;

public sealed class TouchSeCache
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	List<TouchSeInfo> _songInfoCache = new List<TouchSeInfo>();

	//------------------
	// キャッシュ.
	//------------------
	static TouchSeCache _instance;

	//------------------
	// プロパティ.
	//------------------
	public bool HasCache => _songInfoCache.Count > 0;

	public List<TouchSeInfo> Data => _songInfoCache;

	public static TouchSeCache Instance
	{
		get
		{
			return _instance != null ? _instance : _instance = new TouchSeCache();
		}
	}

	/// <summary>
	/// TouchSeInfoのキャッシュを保持する
	/// メモリ上に残り続ける
	/// </summary>
	/// <param name="songInfos"></param>
	public void Cache(List<TouchSeInfo> songInfos)
	{
		_songInfoCache = songInfos.DeepCopy();
		//songInfos.Clear();
		//songInfos = null;
	}

	public void Clear()
	{
		int count = _songInfoCache.Count;

		for (var i = 0; i < count; ++i)
		{
			_songInfoCache[i] = null;
		}

		_songInfoCache.Clear();
	}
}