
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public sealed class RouletteManager : MonoBehaviour
{

    public Transform roulette;
    public GameObject[] plate;

    readonly Color[] chartColor = new Color[3];

    //public void Reset()
    //{
    //    Init();
    //}

    //public void Show()
    //{
    //    StartCoroutine(ShowAnim());
    //}

    //// 円グラフが表示される演出
    //private IEnumerator ShowAnim()
    //{
    //    bool flg = true;
    //    roulette.GetComponent<Image>().fillAmount = 0;
    //    float speed = 0.05f;
    //    while (flg)
    //    {
    //        roulette.GetComponent<Image>().fillAmount += speed;
    //        if (roulette.GetComponent<Image>().fillAmount >= 1) flg = false;
    //        yield return new WaitForSeconds(0.01f);
    //    }
    //}

    public void Init(int[] values)
    {
        chartColor[0] = Color.yellow;
        chartColor[1] = Color.blue;
        chartColor[2] = Color.green;

        int max = values[0] + values[1] + values[2];
        int current = max;

        for (int i = 0; i < values.Length; i++)
        {
            // zの角度を設定
            plate[i].transform.localEulerAngles = new Vector3(0, 0, (max - (float)current) / max * -360f);

            // 円のサイズをfillAmountに設定
            plate[i].GetComponent<Image>().fillAmount = (float)values[i] / max;

            // 色をランダムに設定 明るめにしてます
            plate[i].GetComponent<Image>().color = chartColor[i];
            plate[i].SetActive(true);
            current -= values[i];

        }
        //roulette.GetComponent<Image>().fillAmount = 1;
    }
}
