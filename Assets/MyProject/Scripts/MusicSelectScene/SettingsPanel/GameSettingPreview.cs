﻿using UnityEngine;

/// <summary>
/// ゲーム設定のノーツのプレビューを操作するクラス.
/// </summary>
public class GameSettingPreview : MonoBehaviour
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[SerializeField] NoteObjectPool _noteObjectPool;
	[SerializeField] BeatBarObjectPool _beatBarObjectPool;
	[SerializeField] NoteTexture _noteTexture;
	[SerializeField] Camera _previreCamera;

	//------------------
	// キャッシュ.
	//------------------
	double _previewTime = 7d;
	double _time = -1d;
	int _noteIndex = 0;
	int _bar = 0;
	BpmHelper _bpmHelper = new BpmHelper();
	Sequence _previewSequence = new Sequence();

	//------------------
	// 定数.
	//------------------
	const float START_BEAT_POSITION = 3f;
	const float BASE_BPM = 150f;
	static readonly float BEAT_DISTANCE = Constant.Note.BEAT_DISTANCE;
	static readonly float LANE_LENGTH = Constant.Note.LANE_LENGTH;
	static readonly float OFFSET_Z = Constant.Note.OFFSET_Z;

	//------------------
	// プロパティ.
	//------------------
	float VisibleBeat => (LANE_LENGTH + OFFSET_Z) / (BEAT_DISTANCE * 1f * GameManager.Instance.NotesOption.HiSpeed);

	public void Init()
	{
		for (int i = 0; i < 15; i++)
		{
			_previewSequence.BeatPositions.Add(START_BEAT_POSITION + i);
			_previewSequence.Lanes.Add(3);
			_previewSequence.NoteTypes.Add(NoteType.Normal);
		}

		int last = _previewSequence.BeatPositions.Count - 1;

		SongInfo songInfo = new()
		{
			Offset = 0,
			BaseBpm = BASE_BPM
		};

		songInfo.BpmPositions.Add(0f);
		songInfo.Bpms.Add(150f);

		GameManager.Instance.SelectSongInfo = songInfo;

		_beatBarObjectPool.Init();
		_noteObjectPool.Init();
		_bpmHelper.Init(songInfo);

		_bar = (int)START_BEAT_POSITION;
	}

	public void CallUpdate()
	{
		if (_time > _previewTime)
		{
			_time = -1d;
			_bar = (int)START_BEAT_POSITION;
			_noteIndex = 0;
		}

		_time += Time.deltaTime;
		double beat = _bpmHelper.TimeToBeat(_time);

		if (NeedsCreateBeatBar(beat))
		{
			if (GameManager.Instance.DisplayOption.BeatBar)
			{
				CreateBeatBarFromObjectPool();
			}
			_bar++;
		}

		// 拍線位置更新
		for (int i = 0; i < _beatBarObjectPool.CurrentActiveBar.Count; i++)
		{
			_beatBarObjectPool.CurrentActiveBar[i].OnUpdate(beat, _time, 1f);
			_beatBarObjectPool.CurrentActiveBar[i].UpdateNoteScrollType();
		}

		// Note生成
		if (NeedsCreateNote(beat))
		{
			CreateNoteFromObjectPool();
			_noteIndex++;
		}

		// Note位置更新
		for (int i = 0; i < _noteObjectPool.CurrentActiveNote.Count; i++)
		{
			_noteObjectPool.CurrentActiveNote[i].OnUpdate(beat, _time, 1f);
			_noteObjectPool.CurrentActiveNote[i].UpdateNoteScrollType();
		}
	}

	/// <summary>
	/// 拍線生成する必要があるか
	/// </summary>
	/// <param name="beat">Beat位置</param>
	bool NeedsCreateBeatBar(double beat)
	{
		return
			_noteIndex < _previewSequence.BeatPositions.Count &&
			beat > _bar - VisibleBeat;
	}

	/// <summary>
	/// オブジェクトプールから拍線生成
	/// </summary>
	void CreateBeatBarFromObjectPool()
	{
		var controller = _beatBarObjectPool.Pool.Get();
		double justTime = _bpmHelper.BeatToTime(_bar);

		controller.SetParam(
			beatPosition: _bar,
			justTime: justTime
		);
	}

	/// <summary>
	/// Note生成する必要があるか
	/// </summary>
	/// <param name="beat">Beat位置</param>
	bool NeedsCreateNote(double beat)
	{
		return
			_noteIndex < _previewSequence.BeatPositions.Count &&
			beat > _previewSequence.BeatPositions[_noteIndex] - VisibleBeat;
	}

	/// <summary>
	/// オブジェクトプールからNote生成
	/// </summary>
	void CreateNoteFromObjectPool()
	{
		var noteController = _noteObjectPool.Pool.Get();
		double justTime = _bpmHelper.BeatToTime(_previewSequence.BeatPositions[_noteIndex]);

		noteController.SetParam(
			beatPosition: _previewSequence.BeatPositions[_noteIndex],
			justTime: justTime,
			lane: _previewSequence.Lanes[_noteIndex],
			noteTex: _noteTexture.NoteTypeToTexture(_previewSequence.NoteTypes[_noteIndex]),
			noteType: _previewSequence.NoteTypes[_noteIndex],
			noteIndex: _noteIndex
		);
	}

	/// <summary>
	/// 表示/非表示切り替え
	/// </summary>
	/// <param name="enabled"></param>
	public void SetActive(bool enabled)
	{
		gameObject.SetActive(enabled);
		_previreCamera.enabled = enabled;
	}

	/// <summary>
	/// ノーツサイズが変更されたときに呼ばれる
	/// </summary>
	public void OnChangeNoteSize()
	{
		_noteObjectPool.ChangeNoteSize();
	}
}
