﻿using Cysharp.Text;
using Cysharp.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Threading;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class NotesSettingsPanel : MonoBehaviour
{
	[Serializable]
	public class NotesPanel
	{
		[SerializeField] Button _downButton = default;
		[SerializeField] Button _upButton = default;
		[SerializeField] Slider _slider = default;
		[SerializeField] TextMeshProUGUI _valueText = default;

		public Action<float> OnChangeValue = null;

		int _currentValue = 0;
		float _rate = 1f;
		string _unit;

		public void Init(int value, float delta, string unit, string format = "")
		{
			_downButton.onClick.AddListener(() =>
			{
				_slider.value = Mathf.Clamp(_currentValue - 1, _slider.minValue, _slider.maxValue);
			});

			_upButton.onClick.AddListener(() =>
			{
				_slider.value = Mathf.Clamp(_currentValue + 1, _slider.minValue, _slider.maxValue);
			});

			_slider.onValueChanged.AddListener(value =>
			{
				_currentValue = (int)value;
				_valueText.SetText(ZString.Concat((_currentValue * _rate).ToString(format), _unit));
				OnChangeValue(value);
			});

			_currentValue = value;
			_slider.value = value / _rate;
			_rate = delta;
			_unit = unit;

			_valueText.SetText(ZString.Concat((_currentValue * _rate).ToString(format), _unit));
		}
	}

	[Serializable]
	public class AutoTimingPanel
	{
		[SerializeField] Toggle _toggle = default;

		public Action<bool> OnChangeShowValue = null;

		public void Init(bool isShow)
		{
			SetEvent();

			_toggle.isOn = isShow;
		}

		void SetEvent()
		{
			_toggle.onValueChanged.AddListener(isOn =>
			{
				OnChangeShowValue(isOn);
			});
		}
	}

	[Serializable]
	public class NoteScrollTypePanel
	{
		[SerializeField] Toggle _constantToggle = default;
		[SerializeField] Toggle _decelerateToggle = default;

		public Action<NotesScrollType> OnChangeValue = null;

		public void Init(NotesScrollType notesScrollType)
		{
			_constantToggle.isOn = notesScrollType == NotesScrollType.Constant;
			_decelerateToggle.isOn = notesScrollType == NotesScrollType.Decelerate;

			_constantToggle.onValueChanged.AddListener(isOn =>
			{
				OnChangeValue(isOn ? NotesScrollType.Constant : NotesScrollType.Decelerate);
			});
		}
	}

	[Serializable]
	public class NoteSkinsPanel
	{
		//--------------------------------------
		// インスペクタまたは外部から設定.
		//--------------------------------------
		[SerializeField] Button _downButton = default;
		[SerializeField] Button _upButton = default;
		[SerializeField] RawImage[] _noteImages = default;
		[SerializeField] TextMeshProUGUI _skinNameText = default;
		[SerializeField] NoteTexture[] _defaultNoteTexture;
		[SerializeField] LongTexture[] _defaultLongTexture;
		[SerializeField] NoteTexture _noteTexture;
		[SerializeField] LongTexture _longTexture;

		public Action<string> OnChangeValue = null;

		//------------------
		// キャッシュ.
		//------------------
		List<NoteSkinInfo> _noteSkinInfoList = new List<NoteSkinInfo>();
		int _currentIndex = 0;

		//------------------
		// 定数.
		//------------------
		const int NORMAL = 0;
		const int SLIDE = 1;
		const int FUZZY = 2;

		public void Init(string noteSkinsName)
		{
			// ◀ボタンを押したとき
			_downButton.onClick.AddListener(() =>
			{
				_currentIndex--;

				if (_currentIndex < 0)
				{
					_currentIndex = _noteSkinInfoList.Count - 1;
				}

				_skinNameText.text = _noteSkinInfoList[_currentIndex].skin_name;
				SetNoteRawImage();
				SetTextureScriptableObject();

				// セーブデータに反映
				OnChangeValue?.Invoke(_noteSkinInfoList[_currentIndex].skin_name);
			});

			// ▶ボタンを押したとき
			_upButton.onClick.AddListener(() =>
			{
				_currentIndex++;

				if (_currentIndex >= _noteSkinInfoList.Count)
				{
					_currentIndex = 0;
				}

				_skinNameText.text = _noteSkinInfoList[_currentIndex].skin_name;
				SetNoteRawImage();
				SetTextureScriptableObject();

				// セーブデータに反映
				OnChangeValue?.Invoke(_noteSkinInfoList[_currentIndex].skin_name);
			});

			CancellationTokenSource ctSource = new CancellationTokenSource();
			InitAsync(ctSource.Token, noteSkinsName).Forget();
		}

		async UniTask InitAsync(CancellationToken ct, string noteSkinsName)
		{
			// デフォルトノーツスキンをリストに追加 (インデックス番号0がデフォルトになる)
			_noteSkinInfoList.Add(new NoteSkinInfo()
			{
				skin_name = Constant.Note.DEFAULT_NOTESKINS_NAME,
				NotesTapTexture = _defaultNoteTexture[0].NoteTypeToTexture(NoteType.Normal),
				NotesSlideTexture = _defaultNoteTexture[0].NoteTypeToTexture(NoteType.LongStart),
				NotesFuzzyTexture = _defaultNoteTexture[0].NoteTypeToTexture(NoteType.FuzzyLongStart),
				RelaySlideTexture = _defaultNoteTexture[0].NoteTypeToTexture(NoteType.LongRelay),
				RelayFuzzyTexture = _defaultNoteTexture[0].NoteTypeToTexture(NoteType.FuzzyLongRelay),

				LineSlideTexture = _defaultLongTexture[0].LongTypeToTexture(LongType.Long),
				LineFuzzyTexture = _defaultLongTexture[0].LongTypeToTexture(LongType.FuzzyLong)
			});

			// 色覚対応ノーツスキンをリストに追加 (インデックス番号1)
			_noteSkinInfoList.Add(new NoteSkinInfo()
			{
				skin_name = Constant.Note.COLORB_NOTESKINS_NAME,
				NotesTapTexture = _defaultNoteTexture[1].NoteTypeToTexture(NoteType.Normal),
				NotesSlideTexture = _defaultNoteTexture[1].NoteTypeToTexture(NoteType.LongStart),
				NotesFuzzyTexture = _defaultNoteTexture[1].NoteTypeToTexture(NoteType.FuzzyLongStart),
				RelaySlideTexture = _defaultNoteTexture[1].NoteTypeToTexture(NoteType.LongRelay),
				RelayFuzzyTexture = _defaultNoteTexture[1].NoteTypeToTexture(NoteType.FuzzyLongRelay),

				LineSlideTexture = _defaultLongTexture[1].LongTypeToTexture(LongType.Long),
				LineFuzzyTexture = _defaultLongTexture[1].LongTypeToTexture(LongType.FuzzyLong)
			});

			// カスタムノーツスキンをリストに追加
			_noteSkinInfoList.AddRange(await ExternalDirectory.NoteSkinsCopyFromStreamingToPersistentAsync(ct));

			// セーブデータのノーツスキンの名前から初期値を設定
			_currentIndex = _noteSkinInfoList.FindIndex(t => t.skin_name == noteSkinsName);

			// ない時はデフォルトにする
			if (_currentIndex < 0)
			{
				_currentIndex = 0;
			}

			// 表示名更新
			_skinNameText.SetText(_noteSkinInfoList[_currentIndex].skin_name);

			// RawImageの画像更新
			SetNoteRawImage();

			// 実際に使うノーツスキン画像に反映
			SetTextureScriptableObject();
		}

		/// <summary>
		/// RawImageの画像更新
		/// </summary>
		void SetNoteRawImage()
		{
			_noteImages[NORMAL].texture = _noteSkinInfoList[_currentIndex].NotesTapTexture;
			_noteImages[SLIDE].texture = _noteSkinInfoList[_currentIndex].NotesSlideTexture;
			_noteImages[FUZZY].texture = _noteSkinInfoList[_currentIndex].NotesFuzzyTexture;
		}

		/// <summary>
		/// 実際に使うノーツスキン画像に反映
		/// </summary>
		void SetTextureScriptableObject()
		{
			_noteTexture.SetTextureFromNoteSkinInfo(_noteSkinInfoList[_currentIndex]);
			_longTexture.SetTextureFromNoteSkinInfo(_noteSkinInfoList[_currentIndex]);
		}
	}


	[Serializable]
	public class TouchSePanel
	{
		//--------------------------------------
		// インスペクタまたは外部から設定.
		//--------------------------------------
		[SerializeField] Button _downButton = default;
		[SerializeField] Button _upButton = default;
		[SerializeField] TextMeshProUGUI _touchSeNameText = default;

		public Action<string> OnChangeValue = null;

		//------------------
		// キャッシュ.
		//------------------
		List<TouchSeInfo> _touchSeInfoList = new List<TouchSeInfo>();
		int _currentIndex = 0;

		//------------------
		// プロパティ.
		//------------------
		public List<TouchSeInfo> TouchSeInfoList => _touchSeInfoList;

		public void Init(string touchSeName)
		{
			// ◀ボタンを押したとき
			_downButton.onClick.AddListener(() =>
			{
				_currentIndex--;

				if (_currentIndex < 0)
				{
					_currentIndex = _touchSeInfoList.Count - 1;
				}

				_touchSeNameText.text = _touchSeInfoList[_currentIndex].TouchSeName;

				// セーブデータに反映
				OnChangeValue?.Invoke(_touchSeInfoList[_currentIndex].TouchSeName);
			});

			// ▶ボタンを押したとき
			_upButton.onClick.AddListener(() =>
			{
				_currentIndex++;

				if (_currentIndex >= _touchSeInfoList.Count)
				{
					_currentIndex = 0;
				}

				_touchSeNameText.text = _touchSeInfoList[_currentIndex].TouchSeName;

				// セーブデータに反映
				OnChangeValue?.Invoke(_touchSeInfoList[_currentIndex].TouchSeName);
			});

			CancellationTokenSource ctSource = new CancellationTokenSource();
			InitTouchSE(touchSeName);
		}

		void InitTouchSE(string touchSeName)
		{
			if (!TouchSeCache.Instance.HasCache)
			{
				// デフォルトタッチSEをリストに追加 (インデックス番号0がデフォルトになる)
				_touchSeInfoList.Add(new TouchSeInfo()
				{
					TouchSeName = Constant.Note.DEFAULT_TOUCH_SE_NAME,
					AcbFile = Constant.Note.DEFAULT_TOUCH_SE_ACB
				});

				// カスタムタッチSEをリストに追加
				_touchSeInfoList.AddRange(ExternalDirectory.LoadTouchSeInfosAsync());

				TouchSeCache.Instance.Cache(_touchSeInfoList);
			}

			_touchSeInfoList = TouchSeCache.Instance.Data;

			if(_touchSeInfoList.Count <= 1)
			{
				_upButton.gameObject.SetActive(false);
				_downButton.gameObject.SetActive(false);
			}

			// セーブデータのノーツスキンの名前から初期値を設定
			_currentIndex = _touchSeInfoList.FindIndex(t => t.TouchSeName == touchSeName);

			// ない時はデフォルトにする
			if (_currentIndex < 0)
			{
				_currentIndex = 0;
			}

			// 表示名更新
			_touchSeNameText.SetText(_touchSeInfoList[_currentIndex].TouchSeName);
		}
	}



	//-------------------------------------
	// パネル本体
	//-------------------------------------

	// ノーツプレビュー
	[SerializeField] GameSettingPreview _gameSettingPreview;

	// ノーツ設定
	[SerializeField] NotesPanel _speedPanel;
	[SerializeField] NotesPanel _sizePanel;
	[SerializeField] NotesPanel _timingPanel;
	[SerializeField] NoteSkinsPanel _skinsPanel;
	[SerializeField] TouchSePanel _touchSePanel;

	public void Init()
	{
		_gameSettingPreview.Init();

		var notesOption = GameManager.Instance.NotesOption;

		_speedPanel.OnChangeValue = value =>
		{
			notesOption.HiSpeed = value / 10f;
		};

		_sizePanel.OnChangeValue = value =>
		{
			notesOption.Size = value / 100f;
			_gameSettingPreview.OnChangeNoteSize();
		};

		_timingPanel.OnChangeValue = value =>
		{
			notesOption.Timing = value / 1000f;
		};

		_skinsPanel.OnChangeValue = value =>
		{
			notesOption.NoteSkinsName = value;
		};

		_touchSePanel.OnChangeValue = value =>
		{
			notesOption.TouchSeName = value;
		};

		// decimalに一度キャストしているのは
		// 変数xが6.1fの時に
		// x * 10 = 61
		// (int)(x * 10) = 60
		// になる問題があったため.
		// 直接リテラル6.1fを入れた時は発生しない.
		_speedPanel.Init((int)((decimal)notesOption.HiSpeed * 10), 0.1f, "", "F1");
		_sizePanel.Init((int)((decimal)notesOption.Size * 100), 1f, "%");
		_timingPanel.Init((int)((decimal)notesOption.Timing * 1000), 1f, "ms");
		_skinsPanel.Init(notesOption.NoteSkinsName);
		_touchSePanel.Init(notesOption.TouchSeName);
	}

	public void SetActive(bool isActive)
	{
		_gameSettingPreview.SetActive(isActive);
		gameObject.SetActive(isActive);
	}

	public void SetActivePreview(bool isActive)
	{
		_gameSettingPreview.SetActive(isActive);
	}

	public void CallUpdate()
	{
		_gameSettingPreview.CallUpdate();
	}

}
