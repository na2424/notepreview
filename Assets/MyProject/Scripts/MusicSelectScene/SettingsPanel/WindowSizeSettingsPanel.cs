﻿using Cysharp.Text;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class WindowSizeSettingsPanel : MonoBehaviour
{
	/// <summary>
	/// スライダー値を設定するクラス
	/// </summary>
	[Serializable]
	public class SliderPanel
	{
		[SerializeField] Button _downButton = default;
		[SerializeField] Button _upButton = default;
		[SerializeField] Slider _slider = default;
		[SerializeField] TextMeshProUGUI _valueText = default;

		public Action<float> OnChangeValue = null;

		// 現在の値
		float _currentValue = 0f;
		// スライダーの1移動分
		float _deltaValue = 1f;
		// 値の単位文字
		string _unit;

		public void Init(float value, float delta, string unit, float maxValue)
		{
			SetEvent();
			_currentValue = value;
			_slider.maxValue = maxValue;
			_slider.value = (int)(value);
			_deltaValue = delta;
			_unit = unit;
		}

		void SetEvent()
		{
			_downButton.onClick.AddListener(() =>
			{
				_slider.value = Mathf.Clamp(_currentValue - _deltaValue, _slider.minValue, _slider.maxValue);
			});

			_upButton.onClick.AddListener(() =>
			{
				_slider.value = Mathf.Clamp(_currentValue + _deltaValue, _slider.minValue, _slider.maxValue);
			});

			_slider.onValueChanged.AddListener(value =>
			{
				_currentValue = value;
				_valueText.SetText(ZString.Concat(value, _unit));
				OnChangeValue(value);
			});
		}

		public void SetValue(float value)
		{
			_currentValue = value;
			_slider.value = (int)(value);
		}
	}

	[SerializeField] SliderPanel _widthSizePanel;
	[SerializeField] SliderPanel _heightSizePanel;
	[SerializeField] Button _resetButton = default;
	[SerializeField] Button _excuteButton = default;

	float _width = 1280f;
	float _height = 720f;

	public void Init()
	{
		_width = Screen.width;
		_height = Screen.height;

		_widthSizePanel.OnChangeValue = value =>
		{
			_width = value;
		};

		_heightSizePanel.OnChangeValue = value =>
		{
			_height = value;
		};

		_resetButton.onClick.AddListener(() =>
		{
			_width = 1280f;
			_height = 720f;
			_widthSizePanel.SetValue(_width);
			_heightSizePanel.SetValue(_height);
		});

		_excuteButton.onClick.AddListener(() =>
		{
			Screen.SetResolution((int)((decimal)_width), (int)((decimal)_height), false);
			BgManager.Instance.ResetResolution();
		});

		_widthSizePanel.Init(_width, 1f, "", Screen.currentResolution.width);
		_heightSizePanel.Init(_height, 1f, "", Screen.currentResolution.height);
	}

	public void SetActive(bool isActive)
	{
		if (isActive)
		{
			_widthSizePanel.SetValue(Screen.width);
			_heightSizePanel.SetValue(Screen.height);
		}

		gameObject.SetActive(isActive);
	}
}
