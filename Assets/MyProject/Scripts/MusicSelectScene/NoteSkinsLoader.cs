﻿using Cysharp.Threading.Tasks;
using Dialog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using UnityEngine;

public sealed class NoteSkinsLoader
{
	/// <summary>
	/// ノーツスキン情報の読み込み
	/// </summary>
	/// <returns></returns>
	public async UniTask<List<NoteSkinInfo>> ReadAsync(CancellationToken ct)
	{
		List<NoteSkinInfo> noteSkinInfos = new List<NoteSkinInfo>();

		var jsonList = await ReadJsonAsync(ct);

		if (jsonList.Count == 0)
		{
			return noteSkinInfos;
		}

		if (ct.IsCancellationRequested)
		{
			return noteSkinInfos;
		}

		try
		{
			await UniTask.SwitchToThreadPool();

			foreach (var json in jsonList)
			{
				noteSkinInfos.Add(JsonUtility.FromJson<NoteSkinInfo>(json.Item2));
				noteSkinInfos[noteSkinInfos.Count - 1].DirectoryPath = json.Item1;
			}

			await UniTask.Yield();

			foreach (NoteSkinInfo info in noteSkinInfos)
			{
				info.NotesTapTexture = CreateNoteSkinTexture(info.DirectoryPath, info.notes_tap);
				info.NotesSlideTexture = CreateNoteSkinTexture(info.DirectoryPath, info.notes_slide);
				info.NotesFuzzyTexture = CreateNoteSkinTexture(info.DirectoryPath, info.notes_fuzzy);
				info.RelaySlideTexture = CreateNoteSkinTexture(info.DirectoryPath, info.relay_slide);
				info.RelayFuzzyTexture = CreateNoteSkinTexture(info.DirectoryPath, info.relay_fuzzy);
				info.LineSlideTexture = CreateNoteSkinTexture(info.DirectoryPath, info.line_slide);
				info.LineFuzzyTexture = CreateNoteSkinTexture(info.DirectoryPath, info.line_fuzzy);
			}
		}
		catch (Exception ex)
		{
			var builder = new DialogParametor.Builder("ノーツスキン読み込みエラー", ex.Message);
			builder.AddDefaultAction("閉じる", () => { });
			builder.AddCallbackOnAutoClosed(() => { });
			DialogManager.Instance.Open(builder.Build());
		}

		return noteSkinInfos;
	}

	async UniTask<List<(string, string)>> ReadJsonAsync(CancellationToken ct)
	{
		var directories = Directory.GetDirectories(ExternalDirectory.NoteSkinsPath);
		List<(string, string)> result = new List<(string, string)>();

		var extensions = new string[] { "*.json" };

		foreach (var directory in directories)
		{
			string[] jsonFiles = null;

			// .jsonファイルを検索
			foreach (string ext in extensions)
			{
				jsonFiles = Directory.GetFiles(directory, ext, System.IO.SearchOption.TopDirectoryOnly);

				if (jsonFiles.Length > 0)
				{
					break;
				}
			}

			if (jsonFiles.Length == 0)
			{
				continue;
			}

			var path = Path.Combine(directory, jsonFiles[0]);

			FileInfo file = null;

			// ファイルがある場合
			if (File.Exists(path))
			{
				file = new FileInfo(path);
			}
			else
			{
				Debug.LogError("ファイルがありません:" + path);
			}

			using (StreamReader sr = new StreamReader(file.OpenRead(), Encoding.UTF8))
			{
				try
				{
					var text = await sr.ReadToEndAsync()
					.AsUniTask()
					.AttachExternalCancellation(ct);

					result.Add((directory, text));
				}
				catch (OperationCanceledException e)
				{
					Debug.Log("キャンセルされました:" + e.Message);
					throw;
				}

			}
		}

		return result;
	}

	/// <summary>
	/// ノーツスキン画像のTextureを生成
	/// </summary>
	/// <param name="path">ファイルパス</param>
	/// <returns>Texture</returns>
	Texture CreateNoteSkinTexture(string directoryPath, string fileName)
	{
		Texture2D texture = null;
		if (fileName.HasValue())
		{
			var path = Path.Combine(directoryPath, fileName);
			texture = TextureLoader.Load(path);
		}
		else
		{
			texture = new Texture2D(1, 1, TextureFormat.RGB24, false);
		}

		return texture;
	}
}
