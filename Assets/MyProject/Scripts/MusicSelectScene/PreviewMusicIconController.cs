using Cysharp.Threading.Tasks;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public sealed class PreviewMusicIconController : MonoBehaviour
{
	[SerializeField] Image _iconImage;
	[SerializeField] Sprite[] _iconSprites;

	CancellationTokenSource _ctSource = null;

	int _currentIconIndex = 15;

	public void ChangeIcon(bool isPlay)
	{
		if(_ctSource!=null)
		{
			_ctSource.Cancel();
			_ctSource = null;
		}

		_ctSource = new CancellationTokenSource();
		ChangeIconAsync(_ctSource.Token, isPlay).Forget();
	}

	async UniTask ChangeIconAsync(CancellationToken ct, bool isPlay)
	{
		if (isPlay)
		{
			_currentIconIndex--;

			while (_currentIconIndex >= 0)
			{
				_iconImage.sprite = _iconSprites[_currentIconIndex];
				_currentIconIndex--;

				await UniTask.Delay(16, false, PlayerLoopTiming.Update, ct);

				if (ct.IsCancellationRequested)
				{
					return;
				}
			}

			_currentIconIndex = 0;
		}
		else
		{
			_currentIconIndex++;

			while (_currentIconIndex < _iconSprites.Length)
			{
				_iconImage.sprite = _iconSprites[_currentIconIndex];
				_currentIconIndex++;

				await UniTask.Delay(16, false, PlayerLoopTiming.Update, ct);

				if (ct.IsCancellationRequested)
				{
					return;
				}
			}

			_currentIconIndex = _iconSprites.Length - 1;
		}
	}

	void OnDestroy()
	{
		if (_ctSource != null)
		{
			_ctSource.Cancel();
			_ctSource = null;
		}
	}
}
