﻿using Cysharp.Text;
using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 譜面情報のパネル
/// </summary>
public sealed class SequenceInfoPanel : MonoBehaviour
{
	//--------------------------------------
	// インスペクタまたは外部から設定.
	//--------------------------------------
	[SerializeField] ScrollRect _scrollRect = default;
	[SerializeField] Button _closeButton = default;
	[SerializeField] RouletteManager _rouletteManager;
	[SerializeField] TextScroll _sequenceArtistText;
	[SerializeField] GameObject _sequenceInfoPanelObj;
	[SerializeField] TextMeshProUGUI _loadStateText;
	[SerializeField] TextMeshProUGUI _totalNotesText;
	[SerializeField] TextMeshProUGUI[] _noteCountText;
	[SerializeField] TextMeshProUGUI _longCount;
	[SerializeField] TextMeshProUGUI _fuzzyLongCount;
	[SerializeField] TextMeshProUGUI _laneNotesCount;

	//------------------
	// キャッシュ.
	//------------------
	CancellationTokenSource _ctSource;
	SequenceReader _sequenceReader = new SequenceReader();

	//------------------
	// 定数.
	//------------------
	const string WAIT_TEXT = "読み込み中...";
	const string ERROR_TEXT = "譜面が読み込めませんでした";

	//------------------
	// プロパティ.
	//------------------

	public void Init()
	{
		_closeButton.onClick.AddListener(() => Close());
	}

	public void Open(SongInfo songInfo)
	{
		_loadStateText.text = WAIT_TEXT;
		_loadStateText.gameObject.SetActive(true);
		_sequenceInfoPanelObj.SetActive(false);
		_sequenceArtistText.SetText(songInfo.SequenceArtist);
		_scrollRect.verticalNormalizedPosition = 1.0f;

		gameObject.SetActive(true);

		if (_ctSource != null)
		{
			_ctSource.Cancel();
			_ctSource = null;
		}

		_ctSource = new CancellationTokenSource();

		LoadAsync(_ctSource.Token, songInfo).Forget();
	}

	async UniTask<bool> LoadAsync(CancellationToken ct, SongInfo songInfo)
	{
		var sequence = await _sequenceReader.ReadAsync(ct, songInfo);

		if (ct.IsCancellationRequested)
		{
			return false;
		}

		if (
			sequence == null ||
			sequence.BeatPositions == null ||
			sequence.BeatPositions.Count == 0
		)
		{
			_loadStateText.text = ERROR_TEXT;
			return false;
		}

		_loadStateText.gameObject.SetActive(false);
		SetSequenceDetail(sequence);

		return true;
	}

	void SetSequenceDetail(Sequence sequence)
	{
		_totalNotesText.text = sequence.BeatPositions.Count.ToString();

		int yellowCount = sequence.NoteTypes.Count(noteType => noteType == NoteType.Normal);

		int blueCount = sequence.NoteTypes.Count(noteType =>
			 noteType == NoteType.LongStart ||
			 noteType == NoteType.LongRelay ||
			 noteType == NoteType.LongEnd);

		int greenCount = sequence.NoteTypes.Count(noteType =>
			noteType == NoteType.Fuzzy ||
			noteType == NoteType.FuzzyLongStart ||
			noteType == NoteType.FuzzyLongRelay ||
			noteType == NoteType.FuzzyLongEnd);

		_noteCountText[0].text = yellowCount.ToString();
		_noteCountText[1].text = blueCount.ToString();
		_noteCountText[2].text = greenCount.ToString();

		_rouletteManager.Init(new int[3] { yellowCount, blueCount, greenCount });

		_longCount.text = sequence.LongInfo.Count(longInfo => longInfo.LongType == LongType.Long).ToString();
		_fuzzyLongCount.text = sequence.LongInfo.Count(longInfo => longInfo.LongType == LongType.FuzzyLong).ToString();

		int[] laneCounts = new int[7] { 0,0,0,0,0,0,0 };

		for(int i=0; i< sequence.Lanes.Count; i++)
		{
			laneCounts[sequence.Lanes[i]]++;
		}

		using (var sb = ZString.CreateUtf8StringBuilder())
		{
			for (int i = 0; i < laneCounts.Length; i++)
			{
				sb.Append(laneCounts[i]);

				if(i < laneCounts.Length - 1)
				{
					sb.Append(" | ");
				}
			}
			_laneNotesCount.text = sb.ToString();
		}

		_sequenceInfoPanelObj.SetActive(true);
	}

	void Close()
	{
		if (_ctSource != null)
		{
			_ctSource.Cancel();
			_ctSource = null;
		}

		gameObject.SetActive(false);
	}


}
