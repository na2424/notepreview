﻿using UnityEngine;
using UnityEditor;

namespace AssetTool
{
	public static class FindGUID
	{

		[MenuItem("Tools/Find by GUID")]
		static void Start()
		{
			FindGUIDWindow.Open();
		}

	}

	public class FindGUIDWindow : EditorWindow
	{

		private Object asset;
		private string guid;

		public static void Open()
		{
			var window = (FindGUIDWindow)GetWindow(typeof(FindGUIDWindow));
			window.titleContent = new GUIContent("Find GUID");
			window.Show();
		}

		private void OnGUI()
		{
			GUILayout.Label("GUIDを入力するとアセットを検索します。\nまたはアセットをドラッグ&ドロップするとGUIDを表示します。");

			var oldGUID = guid;
			guid = EditorGUILayout.TextField("GUID", guid);
			if (oldGUID != guid)
			{
				asset = AssetDatabase.LoadAssetAtPath<Object>(AssetDatabase.GUIDToAssetPath(guid));
			}

			var oldAsset = asset;
			asset = EditorGUILayout.ObjectField(asset, typeof(Object), false);
			if (asset != oldAsset)
			{
				guid = AssetDatabase.AssetPathToGUID(AssetDatabase.GetAssetPath(asset));
			}
		}
	}
}
