using Cysharp.Text;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class FolderOpen
{
	[MenuItem("OpenFolder/Songs")]
	public static void OpenSonsFolder()
	{
		var path = ZString.Concat(Directory.GetCurrentDirectory(), "/Songs");
		EditorUtility.RevealInFinder(path);
	}
	
}
