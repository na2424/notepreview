Shader "Unlit/StencilRead"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Color("Color",  Color) = (1,1,1,1)
        _Stencil("Stencil ID", Float) = 2
        [HDR] _EmissionColor ("Emission Color", Color) = (0,0,0)
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" }
        LOD 100
        ZWrite Off
        ZTest Always
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            Stencil{
                Ref[_Stencil]
                Comp NotEqual
                Fail keep
            }

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            fixed4 _Color;
            float4 _EmissionColor;
            fixed _Stencil;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed anim = sin(_Time.x * 600) * 0.25 + 1.25;
                fixed4 col = tex2D(_MainTex, i.uv) * _Color *  _EmissionColor * ((_Stencil > 1.5) ? float4(anim, anim, anim, 1.0) : float4(1, 1, 1, 1.0));
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
